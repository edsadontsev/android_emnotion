package com.rural.emnotion.activities;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.rural.emnotion.R;
import com.rural.emnotion.adapters.ChatsAdapter;
import com.rural.emnotion.adapters.MessageAdapter;
import com.rural.emnotion.models.Message;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.message) EditText message;

    private String chatId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        fetchExtras();
        initUI();
        initToolbar();
        getMessages();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Messenger");
            toolbar.setTitleTextColor(Color.WHITE);
        }
    }

    private void fetchExtras(){
        chatId = getIntent().getStringExtra("chatId");
    }

    private void initUI(){
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);


        message.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEND) {
                    if(!message.getText().toString().equals("")){
                        JSONObject jsonObject = new JSONObject();
                        try {
                            SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                            String dateText = df2.format(System.currentTimeMillis());
                            jsonObject.put("IdUser",USM.getUserID());
                            jsonObject.put("Token",USM.getUserToken());
                            jsonObject.put("IdUserTo",chatId);
                            jsonObject.put("Subject","");
                            jsonObject.put("MailText",message.getText().toString());
                            jsonObject.put("MailDate",dateText);

                            Log.wtf("json",jsonObject.toString());
                            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
                            sendMessage(requestBody);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return false;
            }
        });
    }

    private void getMessages(){
        USM.init(this);
        Call<List<Message>> getSpecialists  = API.getApiRequestService().getChatById(USM.getUserID(),chatId,
                USM.getUserToken());
        getSpecialists.enqueue(new Callback<List<Message>>() {
            @Override
            public void onResponse(Call<List<Message>> call, Response<List<Message>> response) {
                Log.wtf("call",call.request().url().toString());
                MessageAdapter adapter = new MessageAdapter(ChatActivity.this, response.body());
                recyclerView.setAdapter(adapter);
            }
            @Override
            public void onFailure(Call<List<Message>> call, Throwable t) {
            }
        });
    }

    private void sendMessage(RequestBody requestBody) {
        USM.init(this);

        Call<Void> getSpecialists = API.getApiRequestService().sendMessage(requestBody);
        getSpecialists.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.wtf("call",call.request().url().toString());
                if(response.isSuccessful()) {
                    message.setText("");
                    getMessages();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }
}
