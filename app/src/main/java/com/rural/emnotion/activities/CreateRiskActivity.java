package com.rural.emnotion.activities;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rural.emnotion.R;
import com.rural.emnotion.models.Product;
import com.rural.emnotion.network.API;
import com.rural.emnotion.network.ApiService;
import com.rural.emnotion.settings.USM;
import com.squareup.timessquare.CalendarPickerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.rural.emnotion.util.DateUtils.getDate;
import static com.rural.emnotion.util.DateUtils.getTitleDate;

public class CreateRiskActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.parameters_spinner) MultiSelectSpinner parametersSpinner;
    @BindView(R.id.parameters_layout) LinearLayout parametersLayout;
    @BindView(R.id.btn_create) Button createButton;
    @BindView(R.id.risk_name) EditText riskName;
    @BindView(R.id.risk_description) EditText riskDescription;
    @BindView(R.id.spinner_day_part) MaterialSpinner spinnerDayPart;
    @BindView(R.id.calendar) ImageView calendar;
    @BindView(R.id.date_picker) CalendarPickerView datePicker;
    @BindView(R.id.date_picker_container) RelativeLayout datePickerContainer;
    @BindView(R.id.btn_pick_date) Button pickDateButton;
    @BindView(R.id.start_date) EditText startDate;
    @BindView(R.id.end_date) EditText endDate;
    @BindView(R.id.quantity_of_intervals) EditText quantityOfInterval;
    @BindView(R.id.solution) EditText solution;
    @BindView(R.id.products_spinner) MaterialSpinner spinnerProducts;

    private String[] parametersUnits;
    private String[] parameters;
    private boolean[] selectedParams;

    private List<Product> products;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_risk);
        ButterKnife.bind(this);

        getProduct();
        initUI();
        initToolbar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Create scenario");
            toolbar.setTitleTextColor(Color.WHITE);
        }
    }

    private void initUI() {
        String[] intervalArray = getResources().getStringArray(R.array.risk_day_part);
        spinnerDayPart.setItems(intervalArray);


        parameters = getResources().getStringArray(R.array.alarm_parameters);
        parametersUnits = getResources().getStringArray(R.array.alarm_parameters_unit);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_multiple_choice,
                Arrays.asList(parameters));
        parametersSpinner.setListAdapter(adapter);
        parametersSpinner.setMinSelectedItems(0);
        parametersSpinner.setListener(new MultiSelectSpinner.MultiSpinnerListener() {
            @Override
            public void onItemsSelected(boolean[] selected) {
                selectedParams = selected;
                parametersLayout.removeAllViews();
                for (int i = 0; i < selected.length; i++) {
                    if (selected[i]) {
                        View parameterView = LayoutInflater.from(CreateRiskActivity.this)
                                .inflate(R.layout.item_risk_parameter, parametersLayout, false);
                        TextView parameterTitle = parameterView.findViewById(R.id.parameter_title);
                        parameterTitle.setText(parameters[i] + ", " + parametersUnits[i]);
                        parametersLayout.addView(parameterView);
                    }
                }
            }
        });

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerContainer.setVisibility(View.VISIBLE);

                Calendar lastYear = Calendar.getInstance();
                lastYear.add(Calendar.YEAR, 1);
                Calendar lastDay = Calendar.getInstance();
                lastDay.add(Calendar.DATE, 1);

                datePicker.init(new Date(),lastYear.getTime())
                        .withSelectedDate(lastDay.getTime())
                        .inMode(CalendarPickerView.SelectionMode.RANGE);
            }
        });

        pickDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerContainer.setVisibility(View.GONE);
                ArrayList<Date> selectedDates = (ArrayList<Date>) datePicker.getSelectedDates();

                if(selectedDates.size()>1){
                    startDate.setText(getDate(selectedDates.get(0)));
                    endDate.setText(getDate(selectedDates.get(selectedDates.size()-1)));
                }
            }
        });

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                USM.init(CreateRiskActivity.this);
                int j = 0;
                JSONArray jsArray = new JSONArray();
                for(int i=0;i<selectedParams.length;i++){
                    if(selectedParams[i]){
                        JSONObject params = new JSONObject();
                        RelativeLayout parameter = (RelativeLayout) parametersLayout.getChildAt(j);
                        TextView min = parameter.findViewById(R.id.parameter_min);
                        TextView avg = parameter.findViewById(R.id.parameter_avg);
                        TextView deviation = parameter.findViewById(R.id.parameter_deviation);
                        TextView max = parameter.findViewById(R.id.parameter_max);
                        TextView operator = parameter.findViewById(R.id.parameter_operator);
                        TextView interval = parameter.findViewById(R.id.parameter_interval);
                        try {
                            params.put("Id",i+1);
                            params.put("Min",min.getText().toString());
                            params.put("Max",max.getText().toString());
                            params.put("Avg",avg.getText().toString());
                            params.put("Deviation",deviation.getText().toString());
                            params.put("Operator",operator.getText().toString());
                            params.put("Interval",interval.getText().toString());
                            params.put("IdUser",USM.getUserID());
                            jsArray.put(params);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        j++;
                    }
                }
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("AlarmRiskName",riskName.getText().toString());
                    jsonObject.put("Description",riskDescription.getText().toString());
                    jsonObject.put("DayPart",spinnerDayPart.getSelectedIndex());
                    jsonObject.put("StartDate",startDate.getText().toString());
                    jsonObject.put("EndDate",endDate.getText().toString());
                    jsonObject.put("Intervals",quantityOfInterval.getText().toString());
                    jsonObject.put("Solution",solution.getText().toString());
                    jsonObject.put("IdProduct",products.get(spinnerProducts.getSelectedIndex()).IdProduct);
                    jsonObject.put("IdUser",USM.getUserID());
                    jsonObject.put("IdStation",USM.getStation());
                    jsonObject.put("Token",USM.getUserToken());
                    jsonObject.put("alarmParams",jsArray);
                    Log.wtf("json",jsonObject.toString());
                    RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
                    createRisk(requestBody);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private void getProduct(){
        USM.init(this);
        Call<List<Product>> getSpecialists = API.getApiRequestService().getProductCatalog(USM.getUserID(),USM.getUserToken());
        getSpecialists.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                if(response.isSuccessful()){
                   products = response.body();
                    String[] intervalArray = new String[response.body().size()];
                    for(int i=0;i<response.body().size();i++) intervalArray[i] = response.body().get(i).Name;
                    spinnerProducts.setItems(intervalArray);
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
            }
        });
    }

    private void createRisk(RequestBody requestBody){
        USM.init(this);
        Call<Void> getSpecialists = API.getApiRequestService().createRisk(requestBody);
        getSpecialists.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.wtf("Call",call.request().url().toString());
                if(response.isSuccessful()){
                   finish();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }

}
