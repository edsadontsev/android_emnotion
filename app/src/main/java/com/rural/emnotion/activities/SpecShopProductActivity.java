package com.rural.emnotion.activities;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.rural.emnotion.R;
import com.rural.emnotion.adapters.ShopAdapter;
import com.rural.emnotion.models.Product;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpecShopProductActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;

    private String name;
    private String id;

    private ShopAdapter adapter;
    private List<Product> specialistList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spec_shop_product);
        ButterKnife.bind(this);

        fetchExtras();
        initToolbar();
        getAllProducts();
    }

    private void fetchExtras(){
        name = getIntent().getStringExtra("name");
        id = getIntent().getStringExtra("id");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(name);
            toolbar.setTitleTextColor(Color.WHITE);
        }
    }

    private void getAllProducts(){
        USM.init(this);
        Call<List<Product>> getSpecialists  = API.getApiRequestService().getSpecShopProduct(USM.getUserID(),
                USM.getUserToken(),id);
        getSpecialists.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                Log.wtf("call",call.request().url().toString());
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SpecShopProductActivity.this);
                recyclerView.setLayoutManager(mLayoutManager);
                specialistList = response.body();
                adapter = new ShopAdapter(SpecShopProductActivity.this, specialistList);
                recyclerView.setAdapter(adapter);
            }
            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
            }
        });
    }
}
