package com.rural.emnotion.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.rural.emnotion.R;
import com.rural.emnotion.models.Product;
import com.rural.emnotion.network.ApiService;
import com.rural.emnotion.util.CircleTransform;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShopProductActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.image) ImageView image;
    @BindView(R.id.title) TextView title;
    @BindView(R.id.delivery) EditText delivery;
    @BindView(R.id.owner) EditText owner;
    @BindView(R.id.email) EditText email;
    @BindView(R.id.phone) EditText phone;
    @BindView(R.id.price) TextView price;
    @BindView(R.id.btn_buy) Button buy;


    private Product product;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_product);
        ButterKnife.bind(this);

        fetchExtras();
        initToolbar();
        initUI();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void fetchExtras(){
        product = (Product) getIntent().getSerializableExtra("product");
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(product.Name);
            toolbar.setTitleTextColor(Color.WHITE);
        }
    }

    private void initUI(){
        Glide.with(this).load(ApiService.PRODUCTS_IMAGE_URL + product.PhotoName)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(image);

        title.setText(product.Instruction);
        delivery.setText(product.Delivery + " km");
        owner.setText(product.OwnerName);
        email.setText(product.Email);
        phone.setText(product.Phone);
        price.setText(product.Price + "$");

        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(product.PaymentURL));
                startActivity(i);
            }
        });
    }
}
