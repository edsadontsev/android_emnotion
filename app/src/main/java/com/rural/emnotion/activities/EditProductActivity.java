package com.rural.emnotion.activities;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.rural.emnotion.R;
import com.rural.emnotion.models.Product;
import com.rural.emnotion.network.API;
import com.rural.emnotion.network.ApiService;
import com.rural.emnotion.settings.USM;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProductActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.product_name) EditText productName;
    @BindView(R.id.product_decription) EditText productDescription;
    @BindView(R.id.product_instruction) EditText productInstruction;
    @BindView(R.id.product_distance) EditText productDistance;
    @BindView(R.id.product_photo) CircleImageView productPhoto;
    @BindView(R.id.btn_create) Button addProductButton;
    @BindView(R.id.btn_delete) ImageView deleteButton;

    private Product product;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        ButterKnife.bind(this);

        fetchExtras();
        initUI();
        initToolbar();
        getProduct();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Edit product");
            toolbar.setTitleTextColor(Color.WHITE);
        }
    }

    private void fetchExtras(){
        product = (Product) getIntent().getSerializableExtra("product");
    }

    private void initUI(){
        addProductButton.setText("Save");
        addProductButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(productName.getText().toString().equals("")){
                    Toast.makeText(EditProductActivity.this,"Enter product name",Toast.LENGTH_SHORT).show();
                }
                else if(productDescription.getText().toString().equals("")){
                    Toast.makeText(EditProductActivity.this,"Enter product description",Toast.LENGTH_SHORT).show();
                }
                else if(productInstruction.getText().toString().equals("")){
                    Toast.makeText(EditProductActivity.this,"Enter product instruction",Toast.LENGTH_SHORT).show();
                }
                else if(productDistance.getText().toString().equals("")){
                    Toast.makeText(EditProductActivity.this,"Enter product distance",Toast.LENGTH_SHORT).show();
                }
                else updateProduct();
            }
        });

        deleteButton.setVisibility(View.VISIBLE);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteProduct();
            }
        });
    }

    private void getProduct(){
        USM.init(this);
        Call<List<Product>> getSpecialists = API.getApiRequestService().getProductById(USM.getUserID(),product.IdProduct,USM.getUserToken());
        getSpecialists.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                if(response.isSuccessful()){
                    product = response.body().get(0);
                    productName.setText(response.body().get(0).Name);
                    productDescription.setText(response.body().get(0).Description);
                    productInstruction.setText(response.body().get(0).Instruction);
                    productDistance.setText(response.body().get(0).Delivery);
                    Glide.with(EditProductActivity.this)
                            .load(ApiService.PRODUCTS_IMAGE_URL + response.body().get(0).PhotoName)
                            .centerCrop()
                            .into(productPhoto);
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
            }
        });
    }

    private void updateProduct(){
        USM.init(this);
        Call<Void> getSpecialists = API.getApiRequestService().updateProduct(USM.getUserToken(),product.IdProduct, USM.getUserID(),
                productName.getText().toString(), productDescription.getText().toString(),
                productInstruction.getText().toString(),productInstruction.getText().toString(),null);
        getSpecialists.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful()){
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }

    private void deleteProduct(){
        USM.init(this);
        Call<Void> getSpecialists = API.getApiRequestService().deleteProduct(USM.getUserToken(),
                product.IdProduct, USM.getUserID());
        getSpecialists.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful()){
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.wtf("fail",t.getMessage());
            }
        });
    }
}

