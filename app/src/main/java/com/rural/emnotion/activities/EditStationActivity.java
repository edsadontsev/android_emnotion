package com.rural.emnotion.activities;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rural.emnotion.R;
import com.rural.emnotion.models.AddressModel;
import com.rural.emnotion.models.Station;
import com.rural.emnotion.network.API;
import com.rural.emnotion.network.AddressAPIQuery;
import com.rural.emnotion.settings.USM;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditStationActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.station_title) EditText stationTitle;
    @BindView(R.id.station_type) MaterialSpinner stationType;
    @BindView(R.id.station_ip) EditText stationIp;
    @BindView(R.id.station_usage) MaterialSpinner stationUsage;
    @BindView(R.id.station_description) EditText stationDescription;
    @BindView(R.id.station_address) TextView stationAddress;
    @BindView(R.id.station_coordinates) TextView stationCoordinates;
    @BindView(R.id.station_elevation) EditText stationElevation;
    @BindView(R.id.btn_create) Button createButton;
    @BindView(R.id.btn_delete) ImageView deleteButton;

    private Station station;
    private AddressModel address;
    private LatLng coordinates;
    private GoogleMap googleMap;

    private Call<AddressModel> addressRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_info);
        ButterKnife.bind(this);

        initToolbar();
        fetchExtras();
        initUI();
        initStation();

    }

    @Override
    protected void onDestroy() {
        if(addressRequest!=null)addressRequest.cancel();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Edit station");
            toolbar.setTitleTextColor(Color.WHITE);
        }
    }

    private void fetchExtras(){
        station = (Station) getIntent().getSerializableExtra("station");
    }

    private void initUI(){
        SupportMapFragment mapFragment = (SupportMapFragment) this.getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        deleteButton.setVisibility(View.VISIBLE);
        createButton.setText("Save");
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(stationTitle.getText().toString().equals("")){
                    Toast.makeText(EditStationActivity.this,"Enter station name",Toast.LENGTH_SHORT).show();
                }
                else if(stationIp.getText().toString().equals("")){
                    Toast.makeText(EditStationActivity.this,"Enter station IP",Toast.LENGTH_SHORT).show();
                }
                else if(stationAddress.getText().toString().equals("")){
                    Toast.makeText(EditStationActivity.this,"Select station location",Toast.LENGTH_SHORT).show();
                }
                else if(stationElevation.getText().toString().equals("")){
                    Toast.makeText(EditStationActivity.this,"Enter station elevation level",Toast.LENGTH_SHORT).show();
                }
                else{
                    updateStation();
                }
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteStation();
            }
        });
    }

    private void initStation(){
        stationTitle.setText(station.Name);
        stationIp.setText(station.IpAddress);
        stationDescription.setText(station.Description);
        stationAddress.setText(station.Address);
        stationCoordinates.setText(station.Latitude+"/"+station.Longitude);
        stationElevation.setText(station.Elevation+"");

        String[] stationTypeArray = getResources().getStringArray(R.array.station_type);
        stationType.setItems(stationTypeArray);
        String[] stationUsageArray = getResources().getStringArray(R.array.station_usage);
        stationUsage.setItems(stationUsageArray);

        stationType.setSelectedIndex(station.IdStationType - 1);
        stationUsage.setSelectedIndex(station.IdStationUsage - 1);
    }


    private void getAddress(final String coordinates, final LatLng latLng) {
        addressRequest = AddressAPIQuery.getInstance().getAddress(coordinates, true);
        addressRequest.enqueue(new Callback<AddressModel>() {

            @Override
            public void onResponse(Call<AddressModel> call, final Response<AddressModel> response) {
                Log.wtf("call", call.request().url().toString());
                if (response.isSuccessful()) {
                    if (response.body().results.size() != 0) {
                        address = response.body();
                        stationAddress.setText(response.body().results.get(0).formatted_address);
                        stationCoordinates.setText(latLng.latitude + "/" + latLng.longitude);
                    }

                }
            }

            @Override
            public void onFailure(Call<AddressModel> call, Throwable t) {

            }

        });
    }

    private void updateStation(){
        USM.init(this);
        Call<Void> getStation = API.getApiRequestService().updateStation(USM.getUserToken(),USM.getUserID(), station.IdStation,
                stationTitle.getText().toString(), stationType.getSelectedIndex() + 1, stationIp.getText().toString(),
                address.results.get(0).address_components.get(address.results.get(0).address_components.size() - 2).long_name,
                address.results.get(0).address_components.get(address.results.get(0).address_components.size() - 3).long_name,
                address.results.get(0).address_components.get(address.results.get(0).address_components.size() - 4).long_name,
                stationAddress.getText().toString(), coordinates.latitude, coordinates.longitude, false,
                stationUsage.getSelectedIndex() + 1, stationDescription.getText().toString(), true,
                stationElevation.getText().toString());

        getStation.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.wtf("call",call.request().url().toString());
                Log.wtf("call",response.code()+"");
                Log.wtf("call",response.message()+"");
                if(response.isSuccessful()){
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }

    private void deleteStation(){
        USM.init(this);
        Call<Void> getSpecialists = API.getApiRequestService().deleteStation(USM.getUserToken(),
                station.IdStation, USM.getUserID());
        getSpecialists.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful()){
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.wtf("fail",t.getMessage());
            }
        });
    }


    @Override
    public void onMapClick(LatLng latLng) {
        googleMap.clear();
        coordinates = latLng;
        googleMap.addMarker(new MarkerOptions().position(latLng));
        getAddress(latLng.latitude + " " + latLng.longitude, latLng);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        this.googleMap = googleMap;
        coordinates = new LatLng(station.Latitude,station.Longitude);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 15.5f), 2000, null);
        googleMap.addMarker(new MarkerOptions().position(coordinates));
        getAddress(station.Latitude + " " + station.Longitude, coordinates);
        googleMap.setOnMapClickListener(this);
    }
}

