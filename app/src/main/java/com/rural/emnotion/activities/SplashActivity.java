package com.rural.emnotion.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.rural.emnotion.MainActivity;
import com.rural.emnotion.R;
import com.rural.emnotion.settings.USM;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        USM.init(SplashActivity.this);
        if (USM.isRegister()) {
            Thread background = new Thread() {
                public void run() {
                    try {
                        sleep(2 * 1000);
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        finish();
                    } catch (Exception e) {
                    }
                }
            };
            background.start();
        } else {
            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(i);
            finish();
        }

    }
}
