package com.rural.emnotion.activities;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rural.emnotion.R;
import com.rural.emnotion.adapters.ProductsAdapter;
import com.rural.emnotion.models.Product;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateProductsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.product_name) EditText productName;
    @BindView(R.id.product_decription) EditText productDescription;
    @BindView(R.id.product_instruction) EditText productInstruction;
    @BindView(R.id.product_distance) EditText productDistance;
    @BindView(R.id.product_photo) CircleImageView productPhoto;
    @BindView(R.id.btn_create) Button addProductButton;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        ButterKnife.bind(this);

        initUI();
        initToolbar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Add product");
            toolbar.setTitleTextColor(Color.WHITE);
        }
    }

    private void initUI(){
        addProductButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(productName.getText().toString().equals("")){
                    Toast.makeText(CreateProductsActivity.this,"Enter product name",Toast.LENGTH_SHORT).show();
                }
                else if(productDescription.getText().toString().equals("")){
                    Toast.makeText(CreateProductsActivity.this,"Enter product description",Toast.LENGTH_SHORT).show();
                }
                else if(productInstruction.getText().toString().equals("")){
                    Toast.makeText(CreateProductsActivity.this,"Enter product instruction",Toast.LENGTH_SHORT).show();
                }
                else if(productDistance.getText().toString().equals("")){
                    Toast.makeText(CreateProductsActivity.this,"Enter product distance",Toast.LENGTH_SHORT).show();
                }
                else createProduct();
            }
        });
    }

    private void createProduct(){
        USM.init(this);
        Call<Void> getSpecialists = API.getApiRequestService().addProduct(USM.getUserToken(), USM.getUserID(),
                productName.getText().toString(), productDescription.getText().toString(),
                productInstruction.getText().toString(),productInstruction.getText().toString(),null);
        getSpecialists.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful()){
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }
}
