package com.rural.emnotion.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.rural.emnotion.R;
import com.rural.emnotion.network.API;
import com.rural.emnotion.network.resposes.RegResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegActivity extends AppCompatActivity {


    private AutoCompleteTextView email, phone;
    private EditText password;
    private Button email_sign_up_button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg);

        email = (AutoCompleteTextView) findViewById(R.id.email);
        phone = (AutoCompleteTextView) findViewById(R.id.phone);
        password = (EditText) findViewById(R.id.password);

        email_sign_up_button = (Button) findViewById(R.id.email_sign_up_button);
        email_sign_up_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<RegResponse> call = API.getApiRequestService().regUser(
                        email.getText().toString(),
                        password.getText().toString(),
                        phone.getText().toString(), "1");
                call.enqueue(new Callback<RegResponse>() {
                    @Override
                    public void onResponse(Call<RegResponse> call, Response<RegResponse> response) {
                            if (response.body().IdUser!=null&&!response.body().IdUser.equals("")){
                                startActivity(new Intent(RegActivity.this, LoginActivity.class));
                                finish();
//                                USM.init(RegActivity.this);
//                                USM.setUserSession(response.body().IdUser, response.body().Email, response.body().Phone,
//                                        response.body().Token, response.body().ProviderKey,response.body().SocialNetwork,
//                                        response.body().Status);
//                                Intent intent = new Intent(RegActivity.this, MainActivity.class);
//                                startActivity(intent);
//                                finishAffinity();
                            }
                    }

                    @Override
                    public void onFailure(Call<RegResponse> call, Throwable t) {

                    }
                });
            }
        });

    }
}
