package com.rural.emnotion.activities;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rural.emnotion.R;
import com.rural.emnotion.models.Alarm;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateAlarmActivity extends AppCompatActivity{

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.parameters_spinner) MultiSelectSpinner parametersSpinner;
    @BindView(R.id.parameters_layout) LinearLayout parametersLayout;
    @BindView(R.id.btn_create) Button createButton;
    @BindView(R.id.alarm_name) TextView alarmName;
    @BindView(R.id.alarm_description) TextView alarmDescription;

    private String[] parametersUnits;
    private String[] parameters;
    private boolean[] selectedParams;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        ButterKnife.bind(this);

        initUI();
        initToolbar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Create alarm");
            toolbar.setTitleTextColor(Color.WHITE);
        }
    }

    private void initUI(){
        parameters = getResources().getStringArray(R.array.alarm_parameters);
        parametersUnits = getResources().getStringArray(R.array.alarm_parameters_unit);
        ArrayAdapter<String> adapter = new ArrayAdapter <>(this, android.R.layout.simple_list_item_multiple_choice,
                Arrays.asList(parameters));
        parametersSpinner.setListAdapter(adapter);
        parametersSpinner.setMinSelectedItems(0);
        parametersSpinner.setListener(new MultiSelectSpinner.MultiSpinnerListener() {
            @Override
            public void onItemsSelected(boolean[] selected) {
                selectedParams = selected;
                parametersLayout.removeAllViews();
                for(int i=0; i<selected.length; i++) {
                    if(selected[i]) {
                        View parameterView = LayoutInflater.from(CreateAlarmActivity.this)
                                .inflate(R.layout.item_alarm_parameter, parametersLayout, false);
                        TextView parameterTitle = parameterView.findViewById(R.id.parameter_title);
                        parameterTitle.setText(parameters[i] +", " + parametersUnits[i]);
                        parametersLayout.addView(parameterView);
                    }
                }
            }
        });

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                USM.init(CreateAlarmActivity.this);
                int j = 0;
                JSONArray jsArray = new JSONArray();
                for(int i=0;i<selectedParams.length;i++){
                    if(selectedParams[i]){
                        JSONObject params = new JSONObject();
                        RelativeLayout parameter = (RelativeLayout) parametersLayout.getChildAt(j);
                        TextView min = parameter.findViewById(R.id.parameter_min);
                        TextView max = parameter.findViewById(R.id.parameter_max);
                        try {
                            params.put("Id",i+1);
                            params.put("Min",min.getText().toString());
                            params.put("Max",max.getText().toString());
                            params.put("IdUser",USM.getUserID());
                            jsArray.put(params);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        j++;
                    }
                }
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("AlarmName",alarmName.getText().toString());
                    jsonObject.put("StartAlarmTime",null);
                    jsonObject.put("Description",alarmDescription.getText().toString());
                    jsonObject.put("IdStation",USM.getStation());
                    jsonObject.put("IdUser",USM.getUserID());
                    jsonObject.put("Token",USM.getUserToken());
                    jsonObject.put("alarmParams",jsArray);
                    Log.wtf("json",jsonObject.toString());
                    RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
                    createAlarm(requestBody);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void createAlarm(RequestBody requestBody){
        Call<Void> getAlarm = API.getApiRequestService().insertCompose(requestBody);
        getAlarm.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.wtf("response",response.code()+"");
                Log.wtf("response",response.message()+"");
                if(response.isSuccessful()){
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }
}
