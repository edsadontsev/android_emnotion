package com.rural.emnotion.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rural.emnotion.R;
import com.rural.emnotion.activities.EditRiskActivity;
import com.rural.emnotion.activities.EditStationActivity;
import com.rural.emnotion.models.Impact;
import com.rural.emnotion.models.Risk;

import java.util.List;

public class ImpactAdapter extends RecyclerView.Adapter<ImpactAdapter.MyViewHolder> {

    private Context mContext;
    private List<Impact> albumList;
    public OnShowClickListener onShowClickListener;

    public OnShowClickListener getOnShowClickListener() {
        return onShowClickListener;
    }

    public void setOnShowClickListener(OnShowClickListener onShowClickListener) {
        this.onShowClickListener = onShowClickListener;
    }

    public interface OnShowClickListener{
        void onShowClick(int id);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView event,  date, level;
        public RelativeLayout root;

        public MyViewHolder(View view) {
            super(view);
            root = (RelativeLayout) view.findViewById(R.id.container);
            event = (TextView) view.findViewById(R.id.event);
            date = (TextView) view.findViewById(R.id.date);
            level = (TextView) view.findViewById(R.id.level);
        }
    }


    public ImpactAdapter(Context mContext, List<Impact> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public ImpactAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_impact, parent, false);

        return new ImpactAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ImpactAdapter.MyViewHolder holder, final int position) {
        final Impact article = albumList.get(position);
        holder.event.setText(article.AlarmRiskName);
        holder.date.setText(article.CalcTimeToAlarm);
        if(article.Level >=0 && article.Level <=90){
            holder.level.setText("low/normal");
            holder.root.setBackgroundResource(R.color.colorGreen);

        }
        else if(article.Level >90 && article.Level <=180){
            holder.level.setText("average");
            holder.root.setBackgroundResource(R.color.yellow);
        }

        else if(article.Level >180 && article.Level <=270){
            holder.level.setText("high");
            holder.root.setBackgroundResource(R.color.colorOrange);
        }

        else if(article.Level >270 && article.Level <=360){
            holder.level.setText("very high");
            holder.root.setBackgroundResource(R.color.red);
        }

        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onShowClickListener.onShowClick(article.IdAlarmRisk);
            }
        });

    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }
}


