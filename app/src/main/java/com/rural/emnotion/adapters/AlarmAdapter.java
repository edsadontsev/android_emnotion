package com.rural.emnotion.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rural.emnotion.R;
import com.rural.emnotion.activities.EditAlarmActivity;
import com.rural.emnotion.activities.EditStationActivity;
import com.rural.emnotion.models.Alarm;

import java.util.List;

public class AlarmAdapter extends RecyclerView.Adapter<AlarmAdapter.MyViewHolder> {

    private Context mContext;
    private List<Alarm> albumList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,  time, date;
        public CardView root;
        public MyViewHolder(View view) {
            super(view);
            root = (CardView) view.findViewById(R.id.root);
            title = (TextView) view.findViewById(R.id.title);
            date = (TextView) view.findViewById(R.id.date);
            time = (TextView) view.findViewById(R.id.time);
        }
    }


    public AlarmAdapter(Context mContext, List<Alarm> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.alarm_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if (position % 4 == 0) {
            holder.root.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorPink));
        }else if (position % 4 == 1) {
            holder.root.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorGreen));
        }else if (position % 4 == 2) {
            holder.root.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorOrange));
        }else if (position % 4 == 3) {
            holder.root.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorBlue));
        }
        Alarm article = albumList.get(position);
        holder.title.setText(article.AlarmName);
        holder.date.setText(article.RegDate);
        holder.time.setText(article.RegTime);

        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, EditAlarmActivity.class);
                intent.putExtra("alarm",albumList.get(position));
                mContext.startActivity(intent);
            }
        });
    }



    @Override
    public int getItemCount() {
        return albumList.size();
    }
}
