package com.rural.emnotion.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.rural.emnotion.R;
import com.rural.emnotion.activities.ChatActivity;
import com.rural.emnotion.activities.ShopProductActivity;
import com.rural.emnotion.models.Product;
import com.rural.emnotion.models.Specialist;
import com.rural.emnotion.network.API;
import com.rural.emnotion.network.ApiService;
import com.rural.emnotion.settings.USM;
import com.rural.emnotion.util.CircleTransform;

import java.io.Serializable;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.MyViewHolder> {

    private Context mContext;
    private List<Product> albumList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,  subTitle, price, more;
        public ImageView image;
        public Button button;

        public MyViewHolder(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            subTitle = (TextView) view.findViewById(R.id.sub_title);
            price = (TextView) view.findViewById(R.id.price);
            more = (TextView) view.findViewById(R.id.more);
            button = (Button) view.findViewById(R.id.btn_buy);
        }
    }


    public ShopAdapter(Context mContext, List<Product> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    public List<Product> getAlbumList() {
        return albumList;
    }

    @Override
    public ShopAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_shop, parent, false);

        return new ShopAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ShopAdapter.MyViewHolder holder, final int position) {
        final Product article = albumList.get(position);
        holder.title.setText(article.Name);
        holder.subTitle.setText(article.Instruction);
        holder.price.setText(article.Price + "$");

        Glide.with(mContext).load(ApiService.PRODUCTS_IMAGE_URL + article.PhotoName)
                .bitmapTransform(new CircleTransform(mContext))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.image);

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(article.PaymentURL));
                mContext.startActivity(i);
            }
        });

        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("product",article);
                Intent i = new Intent(mContext, ShopProductActivity.class);
                i.putExtras(bundle);
                mContext.startActivity(i);
            }
        });
    }



    @Override
    public int getItemCount() {
        return albumList.size();
    }
}


