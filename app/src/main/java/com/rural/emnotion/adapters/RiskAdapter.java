package com.rural.emnotion.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rural.emnotion.R;
import com.rural.emnotion.activities.EditAlarmActivity;
import com.rural.emnotion.activities.EditRiskActivity;
import com.rural.emnotion.activities.EditStationActivity;
import com.rural.emnotion.models.Risk;
import com.rural.emnotion.models.Station;

import java.util.List;

public class RiskAdapter extends RecyclerView.Adapter<RiskAdapter.MyViewHolder> {

    private Context mContext;
    private List<Risk> albumList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,  time, date;
        public CardView root;

        public MyViewHolder(View view) {
            super(view);
            root = (CardView) view.findViewById(R.id.root);
            title = (TextView) view.findViewById(R.id.title);
            date = (TextView) view.findViewById(R.id.date);
            time = (TextView) view.findViewById(R.id.time);
        }
    }


    public RiskAdapter(Context mContext, List<Risk> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public RiskAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_risk, parent, false);

        return new RiskAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RiskAdapter.MyViewHolder holder, final int position) {
        if (position % 4 == 0) {
            holder.root.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorPink));
        }else if (position % 4 == 1) {
            holder.root.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorGreen));
        }else if (position % 4 == 2) {
            holder.root.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorOrange));
        }else if (position % 4 == 3) {
            holder.root.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorBlue));
        }
        Risk article = albumList.get(position);
        holder.title.setText(article.AlarmRiskName);
        holder.date.setText(article.RegDate);
        holder.time.setText(article.RegTime);

        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, EditRiskActivity.class);
                intent.putExtra("risk",albumList.get(position));
                mContext.startActivity(intent);
            }
        });
    }



    @Override
    public int getItemCount() {
        return albumList.size();
    }
}

