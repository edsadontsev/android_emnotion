package com.rural.emnotion.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rural.emnotion.R;
import com.rural.emnotion.models.Alarm;

import java.util.List;

public class ParamsAdapter extends RecyclerView.Adapter<ParamsAdapter.MyViewHolder> {

    private Context mContext;
    private List<Alarm.AlarmParams> albumList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,  min, max;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            min = (TextView) view.findViewById(R.id.min);
            max = (TextView) view.findViewById(R.id.max);
        }
    }


    public ParamsAdapter(Context mContext, List<Alarm.AlarmParams> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.params_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Alarm.AlarmParams article = albumList.get(position);
        holder.title.setText(article.ParameterName);
        holder.min.setText(article.Min);
        holder.max.setText(article.Max);
    }



    @Override
    public int getItemCount() {
        return albumList.size();
    }
}
