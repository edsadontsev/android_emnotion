package com.rural.emnotion.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.rural.emnotion.R;
import com.rural.emnotion.activities.ChatActivity;
import com.rural.emnotion.models.Message;
import com.rural.emnotion.models.Specialist;
import com.rural.emnotion.network.API;
import com.rural.emnotion.network.ApiService;
import com.rural.emnotion.settings.USM;
import com.rural.emnotion.util.CircleTransform;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.MyViewHolder> {

    private Context mContext;
    private List<Message> albumList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,  message, date;
        public ImageView icon;
        public MyViewHolder(View view) {
            super(view);
            icon = (ImageView) view.findViewById(R.id.icon);
            name = (TextView) view.findViewById(R.id.name);
            date = (TextView) view.findViewById(R.id.date);
            message = (TextView) view.findViewById(R.id.message);
        }
    }


    public ChatsAdapter(Context mContext, List<Message> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public ChatsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat, parent, false);

        return new ChatsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ChatsAdapter.MyViewHolder holder, final int position) {
        final Message article = albumList.get(position);
        if(article.IdUser.equals(USM.getUserID()))
            holder.name.setText(article.NameTo);
        else
            holder.name.setText(article.NameFrom);
        holder.date.setText(article.MailDate + " " + article.MailTime);
        holder.message.setText(article.MailText);

        String photo = article.PhotoNameFrom == null ? article.PhotoNameTo : article.PhotoNameFrom;
        Glide.with(mContext).load(ApiService.IMAGE_URL + photo)
                .bitmapTransform(new CircleTransform(mContext))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.icon);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ChatActivity.class);
                if(article.IdUserTo.equals(USM.getUserID()))
                    intent.putExtra("chatId",article.IdUser);
                else intent.putExtra("chatId",article.IdUserTo);
                mContext.startActivity(intent);
            }
        });
    }



    @Override
    public int getItemCount() {
        return albumList.size();
    }
}


