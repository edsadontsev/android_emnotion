package com.rural.emnotion.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.rural.emnotion.R;
import com.rural.emnotion.activities.ChatActivity;
import com.rural.emnotion.activities.SpecShopProductActivity;
import com.rural.emnotion.models.Specialist;
import com.rural.emnotion.network.API;
import com.rural.emnotion.network.ApiService;
import com.rural.emnotion.settings.USM;
import com.rural.emnotion.util.CircleTransform;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Response;

public class SpecialistAdapter extends RecyclerView.Adapter<SpecialistAdapter.MyViewHolder> {

    private Context mContext;
    private List<Specialist> albumList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,  skills, location;
        public ImageView add, icon, email,shop;
        public MyViewHolder(View view) {
            super(view);
            shop = (ImageView) view.findViewById(R.id.shop);
            add = (ImageView) view.findViewById(R.id.add);
            icon = (ImageView) view.findViewById(R.id.icon);
            email = (ImageView) view.findViewById(R.id.email);
            name = (TextView) view.findViewById(R.id.name);
            location = (TextView) view.findViewById(R.id.location);
            skills = (TextView) view.findViewById(R.id.skills);
        }
    }


    public SpecialistAdapter(Context mContext, List<Specialist> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public SpecialistAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_specialist, parent, false);

        return new SpecialistAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SpecialistAdapter.MyViewHolder holder, final int position) {
        final Specialist article = albumList.get(position);
        holder.name.setText(article.getName());
        holder.location.setText(article.getFullAddress());
        holder.skills.setText(article.getSpecialization());
        holder.add.setImageResource(R.drawable.ic_delete);

        Glide.with(mContext).load(ApiService.IMAGE_URL+article.getPhotoName())
                .bitmapTransform(new CircleTransform(mContext))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.user_placeholder)
                .into(holder.icon);

        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                USM.init(mContext);
                retrofit2.Call<ResponseBody> addSpec = API.getApiRequestService().deleteSpecialist(USM.getUserToken(),
                        USM.getUserID(), article.getIdUserCompany());
                addSpec.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(retrofit2.Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful()){
                            albumList.remove(position);
                            notifyDataSetChanged();
                        }
                    }
                    @Override
                    public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {}
                });
            }
        });

        holder.email.setVisibility(View.VISIBLE);
        holder.email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ChatActivity.class);
                intent.putExtra("chatId",article.getIdUser());
                mContext.startActivity(intent);
            }
        });

        holder.shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, SpecShopProductActivity.class);
                intent.putExtra("name",article.getName());
                intent.putExtra("id",article.getIdUser());
                mContext.startActivity(intent);
            }
        });
    }



    @Override
    public int getItemCount() {
        return albumList.size();
    }
}

