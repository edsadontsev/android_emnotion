package com.rural.emnotion.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rural.emnotion.R;
import com.rural.emnotion.activities.EditProductActivity;
import com.rural.emnotion.activities.EditStationActivity;
import com.rural.emnotion.models.Product;
import com.rural.emnotion.models.Station;

import java.util.List;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.MyViewHolder> {

    private Context mContext;
    private List<Product> albumList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public CardView root;

        public MyViewHolder(View view) {
            super(view);
            root = (CardView) view.findViewById(R.id.root);
            title = (TextView) view.findViewById(R.id.title);
        }
    }


    public ProductsAdapter(Context mContext, List<Product> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public ProductsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product, parent, false);

        return new ProductsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ProductsAdapter.MyViewHolder holder, final int position) {
        if (position % 4 == 0) {
            holder.root.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorPink));
        }else if (position % 4 == 1) {
            holder.root.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorGreen));
        }else if (position % 4 == 2) {
            holder.root.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorOrange));
        }else if (position % 4 == 3) {
            holder.root.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorBlue));
        }
        Product product = albumList.get(position);
        holder.title.setText(product.Name);

        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, EditProductActivity.class);
                intent.putExtra("product",albumList.get(position));
                mContext.startActivity(intent);
            }
        });
        //    holder.time.setText(article.Address);
    }



    @Override
    public int getItemCount() {
        return albumList.size();
    }
}
