package com.rural.emnotion.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.rural.emnotion.R;
import com.rural.emnotion.activities.ChatActivity;
import com.rural.emnotion.models.Specialist;
import com.rural.emnotion.network.API;
import com.rural.emnotion.network.ApiService;
import com.rural.emnotion.settings.USM;
import com.rural.emnotion.util.CircleTransform;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Response;

public class ClientsAdapter extends RecyclerView.Adapter<ClientsAdapter.MyViewHolder> {

    private Context mContext;
    private List<Specialist> albumList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,  skills, location;
        public ImageView icon, email;
        public MyViewHolder(View view) {
            super(view);
            icon = (ImageView) view.findViewById(R.id.icon);
            email = (ImageView) view.findViewById(R.id.email);
            name = (TextView) view.findViewById(R.id.name);
            location = (TextView) view.findViewById(R.id.location);
            skills = (TextView) view.findViewById(R.id.skills);
        }
    }


    public ClientsAdapter(Context mContext, List<Specialist> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public ClientsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_client, parent, false);

        return new ClientsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ClientsAdapter.MyViewHolder holder, final int position) {
        final Specialist article = albumList.get(position);
        holder.name.setText(article.getName());
        holder.location.setText(article.getFullAddress());
        holder.skills.setText(article.getSpecialization());

        Glide.with(mContext).load(ApiService.IMAGE_URL+article.getPhotoName())
                .bitmapTransform(new CircleTransform(mContext))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.user_placeholder)
                .into(holder.icon);

        holder.email.setVisibility(View.VISIBLE);
        holder.email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ChatActivity.class);
                intent.putExtra("chatId",article.getIdUser());
                mContext.startActivity(intent);
            }
        });
    }



    @Override
    public int getItemCount() {
        return albumList.size();
    }
}


