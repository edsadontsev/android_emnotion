package com.rural.emnotion.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.rural.emnotion.R;
import com.rural.emnotion.models.Message;
import com.rural.emnotion.network.ApiService;
import com.rural.emnotion.settings.USM;
import com.rural.emnotion.util.CircleTransform;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder> {

    private Context mContext;
    private List<Message> albumList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,  message, date;
        public ImageView icon;
        public MyViewHolder(View view) {
            super(view);
            icon = (ImageView) view.findViewById(R.id.icon);
            name = (TextView) view.findViewById(R.id.name);
            date = (TextView) view.findViewById(R.id.date);
            message = (TextView) view.findViewById(R.id.message);
        }
    }


    public MessageAdapter(Context mContext, List<Message> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public MessageAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user_message, parent, false);

        return new MessageAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MessageAdapter.MyViewHolder holder, final int position) {
        final Message article = albumList.get(position);
        holder.name.setText(article.NameFrom);
        holder.date.setText(article.MailDate + "\n" + article.MailTime);
        holder.message.setText(article.MailText);

        if(article.PhotoNameFrom == null){
            holder.icon.setImageResource(R.drawable.ic_user_placeholder);
        }
        else Glide.with(mContext).load(ApiService.IMAGE_URL + article.PhotoNameFrom)
                .bitmapTransform(new CircleTransform(mContext))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.icon);
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }
}



