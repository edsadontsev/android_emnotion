package com.rural.emnotion.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {

    public static String getDate(Date date){
        String year;
        String month;
        String day;

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        year = calendar.get(Calendar.YEAR)+ "";
        if(calendar.get(Calendar.MONTH) + 1<10)
            month = "0" + (calendar.get(Calendar.MONTH) + 1);
        else month = (calendar.get(Calendar.MONTH) + 1)+ "";

        if(calendar.get(Calendar.DAY_OF_MONTH) < 10)
            day = "0" + calendar.get(Calendar.DAY_OF_MONTH);
        else day = calendar.get(Calendar.DAY_OF_MONTH) +"";

        return year + "-" + month + "-" + day;
    }

    public static String getTitleDate(Date date){
        String year;
        String month;
        String day;

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        year = calendar.get(Calendar.YEAR)+ "";
        if(calendar.get(Calendar.MONTH) + 1<10)
            month = "0" + (calendar.get(Calendar.MONTH) + 1);
        else month = (calendar.get(Calendar.MONTH) + 1)+ "";

        if(calendar.get(Calendar.DAY_OF_MONTH) < 10)
            day = "0" + calendar.get(Calendar.DAY_OF_MONTH);
        else day = calendar.get(Calendar.DAY_OF_MONTH) +"";

        return day + "." + month + "." + year;
    }
}
