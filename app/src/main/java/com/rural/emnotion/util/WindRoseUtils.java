package com.rural.emnotion.util;

import android.util.Log;

import com.rural.emnotion.models.Wind;

import java.util.ArrayList;
import java.util.List;

public class WindRoseUtils {

    public static List<Wind> transformArray(List<Wind> list){
        List<Wind> sortedList = new ArrayList<>();
        for(int j=0;j<list.size();j++){
            sortedList.add(new Wind());
        }

        Log.wtf("size",list.size()+"");

        for(int i=0;i<list.size();i++){
            if(list.get(i).WindDir.equals("N")) sortedList.set(0,list.get(i));
            else if(list.get(i).WindDir.equals("NNE")) sortedList.set(1,list.get(i));
            else if(list.get(i).WindDir.equals("NE")) sortedList.set(2,list.get(i));
            else if(list.get(i).WindDir.equals("ENE")) sortedList.set(3,list.get(i));
            else if(list.get(i).WindDir.equals("E")) sortedList.set(4,list.get(i));
            else if(list.get(i).WindDir.equals("ESE")) sortedList.set(5,list.get(i));
            else if(list.get(i).WindDir.equals("SE")) sortedList.set(6,list.get(i));
            else if(list.get(i).WindDir.equals("SSE")) sortedList.set(7,list.get(i));
            else if(list.get(i).WindDir.equals("S")) sortedList.set(8,list.get(i));
            else if(list.get(i).WindDir.equals("SSW")) sortedList.set(9,list.get(i));
            else if(list.get(i).WindDir.equals("SW")) sortedList.set(10,list.get(i));
            else if(list.get(i).WindDir.equals("WSW")) sortedList.set(11,list.get(i));
            else if(list.get(i).WindDir.equals("W")) sortedList.set(12,list.get(i));
            else if(list.get(i).WindDir.equals("WNW")) sortedList.set(13,list.get(i));
            else if(list.get(i).WindDir.equals("NW")) sortedList.set(14,list.get(i));
            else if(list.get(i).WindDir.equals("NNW")) sortedList.set(15,list.get(i));
        }
        Log.wtf("size",sortedList.size()+"");

        return sortedList;
    }
}
