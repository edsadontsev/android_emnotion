package com.rural.emnotion.util;

/**
 * Created by user on 11-Sep-17.
 */

public class Variables {

    public static String humidityDim="%";
    public static String windDim="m/sec";
    public static String degreeDim="°";
    public static String radiationDim="W/m2";
    public static String moistureDim="cB";
}
