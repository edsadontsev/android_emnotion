package com.rural.emnotion.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.rural.emnotion.R;

/**
 * Created by Administrator on 06.12.2017.
 */

public class AlertDialogsUtils {


    public static void showAlert(final Context context, String alert_title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(alert_title)
                .setMessage(message)
                .setIcon(R.drawable.logo_2)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
