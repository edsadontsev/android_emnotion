package com.rural.emnotion;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.rural.emnotion.fcm.Config;
import com.rural.emnotion.fragments.climat_data.AlarmFragment;
import com.rural.emnotion.fragments.climat_data.ClientsFragment;
import com.rural.emnotion.fragments.climat_data.CreateSpecialistFragment;
import com.rural.emnotion.fragments.climat_data.ForecastFragment;
import com.rural.emnotion.fragments.climat_data.MessengerFragment;
import com.rural.emnotion.fragments.climat_data.ProductsFragment;
import com.rural.emnotion.fragments.climat_data.RiskFragment;
import com.rural.emnotion.fragments.climat_data.SearchSpecialistFragment;
import com.rural.emnotion.fragments.climat_data.ShopFragment;
import com.rural.emnotion.fragments.climat_data.SpecialistsFragment;
import com.rural.emnotion.fragments.climat_data.climat.BarometricFragment;
import com.rural.emnotion.fragments.climat_data.climat.HumidityFragment;
import com.rural.emnotion.fragments.climat_data.climat.IndexesFragment;
import com.rural.emnotion.fragments.climat_data.climat.PrecipitationFragment;
import com.rural.emnotion.fragments.climat_data.climat.RadiationFragment;
import com.rural.emnotion.fragments.climat_data.SettingFragment;
import com.rural.emnotion.fragments.climat_data.climat.SoilFragment;
import com.rural.emnotion.fragments.climat_data.StationFragment;
import com.rural.emnotion.fragments.climat_data.climat.TemperatureFragment;
import com.rural.emnotion.fragments.climat_data.climat.WindFragment;
import com.rural.emnotion.models.Station;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;
import com.rural.emnotion.util.AlertDialogsUtils;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ImageButton fab, sos, notification, settings;
    private FrameLayout fabMenu;
    private boolean isFabMenuVisible = false;
    private TextView userName;
    private Spinner stationsView;

    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        fab = (ImageButton) findViewById(R.id.fab);
        fabMenu = (FrameLayout) findViewById(R.id.fab_menu);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFabMenuVisible) {
                    fabMenu.setVisibility(View.GONE);
                    isFabMenuVisible = false;
                } else {
                    fabMenu.setVisibility(View.VISIBLE);
                    isFabMenuVisible = true;
                }
            }
        });

        stationsView = (Spinner) findViewById(R.id.stations);
        stationsView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                USM.init(MainActivity.this);
                USM.setStation(stations.get(i).IdStation);
                displayView(currItem);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        USM.init(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        userName = (TextView) header.findViewById(R.id.name);
        userName.setText(USM.getUserEmail());

        sos = (ImageButton) findViewById(R.id.sos);
        sos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "sos!", Toast.LENGTH_SHORT).show();
                fab.performClick();
            }
        });
        notification = (ImageButton) findViewById(R.id.notification);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "notification!", Toast.LENGTH_SHORT).show();
                fab.performClick();
            }
        });

        settings = (ImageButton) findViewById(R.id.settings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fab.performClick();
                displayView(11);
            }
        });

        getUsersStations();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    Toast.makeText(context, displayFirebaseRegId(), Toast.LENGTH_SHORT).show();
                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    openNotifScreen();
                }
            }
        };
        FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
        displayFirebaseRegId();
    }

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    public void openNotifScreen() {
//        String message = intent.getStringExtra("message");
//        String title = intent.getStringExtra("title");
//        String imageUrl = intent.getStringExtra("imageUrl");
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private String displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        Log.d("Firebase", "Firebase reg id: " + regId);
        if (regId != null) {
            Call<ResponseBody> updatePush =
                    API.getApiRequestService().updatePush(
                            USM.getUserID(),
                            regId,
                            USM.getUserToken());
//            updatePush.enqueue(new Callback<ResponseBody>() {
//                @Override
//                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    Log.d("updatePush ","onResponse");
//                }
//
//                @Override
//                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                }
//            });
        }
        return regId;
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void initSpinnerAdapter(ArrayList<String> list) {
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MainActivity.this,
                R.layout.spinner_item, list);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        // attaching data adapter to spinner
        stationsView.setAdapter(dataAdapter);
        USM.init(MainActivity.this);
        stationsView.setSelection(getStationIndex(USM.getStation()));
        displayView(0);
    }

    private int getStationIndex(int station_id) {
        int index = 0;
        for (int i = 0; i < stations.size(); i++) {
            if (station_id == stations.get(i).IdStation) {
                index = i;
            }
        }
        return index;
    }

    public static List<Station> stations = new ArrayList<>();

    private void getUsersStations() {
        Call<List<Station>> getUsersStations = API.getApiRequestService().getUsersStations(USM.getUserID(),
                USM.getUserToken());
        getUsersStations.enqueue(new Callback<List<Station>>() {
            @Override
            public void onResponse(Call<List<Station>> call, Response<List<Station>> response) {
                stations = response.body();
                //"Status":"token failed"
                if (stations.size() > 0) {
                    if (stations.get(0).Status.equals("token failed")) {
                        USM.init(MainActivity.this);
                        USM.logout(MainActivity.this);
                    } else {
                        ArrayList<String> listSp = new ArrayList<>();
                        for (int i = 0; i < stations.size(); i++) {
                            listSp.add(stations.get(i).Name);
                        }
                        initSpinnerAdapter(listSp);
                    }
                } else {
                    AlertDialogsUtils.showAlert(MainActivity.this, "No active stations!", "Please contact administrator!");
                    displayView(0);
                }
            }

            @Override
            public void onFailure(Call<List<Station>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Menu m = navigationView.getMenu();

        if (id == R.id.knowledge) {
            boolean b=!m.findItem(R.id.nav_shop).isVisible();
            m.findItem(R.id.nav_shop).setVisible(b);
            return true;
        }

        else if (id == R.id.weather_machine) {
            boolean b=!m.findItem(R.id.nav_forecast).isVisible();
            m.findItem(R.id.nav_forecast).setVisible(b);
            m.findItem(R.id.nav_temp).setVisible(b);
            m.findItem(R.id.nav_humid).setVisible(b);
            m.findItem(R.id.nav_precip).setVisible(b);
            m.findItem(R.id.nav_barom).setVisible(b);
            m.findItem(R.id.nav_wind).setVisible(b);
            m.findItem(R.id.nav_radiation).setVisible(b);
            m.findItem(R.id.nav_ind).setVisible(b);
            m.findItem(R.id.nav_soil).setVisible(b);
            return true;
        }
        else if(id == R.id.social_network){
            boolean b=!m.findItem(R.id.nav_contacts).isVisible();
            m.findItem(R.id.nav_contacts).setVisible(b);
            m.findItem(R.id.nav_chat).setVisible(b);
            m.findItem(R.id.nav_search).setVisible(b);
            m.findItem(R.id.nav_clients).setVisible(b);
            return true;
        }
        else if(id == R.id.edit){
            boolean b=!m.findItem(R.id.nav_alarms).isVisible();
            m.findItem(R.id.nav_alarms).setVisible(b);
            m.findItem(R.id.nav_admin).setVisible(b);
            m.findItem(R.id.nav_spec_reg).setVisible(b);
            m.findItem(R.id.nav_catalog).setVisible(b);
            m.findItem(R.id.nav_risk).setVisible(b);
            return true;
        }

        if (id == R.id.nav_temp) {
            // Handle the camera action
            displayView(0);
        } else if (id == R.id.nav_humid) {
            displayView(1);
        } else if (id == R.id.nav_precip) {
            displayView(2);
        } else if (id == R.id.nav_barom) {
            displayView(3);
        } else if (id == R.id.nav_wind) {
            displayView(4);
        } else if (id == R.id.nav_radiation) {
            displayView(5);
        } else if (id == R.id.nav_ind) {
            displayView(6);
        } else if (id == R.id.nav_soil) {
            displayView(7);
        } else if (id == R.id.nav_alarms) {
            displayView(8);
        } else if (id == R.id.nav_admin) {
            displayView(9);
        } else if (id == R.id.nav_logout) {
            displayView(10);
        } else if (id == R.id.nav_risk) {
            displayView(13);
        } else if (id == R.id.nav_forecast) {
            displayView(14);
        } else if (id == R.id.nav_contacts) {
            displayView(15);
        } else if (id == R.id.nav_chat) {
            displayView(16);
        } else if (id == R.id.nav_search) {
            displayView(17);
        } else if (id == R.id.nav_catalog) {
            displayView(12);
        } else if (id == R.id.nav_clients) {
            displayView(18);
        }else if (id == R.id.nav_spec_reg) {
            displayView(19);
        }else if (id == R.id.nav_shop) {
            displayView(20);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private int currItem = -1;

    public void displayView(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new TemperatureFragment();
                currItem = 0;
                // setTitle(getString(R.string.menu_temp));
                break;
            case 1:
                fragment = new HumidityFragment();
                currItem = 1;
                //  setTitle(getString(R.string.menu_humid));
                break;
            case 2:
                fragment = new PrecipitationFragment();
                currItem = 2;
                // setTitle(getString(R.string.menu_precip));
                break;
            case 3:
                fragment = new BarometricFragment();
                currItem = 3;
                // setTitle(getString(R.string.menu_barom));
                break;
            case 4:
                fragment = new WindFragment();
                currItem = 4;
                // setTitle(getString(R.string.menu_wind));
                break;
            case 5:
                fragment = new RadiationFragment();
                currItem = 5;
                //  setTitle(getString(R.string.menu_radiation));
                break;
            case 6:
                fragment = new IndexesFragment();
                currItem = 6;
                //   setTitle(getString(R.string.menu_ind));
                break;
            case 7:
                fragment = new SoilFragment();
                currItem = 7;
                // setTitle(getString(R.string.menu_soil));
                break;
            case 8:
                fragment = new AlarmFragment();
                currItem = 8;
                //  setTitle(getString(R.string.menu_alarms));
                break;
            case 9:
                fragment = new StationFragment();
                currItem = 9;
                // setTitle(getString(R.string.menu_admin));
                break;
            case 11:
                fragment = new SettingFragment();
                currItem = 11;
                // setTitle(getString(R.string.menu_admin));
                break;
            case 10:
                currItem = 10;
                USM.init(this);
                USM.logout(this);
                break;

            case 12:
                currItem = 12;
                //products
                fragment = new ProductsFragment();
                break;
            case 13:
                currItem = 13;
                //Risk editor
                fragment = new RiskFragment();
                break;
            case 14:
                currItem = 14;
                //forecast
                fragment = new ForecastFragment();
                break;
            case 15:
                currItem = 15;
                //Contacts
                fragment = new SpecialistsFragment();
                break;
            case 16:
                currItem = 16;
                fragment = new MessengerFragment();
                //Chat
                break;
            case 17:
                currItem = 17;
                //Specialist
                fragment = new SearchSpecialistFragment();
                break;
            case 18:
                currItem = 18;
                //Specialist
                fragment = new ClientsFragment();
                break;

            case 19:
                currItem = 19;
                fragment = new CreateSpecialistFragment();
                break;

            case 20:
                currItem = 20;
                fragment = new ShopFragment();
                break;

            default:
                break;
        }
        if (fragment != null) {
            if (currItem == 11) {
                replaceFragment(fragment);
            } else {
                replaceFragmentWithoutBackStack(fragment);
            }
        }
    }


    public void popBackStackSupportFragmentManager() {
        getSupportFragmentManager().popBackStack();
    }

    public void replaceFragment(FragmentManager fragmentManager, Fragment fragment) {
        fragmentManager.beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();
    }

    public void addFragment(FragmentManager fragmentManager, Fragment fragment) {
        fragmentManager.beginTransaction().add(R.id.container, fragment).commit();
    }

    public void replaceFragmentWithoutBackStack(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }


    public void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();
    }

    public void addFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().add(R.id.container, fragment).commit();
    }


    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void showSpinner(boolean show) {
        if (show) {
            stationsView.setVisibility(View.VISIBLE);
        } else {
            stationsView.setVisibility(View.GONE);
        }
    }

}
