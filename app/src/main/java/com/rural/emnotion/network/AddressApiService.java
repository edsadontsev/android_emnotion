package com.rural.emnotion.network;

import com.rural.emnotion.models.AddressModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

interface AddressApiService {

    @GET("maps/api/geocode/json")
    Call<AddressModel> getAddress(@Query("latlng") String latLng,
                                  @Query("sensor") boolean sensor);
}
