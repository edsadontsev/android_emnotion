package com.rural.emnotion.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.ContextMenu;

import com.rural.emnotion.MainActivity;
import com.rural.emnotion.app.Emnotion;
import com.rural.emnotion.settings.USM;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Administrator on 6/19/2017.
 */

public class API {
    public static String getResponse(InputStream byteStream){
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        reader = new BufferedReader(new InputStreamReader(byteStream));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String result = sb.toString();
        if (checkToken(result, Emnotion.getInstance().getApplicationContext())){
            return result;
        }else {
            return "";
        }

    }

    public static boolean checkToken(String result, Context mContext){
        boolean validToken = true;
        if (result.contains("token failed")){
            validToken = false;
            USM.init(mContext);
            USM.logout(mContext);
        }
        return validToken;
    }



    public static final int GENERAL_STATION = 9;
    private static final int GENERAL_TIMEOUT = 50;  // seconds
    private static final int CONNECT_TIMEOUT = GENERAL_TIMEOUT;
    private static final int WRITE_TIMEOUT = GENERAL_TIMEOUT;
    private static final int READ_TIMEOUT = GENERAL_TIMEOUT;
    private static final OkHttpClient CLIENT = getUnsafeOkHttpClient();
    private static OkHttpClient.Builder builder;


    static {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder = CLIENT.newBuilder();
        builder.addInterceptor(logging);
        builder.connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS);
    }

    @NonNull
    public static ApiService getApiRequestService() {
        return getRetrofitDefault().create(ApiService.class);
    }

    @NonNull
    private static Retrofit getRetrofitDefault() {
        return new Retrofit.Builder()
                .baseUrl(ApiService.HTTP_URL_REGION)
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build();
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[0];
                }
            }};
            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustAllCerts,
                    new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext
                    .getSocketFactory();
            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient = okHttpClient.newBuilder()
                    .sslSocketFactory(sslSocketFactory)
                    .hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER).build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
