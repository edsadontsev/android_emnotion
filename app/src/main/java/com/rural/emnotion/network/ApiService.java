package com.rural.emnotion.network;

import com.rural.emnotion.models.Alarm;
import com.rural.emnotion.models.Country;
import com.rural.emnotion.models.Impact;
import com.rural.emnotion.models.Language;
import com.rural.emnotion.models.Message;
import com.rural.emnotion.models.Product;
import com.rural.emnotion.models.Risk;
import com.rural.emnotion.models.Specialist;
import com.rural.emnotion.models.Station;
import com.rural.emnotion.network.resposes.RegResponse;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Administrator on 31.03.2017.
 */

public interface ApiService {

    String HTTP_URL_REGION = "http://api.emnotion.net/";
    String HTTP_URL_RURAL = "http://137.74.95.61/rural.emnotion.com/";
    String HTTP_URL_URBAN = "http://137.74.95.61/urban.emnotion.com/";
    String IMAGE_URL = "https://emnotion.com/Galery/Users/";
    String PRODUCTS_IMAGE_URL = "https://emnotion.com/Galery/Products/";

    @FormUrlEncoded
    @POST("User/CreateUser")
    Call<RegResponse> regUser(@Field("Email") String Email, @Field("Pass") String Pass, @Field("Phone") String Phone, @Field("UserType") String UserType);

    @FormUrlEncoded
    @POST("User/VerifyUser")
    Call<RegResponse> loginUser(@Field("Email") String Email, @Field("Pass") String Pass);

    @FormUrlEncoded
    @POST("User/CreateUserSocial")
    Call<RegResponse> regUserSocial(@Field("Email") String Email, @Field("SocialNetwork") String SocialNetwork, @Field("ProviderKey") String ProviderKey, @Field("UserType") String UserType);

    @FormUrlEncoded
    @POST("User/UpdatePush")
    Call<ResponseBody> updatePush(@Field("IdUser") String IdUser, @Field("RegistrationId") String RegistrationId, @Field("Token") String Token);

    @GET("User/GetList/{user_id}")
    Call<ResponseBody> getUserById(@Path("user_id") String user_id);

    @GET("Station/GetListUser/{user_id}/{token}")
    Call<List<Station>> getUsersStations(@Path("user_id") String user_id, @Path("token") String token);

    @FormUrlEncoded
    @POST("Station/InsertStation")
    Call<Void> createStation(@Field("Token") String token,@Field("IdUser") String idUser,
                                      @Field("Name") String name,
                                      @Field("IdStationType") int stationType,
                                      @Field("IpAddress") String ip,
                                      @Field("Country") String country,
                                      @Field("City") String city,
                                      @Field("Region") String region,
                                      @Field("Address") String address,
                                      @Field("Latitude") double lat,
                                      @Field("Longitude") double lng,
                                      @Field("PrivateAccess") boolean access,
                                      @Field("IdStationUsage") int stationUsage,
                                      @Field("Description") String description,
                                      @Field("IsActiv") boolean isActive,
                                      @Field("Elevation") String elevation);

    @FormUrlEncoded
    @POST("Station/UpdateStation")
    Call<Void> updateStation(@Field("Token") String token,@Field("IdUser") String idUser,
                                      @Field("IdStation") int id,
                                      @Field("Name") String name,
                                      @Field("IdStationType") int stationType,
                                      @Field("IpAddress") String ip,
                                      @Field("Country") String country,
                                      @Field("City") String city,
                                      @Field("Region") String region,
                                      @Field("Address") String address,
                                      @Field("Latitude") double lat,
                                      @Field("Longitude") double lng,
                                      @Field("PrivateAccess") boolean access,
                                      @Field("IdStationUsage") int stationUsage,
                                      @Field("Description") String description,
                                      @Field("IsActiv") boolean isActive,
                                      @Field("Elevation") String elevation);

    @FormUrlEncoded
    @POST("Station/DeleteStation")
    Call<Void> deleteStation(@Field("Token") String token,
                                      @Field("IdStation") int id,
                                      @Field("IdUser") String idUser);


    //[Route("AlarmUser/GetList/{IdUser}/{Token}/{IdStation}")]
    @GET("AlarmUser/GetList/{user_id}/{token}/{station_id}")
    Call<List<Alarm>> getAlarmUser(@Path("user_id") String user_id, @Path("token") String token, @Path("station_id") String station_id);


    @GET("AlarmUser/GetAlarmIdAlarm/{IdUser}/{IdUserAlarm}/{Token}")
    Call<List<Alarm>> getAlarmById(@Path("IdUser") String user_id, @Path("IdUserAlarm") int id, @Path("Token") String token);


    //[Route("AlarmUser/InsertCompose")]
    @POST("AlarmUser/InsertCompose")
    Call<Void> insertCompose(@Body RequestBody requestBody);

    @POST("AlarmUser/UpdateAlarm")
    Call<Void> updateAlarm(@Body RequestBody requestBody);

    @FormUrlEncoded
    @POST("AlarmUser/DeleteAlarm")
    Call<Void> deleteAlarm(@Field("Token") String token,@Field("IdUserAlarm") int id,
                           @Field("IdUser") String idUser);



    // Temperature ---------------------------------------------------------------------------------

    @GET("User/GetUserSetting/{user_id}/{token}")
    Call<ResponseBody> getUserSetting(@Path("user_id") String user_id, @Path("token") String token);


    @GET("Temperature/Temp/{station_id}/{user_id}/{token}")
    Call<ResponseBody> getTemperature(@Path("station_id") String station_id,
                                      @Path("user_id") String user_id, @Path("token") String token);

    @GET("Temperature/TempAvgMaxMinDay/{station_id}/{user_id}/{token}/{interval}")
    Call<ResponseBody> TempAvgMaxMinDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                        @Path("token") String token, @Path("interval") String interval);

    @GET("Temperature/TempAvgMaxMinNight/{station_id}/{user_id}/{token}/{interval}")
    Call<ResponseBody> TempAvgMaxMinNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                          @Path("token") String token, @Path("interval") String interval);

    @GET("Temperature/TempAvgMaxMinDay/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> TempAvgMaxMinFromToDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                              @Path("token") String token, @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    @GET("Temperature/TempAvgMaxMinNight/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> TempAvgMaxMinFromToNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                                @Path("token") String token, @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    @GET("Temperature/GraphicAvgDay/{station_id}/{user_id}/{token}/{interval}")
    Call<ResponseBody> GraphicAvgDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                     @Path("token") String token, @Path("interval") String Interval);

    @GET("Temperature/GraphicAvgNight/{station_id}/{user_id}/{token}/{interval}")
    Call<ResponseBody> GraphicAvgNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                       @Path("token") String token, @Path("interval") String Interval);

    @GET("Temperature/GraphicMaxDay/{station_id}/{user_id}/{token}/{interval}")
    Call<ResponseBody> GraphicMaxDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                     @Path("token") String token, @Path("interval") String Interval);

    @GET("Temperature/GraphicMaxNight/{station_id}/{user_id}/{token}/{interval}")
    Call<ResponseBody> GraphicMaxNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                       @Path("token") String token, @Path("interval") String Interval);

    @GET("Temperature/GraphicMinDay/{station_id}/{user_id}/{token}/{interval}")
    Call<ResponseBody> GraphicMinDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                     @Path("token") String token, @Path("interval") String Interval);

    @GET("Temperature/GraphicMinNight/{station_id}/{user_id}/{token}/{interval}")
    Call<ResponseBody> GraphicMinNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                       @Path("token") String token, @Path("interval") String Interval);

    @GET("Temperature/GraphicFromToDay/{station_id}/{user_id}/{token}/{startdate}/{enddate}")
    Call<ResponseBody> GraphicFromToDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                        @Path("token") String token, @Path("startdate") String startdate, @Path("enddate") String enddate);

    @GET("Temperature/GraphicFromToNight/{station_id}/{user_id}/{token}/{startdate}/{enddate}")
    Call<ResponseBody> GraphicFromToNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                          @Path("token") String token, @Path("startdate") String startdate, @Path("enddate") String enddate);

    // Humidity ---------------------------------------------------------------------------------

    @GET("Humidity/Hum/{station_id}/{user_id}/{token}")
    Call<ResponseBody> Humidity(@Path("station_id") String station_id, @Path("user_id") String user_id, @Path("token") String token);


    @GET("Humidity/HumAvgMaxMinDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> HumAvgMaxMinDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                       @Path("token") String token, @Path("Interval") String Interval);

    @GET("Humidity/HumAvgMaxMinNight/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> HumAvgMaxMinNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                         @Path("token") String token, @Path("Interval") String Interval);

    @GET("Humidity/HumAvgMaxMinDay/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> HumAvgMaxMinFromToDay(@Path("station_id") String station_id,
                                       @Path("user_id") String user_id, @Path("token") String token,
                                       @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    @GET("Humidity/HumAvgMaxMinNight/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> HumAvgMaxMinFromToNight(@Path("station_id") String station_id,
                                         @Path("user_id") String user_id, @Path("token") String token,
                                         @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    @GET("Humidity/GraphicAvgDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> HumidityGraphicAvgDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                             @Path("token") String token, @Path("Interval") String Interval);

    @GET("Humidity/GraphicAvgNight/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> HumidityGraphicAvgNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                               @Path("token") String token, @Path("Interval") String Interval);

    @GET("Humidity/GraphicMaxDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> HumidityGraphicMaxDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                             @Path("token") String token, @Path("Interval") String Interval);

    @GET("Humidity/GraphicMaxNight/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> HumidityGraphicMaxNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                               @Path("token") String token, @Path("Interval") String Interval);

    @GET("Humidity/GraphicMinDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> HumidityGraphicMinDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                             @Path("token") String token, @Path("Interval") String Interval);

    @GET("Humidity/GraphicMinNight/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> HumidityGraphicMinNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                               @Path("token") String token, @Path("Interval") String Interval);

    @GET("Humidity/GraphicFromToDay/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> HumidityGraphicFromToDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                                @Path("token") String token, @Path("DateFrom") String startdate, @Path("DateTo") String enddate);

    @GET("Humidity/GraphicMinNight/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> HumidityGraphicFromToNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                                  @Path("token") String token, @Path("DateFrom") String startdate, @Path("DateTo") String enddate);


    // Raining ------------------------------------------------------------------------------------

    @GET("Raining/Rain/{station_id}/{user_id}/{token}")
    Call<ResponseBody> Raining(@Path("station_id") String station_id, @Path("user_id") String user_id, @Path("token") String token);

    @GET("Raining/Rain_5_60/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> Rain_5_60(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                 @Path("token") String token, @Path("Interval") String Interval);

    @GET("Raining/RainDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> RainDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                               @Path("token") String token, @Path("Interval") String Interval);

    @GET("Raining/RainNight/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> RainNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                 @Path("token") String token, @Path("Interval") String Interval);

    @GET("Raining/LastRain/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> LastRain(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                @Path("token") String token, @Path("Interval") String Interval);

    @GET("Raining/RainyDays/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> RainyDays(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                 @Path("token") String token, @Path("Interval") String Interval);

    @GET("Raining/RainDateDay/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> RainDateDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                   @Path("token") String token, @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    @GET("Raining/RainDateNight/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> RainDateNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                     @Path("token") String token, @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    @GET("Raining/LastRain/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> LastRainFromTo(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                      @Path("token") String token, @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    @GET("Raining/RainyDays/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> RainyDaysFromTo(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                       @Path("token") String token, @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    @GET("Raining/GraphicRainDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> GraphicRainDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                      @Path("token") String token, @Path("Interval") String Interval);

    @GET("Raining/GraphicRainNight/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> GraphicRainNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                        @Path("token") String token, @Path("Interval") String Interval);

    @GET("Raining/GraphicFromToDay/{station_id}/{user_id}/{token}/{startdate}/{enddate}")
    Call<ResponseBody> RainingGraphicFromToDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                               @Path("token") String token, @Path("startdate") String startdate, @Path("enddate") String enddate);

    @GET("Raining/GraphicFromToNight/{station_id}/{user_id}/{token}/{startdate}/{enddate}")
    Call<ResponseBody> RainingGraphicFromToNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                                            @Path("token") String token, @Path("startdate") String startdate,
                                                            @Path("enddate") String enddate);


    // Barometric pressure ----------------------------------------------------------------------

    @GET("Barometer/Bar/{station_id}/{user_id}/{token}")
    Call<ResponseBody> Barometer(@Path("station_id") String station_id, @Path("user_id") String user_id, @Path("token") String token);

    @GET("Barometer/BarAvgMaxMinDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> BarAvgMaxMinDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                       @Path("token") String token, @Path("Interval") String Interval);

    @GET("Barometer/BarAvgMaxMinNight/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> BarAvgMaxMinNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                         @Path("token") String token, @Path("Interval") String Interval);

    @GET("Barometer/BarAvgMaxMinDay/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> BarAvgMaxMinFromToDay(@Path("station_id") String station_id,
                                       @Path("user_id") String user_id, @Path("token") String token,
                                       @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    @GET("Barometer/BarAvgMaxMinNight/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> BarAvgMaxMinFromToNight(@Path("station_id") String station_id,
                                         @Path("user_id") String user_id, @Path("token") String token,
                                         @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    @GET("Barometer/GraphicAvgDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> BarGraphicAvgDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                        @Path("token") String token, @Path("Interval") String Interval);

    @GET("Barometer/GraphicAvgNight/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> BarGraphicAvgNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                          @Path("token") String token, @Path("Interval") String Interval);

    @GET("Barometer/GraphicMaxDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> BarGraphicMaxDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                        @Path("token") String token, @Path("Interval") String Interval);

    @GET("Barometer/GraphicMaxNight/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> BarGraphicMaxNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                          @Path("token") String token, @Path("Interval") String Interval);

    @GET("Barometer/GraphicMinDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> BarGraphicMinDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                        @Path("token") String token, @Path("Interval") String Interval);

    @GET("Barometer/GraphicMinNight/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> BarGraphicMinNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                          @Path("token") String token, @Path("Interval") String Interval);

    @GET("Barometer/GraphicFromToDay/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> BarGraphicFromToDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                           @Path("token") String token, @Path("DateFrom") String DateFrom,
                                           @Path("DateTo") String DateTo);

    @GET("Barometer/GraphicFromToNight/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> BarGraphicFromToNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                             @Path("token") String token, @Path("DateFrom") String DateFrom,
                                             @Path("DateTo") String DateTo);

    // Wind ---------------------------------------------------------------------------------------

    @GET("Wind/Wind/{station_id}/{user_id}/{token}")
    Call<ResponseBody> Wind(@Path("station_id") String station_id, @Path("user_id") String user_id, @Path("token") String token);

    @GET("Wind/GraphicWindRose/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> GraphicWindRose(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                       @Path("token") String token, @Path("Interval") String Interval);

    @GET("Wind/GraphicWindRoseDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> GraphicWindRoseDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                          @Path("token") String token, @Path("Interval") String Interval);

    @GET("Wind/GraphicWindRoseNight/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> GraphicWindRoseNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                            @Path("token") String token, @Path("Interval") String Interval);

    @GET("Wind/GraphicWindRose/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> GraphicWindRose(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                       @Path("token") String token, @Path("DateFrom") String DateFrom,
                                       @Path("DateTo") String DateTo);

    @GET("Wind/GraphicWindRoseDay/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> GraphicWindRoseDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                          @Path("token") String token, @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    @GET("Wind/GraphicWindRoseNight/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> GraphicWindRoseNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                            @Path("token") String token, @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    // Radiation -----------------------------------------------------------------------------------

    @GET("Radiation/Rad/{station_id}/{user_id}/{token}")
    Call<ResponseBody> Radiation(@Path("station_id") String station_id, @Path("user_id") String user_id, @Path("token") String token);

    @GET("Radiation/RadInterval/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> RadInterval(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                   @Path("token") String token, @Path("Interval") String Interval);

    @GET("Radiation/SunnyDays/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> SunnyDays(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                 @Path("token") String token, @Path("Interval") String Interval);

    @GET("Radiation/DurSunnyDays/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> DurSunnyDays(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                    @Path("token") String token, @Path("Interval") String Interval);

    @GET("Radiation/Cloudydays/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> CloudyDays(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                  @Path("token") String token, @Path("Interval") String Interval);

    @GET("Radiation/SunnyDays/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> SunnyDays(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                 @Path("token") String token, @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    @GET("Radiation/DurSunnyDays/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> DurSunnyDays(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                    @Path("token") String token, @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    @GET("Radiation/Cloudydays/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> CloudyDays(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                  @Path("token") String token, @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    @GET("Radiation/GraphicRadiationDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> GraphicRadiationDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                           @Path("token") String token, @Path("Interval") String Interval);

    @GET("Radiation/GraphicRadiationFromToDay/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> GraphicRadiationFromToDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                                 @Path("token") String token, @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);


    // Indexes--------------------------------------------------------------------------------------

    @GET("Index/Ind/{station_id}/{user_id}/{token}")
    Call<ResponseBody> Index(@Path("station_id") String station_id, @Path("user_id") String user_id,
                             @Path("token") String token);

    @GET("Index/Ind_5_60/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> Ind_5_60(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                @Path("token") String token, @Path("Interval") String Interval);

    @GET("Index/IndDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> IndDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                              @Path("token") String token, @Path("Interval") String Interval);

    @GET("Index/IndNight/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> IndNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                @Path("token") String token, @Path("Interval") String Interval);

    @GET("Index/IndDay/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> IndDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                              @Path("token") String token, @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    @GET("Index/IndNight/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> IndNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                @Path("token") String token, @Path("DateFrom") String DateFrom, @Path("DateTo") String DateTo);

    @GET("Index/GraphicIndexDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> IndexGraphicIndexDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                            @Path("token") String token, @Path("Interval") String Interval);

    @GET("Index/GraphicIndexNight/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> IndexGraphicIndexNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                              @Path("token") String token, @Path("Interval") String Interval);

    @GET("Index/GraphicIndexDay/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> IndexGraphicIndexDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                            @Path("token") String token, @Path("DateFrom") String DateFrom,
                                            @Path("DateTo") String DateTo);

    @GET("Index/GraphicIndexNight/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> IndexGraphicIndexNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                              @Path("token") String token, @Path("DateFrom") String DateFrom,
                                              @Path("DateTo") String DateTo);

    @GET("Index/GraphicETDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> IndexGraphicETDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                         @Path("token") String token, @Path("Interval") String Interval);

    @GET("Index/GraphicETNight/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> IndexGraphicETNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                           @Path("token") String token, @Path("Interval") String Interval);

    @GET("Index/GraphicETDay/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> IndexGraphicETDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                         @Path("token") String token, @Path("DateFrom") String DateFrom,
                                         @Path("DateTo") String DateTo);

    @GET("Index/GraphicETNight/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> IndexGraphicETNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                           @Path("token") String token, @Path("DateFrom") String DateFrom,
                                           @Path("DateTo") String DateTo);

    @GET("Index/TimeLineLastRainDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> IndexTimeLineLastRainDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                                @Path("token") String token, @Path("Interval") String Interval);

    @GET("Index/TimeLineLastRainNight/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> IndexTimeLineLastRainNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                                  @Path("token") String token, @Path("Interval") String Interval);

    @GET("Index/TimeLineLastRainDay/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> IndexTimeLineLastRainDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                                @Path("token") String token, @Path("DateFrom") String DateFrom,
                                                @Path("DateTo") String DateTo);

    @GET("Index/TimeLineLastRainNight/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> IndexTimeLineLastRainNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                                  @Path("token") String token, @Path("DateFrom") String DateFrom,
                                                  @Path("DateTo") String DateTo);


    @GET("Index/TimeLineLastSunDay/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> IndexTimeLineLastSunDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                               @Path("token") String token, @Path("Interval") String Interval);

    @GET("Index/TimeLineLastSunDay/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> IndexTimeLineLastSunDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                               @Path("token") String token, @Path("DateFrom") String DateFrom,
                                               @Path("DateTo") String DateTo);

    @GET("Index/TimeLineLowSolar/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> IndexTimeLineLowSolar(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                             @Path("token") String token, @Path("Interval") String Interval);

    @GET("Index/TimeLineLowSolar/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> IndexTimeLineLowSolar(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                             @Path("token") String token, @Path("DateFrom") String DateFrom,
                                             @Path("DateTo") String DateTo);

    @GET("Index/TimeLineHiSolar/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> IndexTimeLineHiSolar(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                            @Path("token") String token, @Path("Interval") String Interval);

    @GET("Index/TimeLineHiSolar/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> IndexTimeLineHiSolar(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                            @Path("token") String token, @Path("DateFrom") String DateFrom,
                                            @Path("DateTo") String DateTo);

    @GET("Index/TimeLineLowUV/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> IndexTimeLineLowUV(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                          @Path("token") String token, @Path("Interval") String Interval);

    @GET("Index/TimeLineLowUV/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> IndexTimeLineLowUV(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                          @Path("token") String token, @Path("DateFrom") String DateFrom,
                                          @Path("DateTo") String DateTo);

    @GET("Index/TimeLineHiUV/{station_id}/{user_id}/{token}/{Interval}")
    Call<ResponseBody> IndexTimeLineHiUV(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                         @Path("token") String token, @Path("Interval") String Interval);

    @GET("Index/TimeLineHiUV/{station_id}/{user_id}/{token}/{DateFrom}/{DateTo}")
    Call<ResponseBody> IndexTimeLineHiUV(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                         @Path("token") String token, @Path("DateFrom") String DateFrom,
                                         @Path("DateTo") String DateTo);

    // Soiil ----------------------------------------------------------------------------------------
    @GET("Soil/Soil/{station_id}/{user_id}/{token}/{Transmitter}")
    Call<ResponseBody> Soil(@Path("station_id") String station_id, @Path("user_id") String user_id,
                            @Path("token") String token, @Path("Transmitter") String Transmitter);

    @GET("Soil/LeafWet/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilLeafWet(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                   @Path("token") String token, @Path("Interval") String Interval,
                                   @Path("Transmitter") String Transmitter);

    @GET("Soil/LeafWetDay/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilLeafWetDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                      @Path("token") String token, @Path("Interval") String Interval,
                                      @Path("Transmitter") String Transmitter);

    @GET("Soil/LeafWetNight/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilLeafWetNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                        @Path("token") String token, @Path("Interval") String Interval,
                                        @Path("Transmitter") String Transmitter);

    @GET("Soil/SoilMoist/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilMoist(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                 @Path("token") String token, @Path("Interval") String Interval,
                                 @Path("Transmitter") String Transmitter);

    @GET("Soil/SoilMoistDay/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilMoistDay(@Path("station_id") String station_id,
                                    @Path("user_id") String user_id, @Path("token") String token,
                                    @Path("Interval") String Interval, @Path("Transmitter") String Transmitter);

    @GET("Soil/SoilMoistNight/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilMoistNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                      @Path("token") String token, @Path("Interval") String Interval,
                                      @Path("Transmitter") String Transmitter);

    @GET("Soil/SoilTemp/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilTemp(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                @Path("token") String token, @Path("Interval") String Interval,
                                @Path("Transmitter") String Transmitter);

    @GET("Soil/SoilTempDay/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilTempDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                   @Path("token") String token, @Path("Interval") String Interval,
                                   @Path("Transmitter") String Transmitter);

    @GET("Soil/SoilTempNight/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilTempNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                     @Path("token") String token, @Path("Interval") String Interval,
                                     @Path("Transmitter") String Transmitter);

    @GET("Soil/GraphicMoistAvgDay/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicMoistAvgDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                              @Path("token") String token, @Path("Interval") String Interval,
                                              @Path("Transmitter") String Transmitter);

    @GET("Soil/GraphicMoistAvgNight/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicMoistAvgNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                                @Path("token") String token, @Path("Interval") String Interval,
                                                @Path("Transmitter") String Transmitter);

    @GET("Soil/GraphicMoistAvgDay/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicMoistAvgDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                              @Path("token") String token, @Path("Transmitter") String Transmitter,
                                              @Path("startdate") String startdate, @Path("enddate") String enddate);

    @GET("Soil/GraphicMoistAvgNight/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicMoistAvgNight(@Path("station_id") String station_id,
                                                @Path("user_id") String user_id, @Path("token") String token,
                                                @Path("Transmitter") String Transmitter, @Path("startdate") String startdate,
                                                @Path("enddate") String enddate);

    @GET("Soil/GraphicMoistMaxDay/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicMoistMaxDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                              @Path("token") String token, @Path("Interval") String Interval,
                                              @Path("Transmitter") String Transmitter);

    @GET("Soil/GraphicMoistMaxNight/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicMoistMaxNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                                @Path("token") String token, @Path("Interval") String Interval,
                                                @Path("Transmitter") String Transmitter);

    @GET("Soil/GraphicMoistMaxDay/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicMoistMaxDayDate(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                                  @Path("token") String token, @Path("Transmitter") String Transmitter,
                                                  @Path("startdate") String startdate, @Path("enddate") String enddate);

    @GET("Soil/GraphicMoistMaxDay/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicMoistMaxNightDate(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                                    @Path("token") String token, @Path("Transmitter") String Transmitter,
                                                    @Path("startdate") String startdate, @Path("enddate") String enddate);

    @GET("Soil/GraphicMoistMinDay/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicMoistMinDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                              @Path("token") String token, @Path("Interval") String Interval,
                                              @Path("Transmitter") String Transmitter);

    @GET("Soil/GraphicMoistMinNight/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicMoistMinNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                                @Path("token") String token, @Path("Interval") String Interval,
                                                @Path("Transmitter") String Transmitter);

    @GET("Soil/GraphicMoistMinDay/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicMoistMinDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                              @Path("token") String token, @Path("Transmitter") String Transmitter,
                                              @Path("startdate") String startdate, @Path("enddate") String enddate);

    @GET("Soil/GraphicMoistMinNight/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicMoistMinNightDate(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                                    @Path("token") String token, @Path("Transmitter") String Transmitter,
                                                    @Path("startdate") String startdate, @Path("enddate") String enddate);







    @GET("Soil/GraphicLeafAvgDay/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicLeafAvgDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                             @Path("token") String token, @Path("Interval") String Interval,
                                             @Path("Transmitter") String Transmitter);






    @GET("Soil/GraphicLeafAvgNight/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicLeafAvgNight(@Path("station_id") String station_id,
                                               @Path("user_id") String user_id,
                                               @Path("token") String token,
                                               @Path("Interval") String Interval,
                                               @Path("Transmitter") String Transmitter);

    //http://localhost:53610/Soil/GraphicLeafAvgDay/{IdStation}/{IdUser}/{Token}/{Transmitter}/{startdate}/{enddate}
    @GET("Soil/GraphicLeafAvgDay/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicLeafAvgDay(@Path("station_id") String station_id,
                                             @Path("user_id") String user_id,
                                             @Path("token") String token,
                                             @Path("Transmitter") String Transmitter,
                                             @Path("startdate") String startdate,
                                             @Path("enddate") String enddate);

    //http://localhost:53610/Soil/GraphicLeafAvgNight/{IdStation}/{IdUser}/{Token}/{Transmitter}/{startdate}/{enddate}
    @GET("Soil/GraphicLeafAvgNight/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicLeafAvgNight(@Path("station_id") String station_id,
                                               @Path("user_id") String user_id,
                                               @Path("token") String token,
                                               @Path("Transmitter") String Transmitter,
                                               @Path("startdate") String startdate,
                                               @Path("enddate") String enddate);

    //http://localhost:53610/Soil/GraphicLeafMaxDay/{IdStation}/{IdUser}/{Token}/{Interval}/{Transmitter}
    @GET("Soil/GraphicLeafMaxDay/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicLeafMaxDay(@Path("station_id") String station_id,
                                             @Path("user_id") String user_id,
                                             @Path("token") String token,
                                             @Path("Interval") String Interval,
                                             @Path("Transmitter") String Transmitter);

    //http://localhost:53610/Soil/GraphicLeafMaxNight/{IdStation}/{IdUser}/{Token}/{Interval}/{Transmitter}
    @GET("Soil/GraphicLeafMaxNight/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicLeafMaxNight(@Path("station_id") String station_id,
                                               @Path("user_id") String user_id,
                                               @Path("token") String token,
                                               @Path("Interval") String Interval,
                                               @Path("Transmitter") String Transmitter);

    //http://localhost:53610/Soil/GraphicLeafMaxDay/{IdStation}/{IdUser}/{Token}/{Transmitter}/{startdate}/{enddate}
    @GET("Soil/GraphicLeafMaxDay/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicLeafMaxDay(@Path("station_id") String station_id,
                                             @Path("user_id") String user_id,
                                             @Path("token") String token,
                                             @Path("Transmitter") String Transmitter,
                                             @Path("startdate") String startdate,
                                             @Path("enddate") String enddate);

    //http://localhost:53610/Soil/GraphicLeafMaxNight/{IdStation}/{IdUser}/{Token}/{Transmitter}/{startdate}/{enddate}
    @GET("Soil/GraphicLeafMaxNight/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicLeafMaxNight(@Path("station_id") String station_id,
                                               @Path("user_id") String user_id,
                                               @Path("token") String token,
                                               @Path("Transmitter") String Transmitter,
                                               @Path("startdate") String startdate,
                                               @Path("enddate") String enddate);


    //http://localhost:53610/Soil/GraphicLeafMinDay/{IdStation}/{IdUser}/{Token}/{Interval}/{Transmitter}
    @GET("Soil/GraphicLeafMinDay/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicLeafMinDay(@Path("station_id") String station_id,
                                             @Path("user_id") String user_id,
                                             @Path("token") String token,
                                             @Path("Interval") String Interval,
                                             @Path("Transmitter") String Transmitter);

    //http://localhost:53610/Soil/GraphicLeafMinNight/{IdStation}/{IdUser}/{Token}/{Interval}/{Transmitter}
    @GET("Soil/GraphicLeafMinNight/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicLeafMinNight(@Path("station_id") String station_id,
                                               @Path("user_id") String user_id,
                                               @Path("token") String token,
                                               @Path("Interval") String Interval,
                                               @Path("Transmitter") String Transmitter);

    //http://localhost:53610/Soil/GraphicLeafMinDay/{IdStation}/{IdUser}/{Token}/{Transmitter}/{startdate}/{enddate}
    @GET("Soil/GraphicLeafMinDay/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicLeafMinDay(@Path("station_id") String station_id,
                                             @Path("user_id") String user_id,
                                             @Path("token") String token,
                                             @Path("Transmitter") String Transmitter,
                                             @Path("startdate") String startdate,
                                             @Path("enddate") String enddate);

    //http://localhost:53610/Soil/GraphicLeafMinNight/{IdStation}/{IdUser}/{Token}/{Transmitter}/{startdate}/{enddate}
    @GET("Soil/GraphicLeafMinNight/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicLeafMinNight(@Path("station_id") String station_id,
                                               @Path("user_id") String user_id,
                                               @Path("token") String token,
                                               @Path("Transmitter") String Transmitter,
                                               @Path("startdate") String startdate,
                                               @Path("enddate") String enddate);

    @GET("Soil/GraphicTempAvgDay/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicTempAvgDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                             @Path("token") String token, @Path("Interval") String Interval,
                                             @Path("Transmitter") String Transmitter);

    @GET("Soil/GraphicTempAvgNight/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicTempAvgNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                               @Path("token") String token, @Path("Interval") String Interval,
                                               @Path("Transmitter") String Transmitter);

    @GET("Soil/GraphicTempAvgDay/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicTempAvgDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                             @Path("token") String token, @Path("Transmitter") String Transmitter,
                                             @Path("startdate") String startdate, @Path("enddate") String enddate);

    @GET("Soil/GraphicTempAvgNight/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicTempAvgNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                               @Path("token") String token, @Path("Transmitter") String Transmitter,
                                               @Path("startdate") String startdate, @Path("enddate") String enddate);

    @GET("Soil/GraphicTempMaxDay/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicTempMaxDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                             @Path("token") String token, @Path("Interval") String Interval,
                                             @Path("Transmitter") String Transmitter);

    @GET("Soil/GraphicTempMaxNight/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicTempMaxNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                               @Path("token") String token, @Path("Interval") String Interval,
                                               @Path("Transmitter") String Transmitter);

    @GET("Soil/GraphicTempMaxDay/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicTempMaxDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                             @Path("token") String token, @Path("Transmitter") String Transmitter,
                                             @Path("startdate") String startdate, @Path("enddate") String enddate);

    @GET("Soil/GraphicTempMaxNight/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicTempMaxNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                               @Path("token") String token, @Path("Transmitter") String Transmitter,
                                               @Path("startdate") String startdate, @Path("enddate") String enddate);

    @GET("Soil/GraphicTempMinDay/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicTempMinDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                             @Path("token") String token, @Path("Interval") String Interval,
                                             @Path("Transmitter") String Transmitter);

    @GET("Soil/GraphicTempMinNight/{station_id}/{user_id}/{token}/{Interval}/{Transmitter}")
    Call<ResponseBody> SoilGraphicTempMinNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                               @Path("token") String token, @Path("Interval") String Interval,
                                               @Path("Transmitter") String Transmitter);

    @GET("Soil/GraphicTempMinDay/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicTempMinDay(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                             @Path("token") String token, @Path("Transmitter") String Transmitter,
                                             @Path("startdate") String startdate, @Path("enddate") String enddate);

    @GET("Soil/GraphicTempMinNight/{station_id}/{user_id}/{token}/{Transmitter}/{startdate}/{enddate}")
    Call<ResponseBody> SoilGraphicTempMinNight(@Path("station_id") String station_id, @Path("user_id") String user_id,
                                               @Path("token") String token, @Path("Transmitter") String Transmitter,
                                               @Path("startdate") String startdate, @Path("enddate") String enddate);




    @FormUrlEncoded
    @POST("User/CreateUserSocial")
    Call<RegResponse> regUserSocialRural(@Field("Email") String Email,
                                         @Field("SocialNetwork") String SocialNetwork,
                                         @Field("ProviderKey") String ProviderKey);


    //GET http://localhost:53609/Specialist/GetAllContact/2/ef64c53545480a2e1d995870424473e01a509e92b2aca0ac2dd53041623efa58
    @GET("Specialist/GetAllContact/{user_id}/{token}")
    Call<List<Specialist>> getAllSpecialists(@Path("user_id") String user_id,
                                             @Path("token") String token);

    @GET("Specialist/SearchSpecialist/{Specialization}/{Location}/{IdUser}/{Token}")
    Call<List<Specialist>> searchSpecialist(@Path("Specialization") String specialization, @Path("Location") String location,
                                            @Path("IdUser") String user_id, @Path("Token") String token);

    @GET("Specialist/FindContactIdUser/{IdUser}/{Token}/{Name}")
    Call<List<Specialist>> searchSpecialist(@Path("IdUser") String user_id, @Path("Token") String token,
                                            @Path("Name") String name);

    //GET http://localhost:53609/Specialist/GetContactIdUser/2/ef64c53545480a2e1d995870424473e01a509e92b2aca0ac2dd53041623efa58
    @GET("Specialist/GetContactIdUser/{user_id}/{token}")
    Call<List<Specialist>> getMySpecialists(@Path("user_id") String user_id,
                                             @Path("token") String token);

    //POST http://localhost:53609/Specialist/AddSpecialist
    @FormUrlEncoded
    @POST("Specialist/AddSpecialist")
    Call<ResponseBody> addSpecialist(@Field("Token") String Token,
                                         @Field("IdUser") String IdUser,
                                         @Field("IdUserCompany") int IdUserCompany);

    @FormUrlEncoded
    @POST("Specialist/DelSpecialist")
    Call<ResponseBody> deleteSpecialist(@Field("Token") String Token,
                                     @Field("IdUser") String IdUser,
                                     @Field("IdUserCompany") int IdUserCompany);


    //GET http://localhost:53609/Product/GetList/1/12345
    @GET("Product/GetList/{user_id}/{token}")
    Call<List<Product>> getMyProducts(@Path("user_id") String user_id,
                                      @Path("token") String token);

    @GET("Product/GetListProduct/{IdUser}/{Token}")
    Call<List<Product>> getProductCatalog(@Path("IdUser") String user_id,
                                      @Path("Token") String token);

    @GET("Product/GetListIdProduct/{user_id}/{product_id}/{token}")
    Call<List<Product>> getProductById(@Path("user_id") String user_id,@Path("product_id") int product_id,
                                      @Path("token") String token);


    @GET("Product/GetShopCountPage/{IdUser}/{Token}")
    Call<Product> getProductCount(@Path("IdUser") String user_id, @Path("Token") String token);

    @GET("Product/GetShopPaging/{IdUser}/{Token}/{PageNumber}")
    Call<List<Product>> getShopProduct(@Path("IdUser") String user_id, @Path("Token") String token,@Path("PageNumber") int pageNumber);

    @GET("Product/GetFindShop/{IdUser}/{Token}/{Name}")
    Call<List<Product>> searchShopProduct(@Path("IdUser") String user_id, @Path("Token") String token,@Path("Name") String name);

    @GET("Product/GetProductFromSpecialist/{IdUser}/{Token}/{IdSpec}")
    Call<List<Product>> getSpecShopProduct(@Path("IdUser") String user_id, @Path("Token") String token,@Path("IdSpec") String idSpec);

    @FormUrlEncoded
    @POST("Product/AddProduct")
    Call<Void> addProduct(@Field("Token") String token, @Field("IdUser") String idUser, @Field("Name")
            String name, @Field("Description") String description, @Field("Instruction") String instruction,
                          @Field("Delivery") String delivery, @Field("PhotoName") String photoName);

    @FormUrlEncoded
    @POST("Product/UpdateProduct")
    Call<Void> updateProduct(@Field("Token") String token,  @Field("IdProduct") int idProduct,
                             @Field("IdUser") String idUser, @Field("Name") String name,
                             @Field("Description") String description, @Field("Instruction") String instruction,
                          @Field("Delivery") String delivery, @Field("PhotoName") String photoName);

    @FormUrlEncoded
    @POST("Product/DeleteProduct")
    Call<Void> deleteProduct(@Field("Token") String token, @Field("IdProduct") int idProduct,
                             @Field("IdUser") String idUser);





    @FormUrlEncoded
    @POST("RiskUser/DeleteRisk")
    Call<Void> deleteRisk(@Field("Token") String token, @Field("IdUserAlarmRisk") int idRisk,
                             @Field("IdUser") String idUser);

    @GET("RiskUser/GetList/{IdUser}/{IdStation}/{Token}")
    Call<List<Risk>> getRisks(@Path("IdUser") String idUser, @Path("IdStation") int idStation, @Path("Token") String token);

    @GET("RiskUser/GetRiskIdRisk/{IdUser}/{IdUserAlarmRisk}/{Token}")
    Call<List<Risk>> getRiskById(@Path("IdUser") String idUser, @Path("IdUserAlarmRisk") int idRisk, @Path("Token") String token);

    @POST("RiskUser/InsertComposeRisk")
    Call<Void> createRisk(@Body RequestBody requestBody);

    @POST("RiskUser/UpdateRisk")
    Call<Void> updateRisk(@Body RequestBody requestBody);


    @GET("Impact/GetRisk/{IdUser}/{IdStation}/{Token}")
    Call<List<Impact>> getImpacts(@Path("IdUser") String idUser, @Path("IdStation") int idStation, @Path("Token") String token);

    @GET("Impact/GetRiskDetail/{IdUser}/{IdAlarmRisk}/{Token}")
    Call<List<Impact>> getImpactById(@Path("IdUser") String idUser, @Path("IdAlarmRisk") int idAlarmRisk, @Path("Token") String token);

    @GET("Messenger/GetListNoRead/{IdUser}/{Token}")
    Call<List<Message>> getChats(@Path("IdUser") String idUser, @Path("Token") String token);

    @GET("Messenger/GetListRead/{IdUser}/{IdUserTo}/{Token}")
    Call<List<Message>> getChatById(@Path("IdUser") String idUser, @Path("IdUserTo") String idUserTo,@Path("Token") String token);

    @POST("Messenger/MailInsert")
    Call<Void> sendMessage(@Body RequestBody requestBody);

    @POST("Specialist/CreateSpecialist")
    Call<Void> createSpecialist(@Body RequestBody requestBody);

    @GET("Specialist/GetClientIdUser/{IdUser}/{Token}")
    Call<List<Specialist>> getClients(@Path("IdUser") String user_id,
                                            @Path("Token") String token);

    @GET("Geography/GetListCountry")
    Call<List<Country>> getCountries();

    @GET("LanguageUser/GetLanguageUser")
    Call<List<Language>> getLanguages();

}
