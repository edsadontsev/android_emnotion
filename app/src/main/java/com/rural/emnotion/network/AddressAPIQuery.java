package com.rural.emnotion.network;

import com.rural.emnotion.models.AddressModel;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddressAPIQuery {
    private Retrofit retrofit;
    private AddressApiService service;

    public static AddressAPIQuery getInstance() {
        synchronized (AddressAPIQuery.class) {
            return new AddressAPIQuery();
        }
    }

    private AddressAPIQuery() {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://maps.googleapis.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.service = retrofit.create(AddressApiService.class);
    }

    public Retrofit getRetrofitConnection() {
        return retrofit;
    }

    public Call<AddressModel> getAddress(String latLng, boolean sensor) {
        return service.getAddress(latLng,sensor);
    }
}