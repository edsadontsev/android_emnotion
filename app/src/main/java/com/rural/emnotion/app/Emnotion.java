package com.rural.emnotion.app;

import android.app.Application;
import android.util.Log;

import com.rural.emnotion.settings.USM;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Administrator on 10.05.2017.
 */

public class Emnotion extends Application {

    private static Emnotion _instance;

    public Emnotion() {
        Log.d("App ", "application object has been created");
    }

    public static Emnotion getInstance() {
        return _instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        _instance = this;
        USM.init(this);
    }

}
