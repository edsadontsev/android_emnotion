package com.rural.emnotion.fragments.climat_data;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rural.emnotion.MainActivity;
import com.rural.emnotion.R;
import com.rural.emnotion.activities.CreateAlarmActivity;
import com.rural.emnotion.adapters.AlarmAdapter;
import com.rural.emnotion.adapters.ParamsAdapter;
import com.rural.emnotion.models.Alarm;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AlarmFragment extends Fragment {
    public AlarmFragment() {

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).showSpinner(true);
        getUserAlarms();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private View view;
    private CardView addAlarm;


    private RecyclerView recyclerView;
    private AlarmAdapter adapter;
    private List<Alarm> alarmList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_station, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        addAlarm = (CardView) view.findViewById(R.id.add_alarm);
        addAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), CreateAlarmActivity.class));
            }
        });

        alarmList = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return view;
    }

    private void getUserAlarms(){
        USM.init(getActivity());
        Call<List<Alarm>> getAlarms  = API.getApiRequestService().getAlarmUser(USM.getUserID(),
                USM.getUserToken(),String.valueOf(USM.getStation()));
        getAlarms.enqueue(new Callback<List<Alarm>>() {
            @Override
            public void onResponse(Call<List<Alarm>> call, Response<List<Alarm>> response) {
                Log.wtf("call",call.request().url().toString());
                alarmList = response.body();
                adapter = new AlarmAdapter(getActivity(), alarmList);
                recyclerView.setAdapter(adapter);
            }
            @Override
            public void onFailure(Call<List<Alarm>> call, Throwable t) {
            }
        });
    }



    private void addAlert(){
        //Call<ResponseBody> addCall = API.getApiRequestService().insertCompose("cddfcdfd");
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
}
