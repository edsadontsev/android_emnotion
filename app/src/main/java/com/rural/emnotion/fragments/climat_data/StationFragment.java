package com.rural.emnotion.fragments.climat_data;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rural.emnotion.MainActivity;
import com.rural.emnotion.R;
import com.rural.emnotion.activities.CreateStationActivity;
import com.rural.emnotion.adapters.StationAdapter;
import com.rural.emnotion.models.Station;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StationFragment extends Fragment {

    public StationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).showSpinner(false);
        getUsersStations();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    private View view;
    private RecyclerView recyclerView;
    private StationAdapter adapter;
    private List<Station> stations = new ArrayList<>();
    private CardView addStation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_admin, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        addStation = (CardView) view.findViewById(R.id.add_station);
        addStation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),CreateStationActivity.class));
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return view;
    }


    private void getUsersStations() {
        Call<List<Station>> getUsersStations = API.getApiRequestService().getUsersStations(USM.getUserID(),
                USM.getUserToken());
        getUsersStations.enqueue(new Callback<List<Station>>() {
            @Override
            public void onResponse(Call<List<Station>> call, Response<List<Station>> response) {
                Log.wtf("call",call.request().url().toString());
                stations = response.body();
                //"Status":"token failed"
                if (stations.get(0).Status.equals("token failed")) {
                    USM.init(getActivity());
                    USM.logout(getActivity());
                } else {
                    adapter = new StationAdapter(getActivity(), stations);
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<Station>> call, Throwable t) {

            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
