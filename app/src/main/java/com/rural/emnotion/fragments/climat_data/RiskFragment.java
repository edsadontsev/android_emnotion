package com.rural.emnotion.fragments.climat_data;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rural.emnotion.MainActivity;
import com.rural.emnotion.R;
import com.rural.emnotion.activities.CreateProductsActivity;
import com.rural.emnotion.activities.CreateRiskActivity;
import com.rural.emnotion.adapters.ProductsAdapter;
import com.rural.emnotion.adapters.RiskAdapter;
import com.rural.emnotion.models.Product;
import com.rural.emnotion.models.Risk;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RiskFragment extends Fragment {


    public RiskFragment() {
        // Required empty public constructor
    }

    private RecyclerView recyclerView;
    private RiskAdapter adapter;
    private CardView addScenario;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_risk, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        addScenario = (CardView) view.findViewById(R.id.add_risk);
        addScenario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), CreateRiskActivity.class));
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).showSpinner(false);
        getRisk();
    }

    private void getRisk(){
        USM.init(getContext());
        Call<List<Risk>> getSpecialists = API.getApiRequestService().getRisks(USM.getUserID(),USM.getStation(),
                USM.getUserToken());
        getSpecialists.enqueue(new Callback<List<Risk>>() {
            @Override
            public void onResponse(Call<List<Risk>> call, Response<List<Risk>> response) {
                Log.wtf("call",call.request().url().toString());
                adapter = new RiskAdapter(getActivity(), response.body());
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<List<Risk>> call, Throwable t) {
            }
        });
    }
}
