package com.rural.emnotion.fragments.climat_data;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rural.emnotion.MainActivity;
import com.rural.emnotion.R;
import com.rural.emnotion.activities.CreateAlarmActivity;
import com.rural.emnotion.models.AddressModel;
import com.rural.emnotion.models.Country;
import com.rural.emnotion.models.Language;
import com.rural.emnotion.network.API;
import com.rural.emnotion.network.AddressAPIQuery;
import com.rural.emnotion.settings.USM;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateSpecialistFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener{

    @BindView(R.id.station_address) TextView stationAddress;
    @BindView(R.id.station_coordinates) TextView stationCoordinates;
    @BindView(R.id.name) EditText name;
    @BindView(R.id.email) EditText email;
    @BindView(R.id.phone) EditText phone;
    @BindView(R.id.mobile_phone) EditText mobilePhone;
    @BindView(R.id.skype) EditText skype;
    @BindView(R.id.whats_app) EditText whatsApp;
    @BindView(R.id.education) EditText university;
    @BindView(R.id.start_education) EditText startEducation;
    @BindView(R.id.end_education) EditText endEducation;
    @BindView(R.id.organization) EditText organization;
    @BindView(R.id.start_organization) EditText startWork;
    @BindView(R.id.end_organization) EditText endWork;
    @BindView(R.id.specialization) EditText specialization;
    @BindView(R.id.distance) EditText distance;
    @BindView(R.id.btn_create) Button createButton;
    @BindView(R.id.countries) MultiSelectSpinner countries;
    @BindView(R.id.languages) MultiSelectSpinner languages;

    private AddressModel address;
    private LatLng coordinates;
    private GoogleMap googleMap;
    private Call<AddressModel> addressRequest;
    private List<String> countriesList;
    private JSONArray countriesJsonArray;
   private List<String> languagesList;
    private JSONArray languagesJsonArray;


    public CreateSpecialistFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showSpinner(false);
    }

    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_create_specialist, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        getCountries();
        getLanguages();

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("IdUser",USM.getUserID());
                    jsonObject.put("Token",USM.getUserToken());
                    jsonObject.put("Name",name.getText().toString());
                    jsonObject.put("Email1",email.getText().toString());
                    jsonObject.put("Phone",phone.getText().toString());
                    jsonObject.put("MobilePhone", mobilePhone.getText().toString());
                    jsonObject.put("Skype", skype.getText().toString());
                    jsonObject.put("Messenger", whatsApp.getText().toString());
                    jsonObject.put("Specialization", specialization.getText().toString());
                    jsonObject.put("Distance", distance.getText().toString());
                    jsonObject.put("Latitude", coordinates.latitude);
                    jsonObject.put("Longitude", coordinates.longitude);

                    JSONArray educationArray = new JSONArray();
                    JSONObject education = new JSONObject();
                    education.put("University",university.getText().toString());
                    education.put("StartDateEdu",startEducation.getText().toString());
                    education.put("EndDateEdu",endEducation.getText().toString());
                    educationArray.put(education);

                    JSONArray workArray = new JSONArray();
                    JSONObject work = new JSONObject();
                    work.put("Organization",organization.getText().toString());
                    work.put("StartDateExp",startWork.getText().toString());
                    work.put("EndDateExp",endWork.getText().toString());
                    workArray.put(work);

                    jsonObject.put("Education",educationArray);
                    jsonObject.put("Experiences",workArray);
                    jsonObject.put("CountryUser",countriesJsonArray);
                    jsonObject.put("LanguageUser",languagesJsonArray);

                    Log.wtf("json",jsonObject.toString());
                    RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
                    createSpecialist(requestBody);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        if(addressRequest!=null)addressRequest.cancel();
        super.onDestroy();
    }

    @Override
    public void onMapClick(LatLng latLng) {
        googleMap.clear();
        coordinates = latLng;
        googleMap.addMarker(new MarkerOptions().position(latLng));
        getAddress(latLng.latitude + " " + latLng.longitude, latLng);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        this.googleMap = googleMap;
        googleMap.setOnMapClickListener(this);
    }

    private void getAddress(final String coordinates, final LatLng latLng) {
        addressRequest = AddressAPIQuery.getInstance().getAddress(coordinates, true);
        addressRequest.enqueue(new Callback<AddressModel>() {

            @Override
            public void onResponse(Call<AddressModel> call, final Response<AddressModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().results.size() != 0) {
                        address = response.body();
                        stationAddress.setText(response.body().results.get(0).formatted_address);
                        stationCoordinates.setText(latLng.latitude + "/" + latLng.longitude);
                    }

                }
            }

            @Override
            public void onFailure(Call<AddressModel> call, Throwable t) {

            }

        });
    }

    private void createSpecialist(RequestBody requestBody){
        Call<Void> getAlarm = API.getApiRequestService().createSpecialist(requestBody);
        getAlarm.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.wtf("response",response.code()+"");
                Log.wtf("response",response.message()+"");
                if(response.isSuccessful()){
                    ((MainActivity)getActivity()).replaceFragment(new SearchSpecialistFragment());
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }

    private void getCountries(){
        Call<List<Country>> getAlarm = API.getApiRequestService().getCountries();
        getAlarm.enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, final Response<List<Country>> response) {
                if(response.isSuccessful()){
                    countriesList = new ArrayList<>();
                    for(int i =0;i<response.body().size();i++) countriesList.add(response.body().get(i).Country);

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_multiple_choice,countriesList);

                    countries.setListAdapter(adapter);
                    countries.setMinSelectedItems(0);
                    countries.setListener(new MultiSelectSpinner.MultiSpinnerListener() {
                        @Override
                        public void onItemsSelected(boolean[] selected) {
                            countriesJsonArray = new JSONArray();
                            for(int i=0; i<selected.length; i++) {
                                if(selected[i]) {
                                    try {
                                        JSONObject json = new JSONObject();
                                        json.put("IdCountry",response.body().get(i).IdCountry);
                                        countriesJsonArray.put(json);
                                    }
                                    catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {
            }
        });
    }

    private void getLanguages(){
        Call<List<Language>> getAlarm = API.getApiRequestService().getLanguages();
        getAlarm.enqueue(new Callback<List<Language>>() {
            @Override
            public void onResponse(Call<List<Language>> call, final Response<List<Language>> response) {
                if(response.isSuccessful()){
                    languagesList = new ArrayList<>();
                    for(int i =0;i<response.body().size();i++) languagesList.add(response.body().get(i).Name);

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_multiple_choice,languagesList);

                    languages.setListAdapter(adapter);
                    languages.setMinSelectedItems(0);
                    languages.setListener(new MultiSelectSpinner.MultiSpinnerListener() {
                        @Override
                        public void onItemsSelected(boolean[] selected) {
                            languagesJsonArray = new JSONArray();
                            for(int i=0; i<selected.length; i++) {
                                if(selected[i]) {
                                    try {
                                        JSONObject json = new JSONObject();
                                        json.put("IdLanguage",response.body().get(i).IdLanguage);
                                        languagesJsonArray.put(json);
                                    }
                                    catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<Language>> call, Throwable t) {
            }
        });
    }
}
