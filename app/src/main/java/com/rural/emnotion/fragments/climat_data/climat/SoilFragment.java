package com.rural.emnotion.fragments.climat_data.climat;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rural.emnotion.MainActivity;
import com.rural.emnotion.R;
import com.rural.emnotion.charts.MyAxisValueFormatter;
import com.rural.emnotion.charts.SoilXAxisValueFormatter;
import com.rural.emnotion.charts.XYMarkerView;
import com.rural.emnotion.fragments.climat_data.BaseFragment;
import com.rural.emnotion.models.Index;
import com.rural.emnotion.models.Soil;
import com.rural.emnotion.models.Temperature;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;
import com.squareup.timessquare.CalendarPickerView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.rural.emnotion.util.DateUtils.getDate;
import static com.rural.emnotion.util.DateUtils.getTitleDate;

public class SoilFragment extends BaseFragment {

    private static final String MOISTURE_TYPE_1 = " min soil moisture ";
    private static final String MOISTURE_TYPE_2 = " min soil temperature ";
    private static final String MOISTURE_TYPE_3 = " avg soil moisture ";
    private static final String MOISTURE_TYPE_4 = " avg soil temperature ";
    private static final String MOISTURE_TYPE_5 = " max soil moisture ";
    private static final String MOISTURE_TYPE_6 = " max soil temperature ";

    private static final String LEAF_WETNESS_1 = " min leaf wetness";
    private static final String LEAF_WETNESS_2 = " max leaf wetness";
    private static final String LEAF_WETNESS_3 = " average leaf wetness";

    public static final String FIRST_TYPE_BAR_CHART = " soil moisture for ";
    public static final String SECOND_TYPE_BAR_CHART = " soil temperature (cB) for ";
    public static final String THIRD_TYPE_BAR_CHART = " leaf wetness for ";

    @BindView(R.id.soil_moisture) TextView soilMoisture;
    @BindView(R.id.soil_temperature) TextView soilTemperature;
    @BindView(R.id.leaf_wetness) TextView leafWetness;

    @BindView(R.id.bar_chart) BarChart barChart;
    @BindView(R.id.title_bar_chart) TextView titleBarChart;
    @BindView(R.id.spinner_bar_chart) MaterialSpinner spinnerBarChart;
    @BindView(R.id.day_bar_chart) ImageView dayBarChart;
    @BindView(R.id.night_bar_chart) ImageView nightBarChart;
    @BindView(R.id.soil_moisture_bar_chart) CardView soilMoistureBarChart;
    @BindView(R.id.soil_temperature_bar_chart) CardView soilTemperatureBarChart;
    @BindView(R.id.leaf_wetness_bar_chart) CardView leafWetnessBarChart;

    @BindView(R.id.line_chart_first) LineChart lineChartFirst;
    @BindView(R.id.title_line_chart_first) TextView titleLineChartFirst;
    @BindView(R.id.spinner_line_chart_first) MaterialSpinner spinnerLineChartFirst;
    @BindView(R.id.day_line_chart_first) ImageView dayLineChartFirst;
    @BindView(R.id.night_line_chart_first) ImageView nightLineChartFirst;
    @BindView(R.id.calendar_line_chart_first) ImageView calendarLineChartFirst;
    @BindView(R.id.date_picker_line_chart_first) CalendarPickerView datePickerLineChartFirst;
    @BindView(R.id.date_picker_line_chart_first_container) RelativeLayout datePickerLineChartFirstContainer;
    @BindView(R.id.min_soil_moisture) CardView minSoilMoisture;
    @BindView(R.id.min_soil_temperature) CardView minSoilTemperature;
    @BindView(R.id.avg_soil_moisture) CardView avgSoilMoisture;
    @BindView(R.id.avg_soil_temperature) CardView avgSoilTemperature;
    @BindView(R.id.max_soil_moisture) CardView maxSoilMoisture;
    @BindView(R.id.max_soil_temperature) CardView maxSoilTemperature;

    @BindView(R.id.line_chart_second) LineChart lineChartSecond;
    @BindView(R.id.title_line_chart_second) TextView titleLineChartSecond;
    @BindView(R.id.spinner_line_chart_second) MaterialSpinner spinnerLineChartSecond;
    @BindView(R.id.day_line_chart_second) ImageView dayLineChartSecond;
    @BindView(R.id.night_line_chart_second) ImageView nightLineChartSecond;
    @BindView(R.id.calendar_line_chart_second) ImageView calendarLineChartSecond;
    @BindView(R.id.date_picker_line_chart_second) CalendarPickerView datePickerLineChartSecond;
    @BindView(R.id.date_picker_line_chart_second_container) RelativeLayout datePickerLineChartSecondContainer;
    @BindView(R.id.min_leaf_wetness) CardView minLeafWetness;
    @BindView(R.id.max_leaf_wetness) CardView maxLeafWetness;
    @BindView(R.id.avg_leaf_wetness) CardView avgLeafWetness;

    @BindColor(R.color.colorPrimary) int blue;
    @BindColor(R.color.colorOrange) int orange;

    private String partOfDayBarChart;
    private String intervalBarChart;
    private boolean isDayBarChart;
    private String typeSoilBarChart;

    private String partOfDayLineChartFirst;
    private String intervalLineChartFirst;
    private boolean isDayLineChartFirst;
    private boolean isCalendarLineChartFirst;
    private String typeSoilLineChartFirst;
    private String startDateLineChartFirst;
    private String endDateLineChartFirst;
    private String startDateTitleLineChartFirst;
    private String endDateTitleLineChartFirst;

    private String partOfDayLineChartSecond;
    private String intervalLineChartSecond;
    private boolean isDayLineChartSecond;
    private boolean isCalendarLineChartSecond;
    private String typeSoilLineChartSecond;
    private String startDateLineChartSecond;
    private String endDateLineChartSecond;
    private String startDateTitleLineChartSecond;
    private String endDateTitleLineChartSecond;

    private List<Soil> soilGraphList;
    private Soil soil;

    public SoilFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showSpinner(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_soil, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        USM.init(getActivity());
        initObjects();
        initViews();
        initListeners();

        initHeaderSoil();

        soilMoistureBarChart.performClick();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                minSoilMoisture.performClick();
                drawLineGraphFirst();
            }
        },1000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                minLeafWetness.performClick();
                drawLineGraphSecond();
            }
        },2000);

    }

    private void initObjects(){
        partOfDayBarChart = "Day";
        intervalBarChart = "5 min";
        isDayBarChart = true;

        partOfDayLineChartFirst = "Day";
        intervalLineChartFirst = "last day";
        isDayLineChartFirst = true;

        partOfDayLineChartSecond = "Day";
        intervalLineChartSecond = "last day";
        isDayLineChartSecond = true;
    }

    private void initViews() {
        String[] intervalArray = getResources().getStringArray(R.array.indexes_intervals);
        spinnerBarChart.setItems(intervalArray);

        String[] interval = getResources().getStringArray(R.array.general_intervals);
        spinnerLineChartFirst.setItems(interval);
        spinnerLineChartSecond.setItems(interval);
    }

    private void initListeners() {
        spinnerBarChart.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Toast.makeText(getContext(), item.toString(), Toast.LENGTH_SHORT).show();
                intervalBarChart = item.toString();
                titleBarChart.setText(partOfDayBarChart + typeSoilBarChart + intervalBarChart);
                initBarChart();
            }
        });

        spinnerLineChartFirst.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                isCalendarLineChartFirst = false;
                Toast.makeText(getContext(), item.toString(), Toast.LENGTH_SHORT).show();
                intervalLineChartFirst = item.toString();
                titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for " + intervalLineChartFirst);
                initIntervalSoil();
            }
        });

        spinnerLineChartSecond.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                isCalendarLineChartSecond = false;
                Toast.makeText(getContext(), item.toString(), Toast.LENGTH_SHORT).show();
                intervalLineChartSecond = item.toString();
                titleLineChartSecond.setText(partOfDayLineChartSecond + typeSoilLineChartSecond + " for " + intervalLineChartSecond);
                initIntervalLeafWetness();
            }
        });
    }

    // Start Header ----------------------------------------------------------------------------

    private void initHeaderSoil(){
        Call<ResponseBody> getSoil = API.getApiRequestService().Soil(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), "1");
        getSoil.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Soil>() {}.getType();
                    soil = gson.fromJson(result, listType);

                    soilMoisture.setText(String.format("%.1f", soil.SoilMoist) + " cB");
                    soilTemperature.setText(String.format("%.2f", soil.SoilTemp) + " °C");
                    leafWetness.setText(String.format("%.0f", soil.LeafWet) + " ");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    // End Header ----------------------------------------------------------------------------

    // Start Bar Chart ----------------------------------------------------------------------------

    @OnClick({R.id.day_bar_chart,R.id.night_bar_chart})
    protected void partOfDayBarChartOnClick(){
        if (isDayBarChart) {
            dayBarChart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_day_noactiv));
            nightBarChart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_night_activ));
            isDayBarChart = false;
            partOfDayBarChart = "Night";
            titleBarChart.setText(partOfDayBarChart + typeSoilBarChart + intervalBarChart);
            initBarChart();

        } else {
            dayBarChart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_day_activ));
            nightBarChart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_night_noactiv));
            isDayBarChart = true;
            partOfDayBarChart = "Day";
            titleBarChart.setText(partOfDayBarChart + typeSoilBarChart + intervalBarChart);
            initBarChart();
        }
    }

    @OnClick(R.id.soil_moisture_bar_chart)
    protected void soilMoistureBarChartOnClick(){
        typeSoilBarChart = FIRST_TYPE_BAR_CHART;
        titleBarChart.setText(partOfDayBarChart + typeSoilBarChart + intervalBarChart);
        initBarChart();
    }

    @OnClick(R.id.soil_temperature_bar_chart)
    protected void soilTemperatureBarChartOnClick(){
        typeSoilBarChart = SECOND_TYPE_BAR_CHART;
        titleBarChart.setText(partOfDayBarChart + typeSoilBarChart + intervalBarChart);
        initBarChart();
    }

    @OnClick(R.id.leaf_wetness_bar_chart)
    protected void leafWetnessBarChartOnClick(){
        typeSoilBarChart = THIRD_TYPE_BAR_CHART;
        titleBarChart.setText(partOfDayBarChart + typeSoilBarChart + intervalBarChart);
        initBarChart();

    }

    private void drawBarGraph() {
        barChart.setOnChartValueSelectedListener(this);
        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);
        barChart.getDescription().setEnabled(false);
        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        barChart.setMaxVisibleValueCount(60);
        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(false);
        // lineChart.setDrawYLabels(false);
        IAxisValueFormatter xAxisFormatter = new SoilXAxisValueFormatter(barChart,typeSoilBarChart);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        // xAxis.setTypeface(mTfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);

        IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = barChart.getAxisLeft();
        // leftAxis.setTypeface(mTfLight);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0);

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        // rightAxis.setTypeface(mTfLight);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0);

        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);

        XYMarkerView mv = new XYMarkerView(getActivity(), xAxisFormatter);
        mv.setChartView(barChart); //for bounds control
        barChart.setMarker(mv); // Set the marker to the chart
    }

    private void initBarChart() {
        Call<ResponseBody> getSoil = null;
        if(typeSoilBarChart.equals(FIRST_TYPE_BAR_CHART)) {
            if (isDayBarChart)
                getSoil = API.getApiRequestService().SoilMoistDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalBarChart, "1");
            else
                getSoil = API.getApiRequestService().SoilMoistNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalBarChart, "1");
        }
        else if(typeSoilBarChart.equals(SECOND_TYPE_BAR_CHART)) {
            if (isDayBarChart)
                getSoil = API.getApiRequestService().SoilTempDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalBarChart, "1");
            else
                getSoil = API.getApiRequestService().SoilTempNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalBarChart, "1");
        }
        else if(typeSoilBarChart.equals(THIRD_TYPE_BAR_CHART)) {
            if (isDayBarChart)
                getSoil = API.getApiRequestService().SoilLeafWetDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalBarChart, "1");
            else
                getSoil = API.getApiRequestService().SoilLeafWetNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalBarChart, "1");
        }

        getSoil.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Soil>() {}.getType();
                    soil = gson.fromJson(result, listType);
                    if(typeSoilBarChart.equals(FIRST_TYPE_BAR_CHART))
                        setBarData((float) soil.SoilMoist1, (float) soil.SoilMoist2,
                                (float) soil.SoilMoist3, (float) soil.SoilMoist4);
                    else if(typeSoilBarChart.equals(SECOND_TYPE_BAR_CHART)) setBarData((float) soil.SoilTemp1,
                            (float) soil.SoilTemp2, (float) soil.SoilTemp3, (float) soil.SoilTemp4);
                    else if(typeSoilBarChart.equals(THIRD_TYPE_BAR_CHART)) setBarData((float) soil.LeafWet1,
                            (float) soil.LeafWet2, 0, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private void setBarData(float a, float b, float c, float d) {
        drawBarGraph();

        ArrayList<BarEntry> yValues = new ArrayList<BarEntry>();

        if(!typeSoilBarChart.equals(THIRD_TYPE_BAR_CHART)) {
            yValues.add(new BarEntry(0, a));
            yValues.add(new BarEntry(1, b));
            yValues.add(new BarEntry(2, c));
            yValues.add(new BarEntry(3, d));
        }
        else{
            yValues.add(new BarEntry(0, a));
            yValues.add(new BarEntry(1, b));
        }

        BarDataSet set;

        if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {
            set = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set.setValues(yValues);
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();
        } else {
            set = new BarDataSet(yValues, "Soil");
            set.setDrawIcons(false);
            set.setColors(getResources().getColor(R.color.colorPrimary));
            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set);
            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            // data.setValueTypeface(mTfLight);
            data.setBarWidth(0.6f);

            barChart.setData(data);
        }
        barChart.invalidate();
    }

    // End Bar Chart ----------------------------------------------------------------------------

    // Start Line Chart First ----------------------------------------------------------------------------

    @OnClick({R.id.day_line_chart_first,R.id.night_line_chart_first})
    protected void partOfDayLineChartOnClick() {
        if (isDayLineChartFirst) {
            dayLineChartFirst.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_day_noactiv));
            nightLineChartFirst.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_night_activ));
            isDayLineChartFirst = false;
            partOfDayLineChartFirst = "Night";
            if(isCalendarLineChartFirst) {
                titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst+ " for " +
                        startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);
                initFromToLineChart();
            }
            else {
                titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for " + intervalLineChartFirst);
                initIntervalSoil();
            }


        } else {
            dayLineChartFirst.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_day_activ));
            nightLineChartFirst.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_night_noactiv));
            isDayLineChartFirst = true;
            partOfDayLineChartFirst = "Day";
            if(isCalendarLineChartFirst) {
                titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for "
                        + startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);
                initFromToLineChart();
            }
            else {
                titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for " + intervalLineChartFirst);
                initIntervalSoil();
            }
        }
    }

    @OnClick(R.id.min_soil_moisture)
    protected void minSoilMoistureLineChartOnClick(){
        typeSoilLineChartFirst = MOISTURE_TYPE_1;
        changeBgLineChartFirst();
        if(isCalendarLineChartFirst) {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for " +
                    startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);
            initFromToLineChart();
        }
        else {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for " + intervalLineChartFirst);
            initIntervalSoil();
        }
    }

    @OnClick(R.id.min_soil_temperature)
    protected void minSoilTemperatureLineChartOnClick(){
        typeSoilLineChartFirst = MOISTURE_TYPE_2;
        changeBgLineChartFirst();
        if(isCalendarLineChartFirst) {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for "
                    + startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);
            initFromToLineChart();
        }
        else {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for " + intervalLineChartFirst);
            initIntervalSoil();
        }
    }

    @OnClick(R.id.avg_soil_moisture)
    protected void avgSoilMoistureLineChartOnClick(){
        typeSoilLineChartFirst = MOISTURE_TYPE_3;
        changeBgLineChartFirst();
        if(isCalendarLineChartFirst) {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for "
                    + startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);
            initFromToLineChart();
        }
        else {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for " + intervalLineChartFirst);
            initIntervalSoil();
        }
    }

    @OnClick(R.id.avg_soil_temperature)
    protected void avgSoilTemperatureLineChartOnClick(){
        typeSoilLineChartFirst = MOISTURE_TYPE_4;
        changeBgLineChartFirst();
        if(isCalendarLineChartFirst) {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for "
                    + startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);
            initFromToLineChart();
        }
        else {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for " + intervalLineChartFirst);
            initIntervalSoil();
        }
    }

    @OnClick(R.id.max_soil_moisture)
    protected void maxSoilMoistureLineChartOnClick(){
        typeSoilLineChartFirst = MOISTURE_TYPE_5;
        changeBgLineChartFirst();
        if(isCalendarLineChartFirst) {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for "
                    + startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);
            initFromToLineChart();
        }
        else {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for " + intervalLineChartFirst);
            initIntervalSoil();
        }
    }

    @OnClick(R.id.max_soil_temperature)
    protected void maxSoilTemperatureLineChartOnClick(){
        typeSoilLineChartFirst = MOISTURE_TYPE_6;
        changeBgLineChartFirst();
        if(isCalendarLineChartFirst) {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for "
                    + startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);
            initFromToLineChart();
        }
        else {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for " + intervalLineChartFirst);
            initIntervalSoil();
        }
    }

    @OnClick(R.id.calendar_line_chart_first)
    protected void calendarLineChartOnClick(){
        initDatePickerLineChart();
    }

    @OnClick(R.id.btn_pick_date_line_chart_first)
    protected void btnPickDateLineChartOnClick(){
        datePickerLineChartFirstContainer.setVisibility(View.GONE);
        ArrayList<Date> selectedDates = (ArrayList<Date>) datePickerLineChartFirst.getSelectedDates();

        if(selectedDates.size()>1){
            isCalendarLineChartFirst = true;
            startDateLineChartFirst = getDate(selectedDates.get(0));
            endDateLineChartFirst = getDate(selectedDates.get(selectedDates.size()-1));

            startDateTitleLineChartFirst = getTitleDate(selectedDates.get(0));
            endDateTitleLineChartFirst = getTitleDate(selectedDates.get(selectedDates.size()-1));
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeSoilLineChartFirst + " for "
                    + startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);

            initFromToLineChart();
        }
    }

    private void changeBgLineChartFirst(){
        minSoilMoisture.setBackgroundColor(blue);
        minSoilTemperature.setBackgroundColor(blue);
        avgSoilMoisture.setBackgroundColor(blue);
        avgSoilTemperature.setBackgroundColor(blue);
        maxSoilMoisture.setBackgroundColor(blue);
        maxSoilTemperature.setBackgroundColor(blue);

        if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_1)) minSoilMoisture.setBackgroundColor(orange);
        else if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_2)) minSoilTemperature.setBackgroundColor(orange);
        else if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_3)) avgSoilMoisture.setBackgroundColor(orange);
        else if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_4)) avgSoilTemperature.setBackgroundColor(orange);
        else if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_5)) maxSoilMoisture.setBackgroundColor(orange);
        else if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_6)) maxSoilTemperature.setBackgroundColor(orange);
    }
    private void initDatePickerLineChart(){
        datePickerLineChartFirstContainer.setVisibility(View.VISIBLE);

        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);
        Calendar lastDay = Calendar.getInstance();
        lastDay.add(Calendar.DATE, -1);

        datePickerLineChartFirst.init(lastYear.getTime(),new Date())
                .withSelectedDate(lastDay.getTime())
                .inMode(CalendarPickerView.SelectionMode.RANGE);
    }

    private void drawLineGraphFirst() {
        lineChartFirst.setOnChartValueSelectedListener(this);
        // no description text
        lineChartFirst.getDescription().setEnabled(false);
        // enable touch gestures
        lineChartFirst.setTouchEnabled(true);
        lineChartFirst.setDragDecelerationFrictionCoef(0.9f);
        // enable scaling and dragging
        lineChartFirst.setDragEnabled(true);
        lineChartFirst.setScaleEnabled(true);
        lineChartFirst.setDrawGridBackground(false);
        lineChartFirst.setHighlightPerDragEnabled(true);
        // if disabled, scaling can be done on x- and y-axis separately
        lineChartFirst.setPinchZoom(true);
        // set an alternative background color
        lineChartFirst.setBackgroundColor(Color.WHITE);
    }

    private void setLegend() {
        lineChartFirst.animateX(2500);

        // get the legend (only possible after setting data)
        Legend l = lineChartFirst.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);
        //   l.setTypeface(mTfLight);
        l.setTextSize(11f);
        l.setTextColor(Color.GRAY);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
//        l.setYOffset(11f);

        XAxis xAxis = lineChartFirst.getXAxis();
        //  xAxis.setTypeface(mTfLight);
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.GRAY);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);

        YAxis leftAxis = lineChartFirst.getAxisLeft();
        //   leftAxis.setTypeface(mTfLight);
        leftAxis.setTextColor(Color.BLACK);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);

        YAxis rightAxis = lineChartFirst.getAxisRight();
        //   leftAxis.setTypeface(mTfLight);
        rightAxis.setTextColor(Color.BLACK);
        rightAxis.setDrawGridLines(true);
        rightAxis.setGranularityEnabled(true);
    }

    private void setData(List<Soil> temperatureGraphList) {
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        for(int i=0;i<4;i++){
            ArrayList<Entry> yValues = new ArrayList<>();
            for(int j=0;j<temperatureGraphList.size();j++){
                if(i==0){
                    if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_1) ||
                            typeSoilLineChartFirst.equals(MOISTURE_TYPE_3) ||
                            typeSoilLineChartFirst.equals(MOISTURE_TYPE_5))
                        yValues.add(new Entry(j, (float) temperatureGraphList.get(j).SoilMoist1));
                    else
                        yValues.add(new Entry(j, (float) temperatureGraphList.get(j).SoilTemp1));
                }
                else if(i==1){
                    if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_1) ||
                            typeSoilLineChartFirst.equals(MOISTURE_TYPE_3) ||
                            typeSoilLineChartFirst.equals(MOISTURE_TYPE_5))
                        yValues.add(new Entry(j, (float) temperatureGraphList.get(j).SoilMoist2));
                    else
                        yValues.add(new Entry(j, (float) temperatureGraphList.get(j).SoilTemp2));
                }
                else if(i==2){
                    if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_1) ||
                            typeSoilLineChartFirst.equals(MOISTURE_TYPE_3) ||
                            typeSoilLineChartFirst.equals(MOISTURE_TYPE_5))
                        yValues.add(new Entry(j, (float) temperatureGraphList.get(j).SoilMoist3));
                    else
                        yValues.add(new Entry(j, (float) temperatureGraphList.get(j).SoilTemp3));
                }
                else if(i==3){
                    if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_1) ||
                            typeSoilLineChartFirst.equals(MOISTURE_TYPE_3) ||
                            typeSoilLineChartFirst.equals(MOISTURE_TYPE_5))
                        yValues.add(new Entry(j, (float) temperatureGraphList.get(j).SoilMoist4));
                    else
                        yValues.add(new Entry(j, (float) temperatureGraphList.get(j).SoilTemp4));
                }

            }
            LineDataSet lineDataSet;
            if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_1) ||
                    typeSoilLineChartFirst.equals(MOISTURE_TYPE_3) ||
                    typeSoilLineChartFirst.equals(MOISTURE_TYPE_5)){

                lineDataSet = new LineDataSet(yValues,"Moisture"+(i+1));
            }
            else {
                lineDataSet = new LineDataSet(yValues, "Temperature" + (i + 1));
            }
            lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
            if(i==0) lineDataSet.setColor(getResources().getColor(R.color.colorPrimary));
            else if(i==1) lineDataSet.setColor(getResources().getColor(R.color.colorGreen));
            else if(i==2) lineDataSet.setColor(getResources().getColor(R.color.colorPink));
            else if(i==3) lineDataSet.setColor(getResources().getColor(R.color.colorOrange));
            lineDataSet.setCircleColor(Color.GRAY);
            lineDataSet.setLineWidth(2f);
            lineDataSet.setCircleRadius(3f);
            lineDataSet.setFillAlpha(65);
            lineDataSet.setFillColor(ColorTemplate.colorWithAlpha(Color.YELLOW, 200));
            lineDataSet.setDrawCircleHole(false);
            lineDataSet.setHighLightColor(Color.rgb(244, 117, 117));

            dataSets.add(lineDataSet);
        }

        LineData data = new LineData(dataSets);
        data.setValueTextColor(Color.GRAY);
        data.setValueTextSize(9f);

        lineChartFirst.setData(data);
        lineChartFirst.invalidate();
    }

    private void initFromToLineChart() {
        Call<ResponseBody> getSoil = null;
        if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_1)) {
            if (isDayBarChart)
                getSoil = API.getApiRequestService().SoilGraphicMoistMinDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), "1", startDateLineChartFirst, endDateLineChartFirst);
            else
                getSoil = API.getApiRequestService().SoilGraphicMoistMinNightDate(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), "1", startDateLineChartFirst, endDateLineChartFirst);
        }
        else if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_2)) {
            if (isDayBarChart)
                getSoil = API.getApiRequestService().SoilGraphicTempMinDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), "1", startDateLineChartFirst, endDateLineChartFirst);
            else
                getSoil = API.getApiRequestService().SoilGraphicTempMinNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), "1", startDateLineChartFirst, endDateLineChartFirst);
        }
        else if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_3)) {
            if (isDayBarChart)
                getSoil = API.getApiRequestService().SoilGraphicMoistAvgDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(),"1", startDateLineChartFirst, endDateLineChartFirst);
            else
                getSoil = API.getApiRequestService().SoilGraphicMoistAvgNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), "1", startDateLineChartFirst, endDateLineChartFirst);
        }
        else if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_4)) {
            if (isDayBarChart)
                getSoil = API.getApiRequestService().SoilGraphicTempAvgDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(),"1", startDateLineChartFirst, endDateLineChartFirst);
            else
                getSoil = API.getApiRequestService().SoilGraphicTempAvgNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), "1", startDateLineChartFirst, endDateLineChartFirst);
        }

        else if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_5)) {
            if (isDayBarChart)
                getSoil = API.getApiRequestService().SoilGraphicMoistMaxDayDate(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(),"1", startDateLineChartFirst, endDateLineChartFirst);
            else
                getSoil = API.getApiRequestService().SoilGraphicMoistMaxNightDate(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), "1", startDateLineChartFirst, endDateLineChartFirst);
        }

        else if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_6)) {
            if (isDayBarChart)
                getSoil = API.getApiRequestService().SoilGraphicTempMaxDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(),"1", startDateLineChartFirst, endDateLineChartFirst);
            else
                getSoil = API.getApiRequestService().SoilGraphicTempMaxNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), "1", startDateLineChartFirst, endDateLineChartFirst);
        }

        getSoil.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Soil>>() {}.getType();
                    soilGraphList = gson.fromJson(result, listType);
                    if (soilGraphList.size() > 0) {
                        setData(soilGraphList);
                        setLegend();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void initIntervalSoil() {
        Call<ResponseBody> getSoil = null;
        if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_1)) {
            if (isDayBarChart)
                getSoil = API.getApiRequestService().SoilGraphicMoistMinDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartFirst, "1");
            else
                getSoil = API.getApiRequestService().SoilGraphicMoistMinNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartFirst, "1");
        }
        else if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_2)) {
            if (isDayBarChart)
                getSoil = API.getApiRequestService().SoilGraphicTempMinDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartFirst, "1");
            else
                getSoil = API.getApiRequestService().SoilGraphicTempMinNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartFirst, "1");
        }

        else if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_3)) {
            if (isDayBarChart)
                getSoil = API.getApiRequestService().SoilGraphicMoistAvgDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartFirst, "1");
            else
                getSoil = API.getApiRequestService().SoilGraphicMoistAvgNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartFirst, "1");
        }

        else if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_4)) {
            if (isDayBarChart)
                getSoil = API.getApiRequestService().SoilGraphicTempAvgDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartFirst, "1");
            else
                getSoil = API.getApiRequestService().SoilGraphicTempAvgNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartFirst, "1");
        }

        else if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_5)) {
            if (isDayBarChart)
                getSoil = API.getApiRequestService().SoilGraphicMoistMaxDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartFirst, "1");
            else
                getSoil = API.getApiRequestService().SoilGraphicMoistMaxNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartFirst, "1");
        }

        else if(typeSoilLineChartFirst.equals(MOISTURE_TYPE_6)) {
            if (isDayBarChart)
                getSoil = API.getApiRequestService().SoilGraphicTempMaxDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartFirst, "1");
            else
                getSoil = API.getApiRequestService().SoilGraphicTempMaxNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartFirst, "1");
        }

        getSoil.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Soil>>() {}.getType();
                    soilGraphList = gson.fromJson(result, listType);
                    if (soilGraphList.size() > 0) {
                        setData(soilGraphList);
                        setLegend();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

        //End Line Chart First -------------------------------------------------------------------------
    }

    // Start Line Chart Second ----------------------------------------------------------------------------

    @OnClick({R.id.day_line_chart_second,R.id.night_line_chart_second})
    protected void partOfDayLineChartSecondOnClick() {
        if (isDayLineChartSecond) {
            dayLineChartSecond.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_day_noactiv));
            nightLineChartSecond.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_night_activ));
            isDayLineChartSecond = false;
            partOfDayLineChartSecond = "Night";
            if(isCalendarLineChartSecond) {
                titleLineChartSecond.setText(partOfDayLineChartSecond + typeSoilLineChartSecond+ " for " +
                        startDateTitleLineChartSecond + " - " + endDateTitleLineChartSecond);
                initFromToLineChartSecond();
            }
            else {
                titleLineChartSecond.setText(partOfDayLineChartSecond + typeSoilLineChartSecond + " for " + intervalLineChartSecond);
                initIntervalLeafWetness();
            }


        } else {
            dayLineChartSecond.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_day_activ));
            nightLineChartSecond.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_night_noactiv));
            isDayLineChartSecond = true;
            partOfDayLineChartSecond = "Day";
            if(isCalendarLineChartSecond) {
                titleLineChartSecond.setText(partOfDayLineChartSecond + typeSoilLineChartSecond + " for "
                        + startDateTitleLineChartSecond + " - " + endDateTitleLineChartSecond);
                initFromToLineChartSecond();
            }
            else {
                titleLineChartSecond.setText(partOfDayLineChartSecond + typeSoilLineChartSecond + " for " + intervalLineChartSecond);
                initIntervalLeafWetness();
            }
        }
    }

    @OnClick(R.id.min_leaf_wetness)
    protected void timelineLastRainLineChartOnClick(){
        Log.wtf("aaa","click");
        dayLineChartSecond.setVisibility(View.VISIBLE);
        nightLineChartSecond.setVisibility(View.VISIBLE);
        isDayLineChartSecond = true;
        partOfDayLineChartSecond = "Day";
        typeSoilLineChartSecond = LEAF_WETNESS_1;
        if(isCalendarLineChartSecond) {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeSoilLineChartSecond + " for " +
                    startDateTitleLineChartSecond + " - " + endDateTitleLineChartSecond);
            initFromToLineChartSecond();
        }
        else {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeSoilLineChartSecond + " for " + intervalLineChartSecond);
            initIntervalLeafWetness();
        }
    }

    @OnClick(R.id.max_leaf_wetness)
    protected void timelineLastSunnyDatLineChartOnClick(){
        dayLineChartSecond.setVisibility(View.GONE);
        nightLineChartSecond.setVisibility(View.GONE);
        isDayLineChartSecond = true;
        partOfDayLineChartSecond = "";
        typeSoilLineChartSecond = LEAF_WETNESS_2;
        if(isCalendarLineChartSecond) {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeSoilLineChartSecond + " for "
                    + startDateTitleLineChartSecond + " - " + endDateTitleLineChartSecond);
            initFromToLineChartSecond();
        }
        else {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeSoilLineChartSecond + " for " + intervalLineChartSecond);
            initIntervalLeafWetness();
        }
    }

    @OnClick(R.id.avg_leaf_wetness)
    protected void timelineSolarRadiationLineChartOnClick(){
        dayLineChartSecond.setVisibility(View.GONE);
        nightLineChartSecond.setVisibility(View.GONE);
        isDayLineChartSecond = true;
        partOfDayLineChartSecond = "";
        typeSoilLineChartSecond = LEAF_WETNESS_3;
        if(isCalendarLineChartSecond) {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeSoilLineChartSecond + " for "
                    + startDateTitleLineChartSecond + " - " + endDateTitleLineChartSecond);
            initFromToLineChartSecond();
        }
        else {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeSoilLineChartSecond + " for " + intervalLineChartSecond);
            initIntervalLeafWetness();
        }
    }

    @OnClick(R.id.calendar_line_chart_second)
    protected void calendarLineChartSecondOnClick(){
        initDatePickerLineChartSecond();
    }

    @OnClick(R.id.btn_pick_date_line_chart_second)
    protected void btnPickDateLineChartSecondOnClick(){
        datePickerLineChartSecondContainer.setVisibility(View.GONE);
        ArrayList<Date> selectedDates = (ArrayList<Date>) datePickerLineChartSecond.getSelectedDates();

        if(selectedDates.size()>1){
            isCalendarLineChartSecond = true;
            startDateLineChartSecond = getDate(selectedDates.get(0));
            endDateLineChartSecond = getDate(selectedDates.get(selectedDates.size()-1));

            startDateTitleLineChartSecond = getTitleDate(selectedDates.get(0));
            endDateTitleLineChartSecond = getTitleDate(selectedDates.get(selectedDates.size()-1));
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeSoilLineChartSecond + " for "
                    + startDateTitleLineChartSecond + " - " + endDateTitleLineChartSecond);

            initFromToLineChartSecond();
        }
    }

    private void initDatePickerLineChartSecond(){
        datePickerLineChartSecondContainer.setVisibility(View.VISIBLE);

        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);
        Calendar lastDay = Calendar.getInstance();
        lastDay.add(Calendar.DATE, -1);

        datePickerLineChartSecond.init(lastYear.getTime(),new Date())
                .withSelectedDate(lastDay.getTime())
                .inMode(CalendarPickerView.SelectionMode.RANGE);
    }

    private void drawLineGraphSecond() {
        lineChartSecond.setOnChartValueSelectedListener(this);
        // no description text
        lineChartSecond.getDescription().setEnabled(false);
        // enable touch gestures
        lineChartSecond.setTouchEnabled(true);
        lineChartSecond.setDragDecelerationFrictionCoef(0.9f);
        // enable scaling and dragging
        lineChartSecond.setDragEnabled(true);
        lineChartSecond.setScaleEnabled(true);
        lineChartSecond.setDrawGridBackground(false);
        lineChartSecond.setHighlightPerDragEnabled(true);
        // if disabled, scaling can be done on x- and y-axis separately
        lineChartSecond.setPinchZoom(true);
        // set an alternative background color
        lineChartSecond.setBackgroundColor(Color.WHITE);
    }

    private void setLegendSecond() {
        lineChartSecond.animateX(2500);

        // get the legend (only possible after setting data)
        Legend l = lineChartSecond.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);
        //   l.setTypeface(mTfLight);
        l.setTextSize(11f);
        l.setTextColor(Color.GRAY);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
//        l.setYOffset(11f);

        XAxis xAxis = lineChartSecond.getXAxis();
        //  xAxis.setTypeface(mTfLight);
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.GRAY);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);

        YAxis leftAxis = lineChartSecond.getAxisLeft();
        //   leftAxis.setTypeface(mTfLight);
        leftAxis.setTextColor(Color.BLACK);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);

        YAxis rightAxis = lineChartSecond.getAxisRight();
        //   leftAxis.setTypeface(mTfLight);
        rightAxis.setTextColor(Color.BLACK);
        rightAxis.setDrawGridLines(true);
        rightAxis.setGranularityEnabled(true);
    }

    private void setDataSecond(List<Soil> temperatureGraphList) {
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        for(int i=0;i<2;i++){
            ArrayList<Entry> yValues = new ArrayList<>();
            for(int j=0;j<temperatureGraphList.size();j++){
                if(i==0) {
                    yValues.add(new Entry(j, (float) temperatureGraphList.get(j).LeafWet1));
                }
                else {
                    yValues.add(new Entry(j, (float) temperatureGraphList.get(j).LeafWet2));
                }

            }
            LineDataSet lineDataSet;
            lineDataSet = new LineDataSet(yValues,"Leaf wetness"+(i+1));

            lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
            if(i==0) lineDataSet.setColor(getResources().getColor(R.color.colorPrimary));
            else if(i==1) lineDataSet.setColor(getResources().getColor(R.color.colorGreen));
            lineDataSet.setCircleColor(Color.GRAY);
            lineDataSet.setLineWidth(2f);
            lineDataSet.setCircleRadius(3f);
            lineDataSet.setFillAlpha(65);
            lineDataSet.setFillColor(ColorTemplate.colorWithAlpha(Color.YELLOW, 200));
            lineDataSet.setDrawCircleHole(false);
            lineDataSet.setHighLightColor(Color.rgb(244, 117, 117));

            dataSets.add(lineDataSet);
        }

        LineData data = new LineData(dataSets);
        data.setValueTextColor(Color.GRAY);
        data.setValueTextSize(9f);

        lineChartSecond.setData(data);
        lineChartSecond.invalidate();
    }

    private void initIntervalLeafWetness() {
        Call<ResponseBody> getAvgLineTemper = null;

        if(typeSoilLineChartSecond.equals(LEAF_WETNESS_1)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().SoilGraphicLeafMinDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartSecond, "1");
            else
                getAvgLineTemper = API.getApiRequestService().SoilGraphicLeafMinNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartSecond, "1");
        }
        else if(typeSoilLineChartSecond.equals(LEAF_WETNESS_2)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().SoilGraphicLeafMaxDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartSecond, "1");
            else
                getAvgLineTemper = API.getApiRequestService().SoilGraphicLeafMaxNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartSecond, "1");
        }
        else if(typeSoilLineChartSecond.equals(LEAF_WETNESS_3)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().SoilGraphicLeafAvgDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartSecond, "1");
            else
                getAvgLineTemper = API.getApiRequestService().SoilGraphicLeafAvgNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartSecond, "1");
        }

        getAvgLineTemper.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String result = API.getResponse(response.body().byteStream());
                Log.wtf("call",call.request().url().toString());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Soil>>() {}.getType();
                    soilGraphList = gson.fromJson(result, listType);
                    if (soilGraphList.size() > 0) {
                        setDataSecond(soilGraphList);
                        setLegendSecond();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void initFromToLineChartSecond() {
        Call<ResponseBody> getAvgLineTemper = null;

        if(typeSoilLineChartSecond.equals(LEAF_WETNESS_1)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().SoilGraphicLeafMinDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), "1", startDateLineChartSecond, endDateLineChartSecond);
            else
                getAvgLineTemper = API.getApiRequestService().SoilGraphicLeafMinNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), "1",  startDateLineChartSecond, endDateLineChartSecond);
        }
        else if(typeSoilLineChartSecond.equals(LEAF_WETNESS_2)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().SoilGraphicLeafMaxDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(),"1", startDateLineChartSecond, endDateLineChartSecond);
            else
                getAvgLineTemper = API.getApiRequestService().SoilGraphicLeafMaxNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), "1", startDateLineChartSecond, endDateLineChartSecond);
        }
        else if(typeSoilLineChartSecond.equals(LEAF_WETNESS_3)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().SoilGraphicLeafAvgDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), "1", startDateLineChartSecond, endDateLineChartSecond);
            else
                getAvgLineTemper = API.getApiRequestService().SoilGraphicLeafAvgNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), "1", startDateLineChartSecond, endDateLineChartSecond);
        }

        getAvgLineTemper.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Index>>() {}.getType();
                    soilGraphList = gson.fromJson(result, listType);
                    if (soilGraphList.size() > 0) {
                        setDataSecond(soilGraphList);
                        setLegend();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

        //End Line Chart Second -------------------------------------------------------------------------
    }

}

