package com.rural.emnotion.fragments.climat_data.climat;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rural.emnotion.MainActivity;
import com.rural.emnotion.R;
import com.rural.emnotion.charts.RadiationXAxisValueFormatter;
import com.rural.emnotion.charts.RadiationYAxisValueFormatter;
import com.rural.emnotion.charts.XYMarkerView;
import com.rural.emnotion.fragments.climat_data.BaseFragment;
import com.rural.emnotion.models.Radiation;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;
import com.squareup.timessquare.CalendarPickerView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.rural.emnotion.util.DateUtils.getDate;
import static com.rural.emnotion.util.DateUtils.getTitleDate;

public class RadiationFragment extends BaseFragment {

    private static final String SOLAR_TYPE = "Solar radiation ";
    private static final String UV_TYPE = "UV ";
    private static final String UV_DOSE_TYPE = "UV dose ";

    @BindView(R.id.solar_radiation) TextView solarRadiation;
    @BindView(R.id.low_solar_radiation) TextView lowSolarRadiation;
    @BindView(R.id.high_solar_radiation) TextView highSolarRadiation;
    @BindView(R.id.uv_index) TextView uvIndex;
    @BindView(R.id.uv_dose) TextView uvDose;
    @BindView(R.id.high_uv) TextView highUv;

    @BindView(R.id.bar_chart) BarChart barChart;
    @BindView(R.id.title_bar_chart) TextView titleBarChart;
    @BindView(R.id.spinner_bar_chart) MaterialSpinner spinnerBarChart;

    @BindView(R.id.line_chart) LineChart lineChart;
    @BindView(R.id.title_line_chart) TextView titleLineChart;
    @BindView(R.id.spinner_line_chart) MaterialSpinner spinnerLineChart;
    @BindView(R.id.calendar_line_chart) ImageView calendarLineChart;
    @BindView(R.id.date_picker_line_chart) CalendarPickerView datePickerLineChart;
    @BindView(R.id.date_picker_line_chart_container) RelativeLayout datePickerLineChartContainer;
    @BindView(R.id.solar_radiation_line_chart) CardView solarRadiationLineChart;
    @BindView(R.id.uv_line_chart) CardView uvLineChart;
    @BindView(R.id.uv_dose_line_chart) CardView uvDoseLineChart;

    @BindView(R.id.title_drought_period) TextView titleDroughtPeriod;
    @BindView(R.id.drought_period_days) TextView daysDroughtPeriod;
    @BindView(R.id.calendar_drought_period) ImageView calendarDroughtPeriod;
    @BindView(R.id.spinner_drought_period) MaterialSpinner spinnerDroughtPeriod;
    @BindView(R.id.date_picker_drought_period) CalendarPickerView datePickerDroughtPeriod;
    @BindView(R.id.date_picker_drought_period_container) RelativeLayout datePickerDroughtPeriodContainer;

    @BindView(R.id.title_sunny_days) TextView titleSunnyDays;
    @BindView(R.id.sunny_days) TextView daysSunnyDays;
    @BindView(R.id.calendar_sunny_days) ImageView calendarSunnyDays;
    @BindView(R.id.spinner_sunny_days) MaterialSpinner spinnerSunnyDays;
    @BindView(R.id.date_picker_sunny_days) CalendarPickerView datePickerSunnyDays;
    @BindView(R.id.date_picker_sunny_days_container) RelativeLayout datePickerSunnyDaysContainer;

    @BindView(R.id.title_cloudy_days) TextView titleCloudyDays;
    @BindView(R.id.cloudy_days) TextView daysCloudyDays;
    @BindView(R.id.calendar_cloudy_days) ImageView calendarCloudyDays;
    @BindView(R.id.spinner_cloudy_days) MaterialSpinner spinnerCloudyDays;
    @BindView(R.id.date_picker_cloudy_days) CalendarPickerView datePickerCloudyDays;
    @BindView(R.id.date_picker_cloudy_days_container) RelativeLayout datePickerCloudyDaysContainer;

    private String intervalBarChart;
    
    private String intervalDroughtPeriod;
    private String startDateDroughtPeriod;
    private String endDateDroughtPeriod;
    private String startDateTitleDroughtPeriod;
    private String endDateTitleDroughtPeriod;

    private String intervalSunnyDays;
    private String startDateSunnyDays;
    private String endDateSunnyDays;
    private String startDateTitleSunnyDays;
    private String endDateTitleSunnyDays;

    private String intervalCloudyDays;
    private String startDateCloudyDays;
    private String endDateCloudyDays;
    private String startDateTitleCloudyDays;
    private String endDateTitleCloudyDays;

    private String intervalLineChart;
    private boolean isCalendarLineChart;
    private String typeRadiationLineChart;
    private String startDateLineChart;
    private String endDateLineChart;
    private String startDateTitleLineChart;
    private String endDateTitleLineChart;

    private List<Radiation> radiationGraphList;
    private Radiation radiation;

    public RadiationFragment() {

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showSpinner(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_radiation, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

        USM.init(getActivity());
        initObjects();
        initViews();
        initListeners();

        initHeaderRadiation();

        solarRadiationLineChart.performClick();

        drawLineGraph();
        drawBarGraph();
    }

    private void initObjects(){
        intervalBarChart = "5 min";
        intervalLineChart = "last day";
        intervalDroughtPeriod = "last three days";
        intervalSunnyDays = "last three days";
        intervalCloudyDays = "last three days";
    }

    private void initViews() {
        String[] intervalArray = getResources().getStringArray(R.array.radiation_intervals);
        spinnerBarChart.setItems(intervalArray);
        titleBarChart.setText("Radiation (W/m2) for " + intervalBarChart);

        String[] intervalLineChartArray = getResources().getStringArray(R.array.general_intervals);
        spinnerLineChart.setItems(intervalLineChartArray);

        String[] intervalRadiationPeriodsArray = getResources().getStringArray(R.array.radiation_periods_intervals);
        spinnerDroughtPeriod.setItems(intervalRadiationPeriodsArray);
        spinnerSunnyDays.setItems(intervalRadiationPeriodsArray);
        spinnerCloudyDays.setItems(intervalRadiationPeriodsArray);

        titleDroughtPeriod.setText("Drought period for " + intervalDroughtPeriod);
        initIntervalDroughtPeriod();

        titleSunnyDays.setText("Number of sunny days for " + intervalSunnyDays);
        initIntervalSunnyDays();

        titleCloudyDays.setText("Number of cloudy days for " + intervalCloudyDays);
        initIntervalCloudyDays();

    }

    private void initListeners() {
        spinnerBarChart.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Toast.makeText(getContext(), item.toString(), Toast.LENGTH_SHORT).show();
                intervalBarChart = item.toString();
                initBarChart();
                titleBarChart.setText("Radiation (W/m2) for " + intervalBarChart);
            }
        });

        spinnerLineChart.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                isCalendarLineChart = false;
                Toast.makeText(getContext(), item.toString(), Toast.LENGTH_SHORT).show();
                intervalLineChart = item.toString();
                titleLineChart.setText(typeRadiationLineChart + " graph for " + intervalLineChart);
                checkTypeRadiation();
            }
        });

        spinnerDroughtPeriod.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Toast.makeText(getContext(), item.toString(), Toast.LENGTH_SHORT).show();
                intervalDroughtPeriod = item.toString();
                titleDroughtPeriod.setText("Drought period for " + intervalDroughtPeriod);
                initIntervalDroughtPeriod();
            }
        });

        spinnerSunnyDays.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Toast.makeText(getContext(), item.toString(), Toast.LENGTH_SHORT).show();
                intervalSunnyDays = item.toString();
                titleSunnyDays.setText("Number of sunny days for " + intervalSunnyDays);
                initIntervalSunnyDays();
            }
        });

        spinnerCloudyDays.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                Toast.makeText(getContext(), item.toString(), Toast.LENGTH_SHORT).show();
                intervalCloudyDays = item.toString();
                titleCloudyDays.setText("Number of cloudy days for " + intervalCloudyDays);
                initIntervalCloudyDays();
            }
        });
    }

    // Start Header ----------------------------------------------------------------------------

    private void initHeaderRadiation() {
        Call<ResponseBody> getRadiation = API.getApiRequestService().Radiation(String.valueOf(USM.getStation()), USM.getUserID(), USM.getUserToken());
        getRadiation.enqueue(new Callback<ResponseBody>() {
            @SuppressLint({"DefaultLocale", "SetTextI18n"})
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String result = API.getResponse(response.body().byteStream());
                Log.wtf("call",call.request().url().toString());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Radiation>>(){}.getType();
                    List<Radiation> radiationList = gson.fromJson(result, listType);

                    Radiation radiation = radiationList.get(0);

                    solarRadiation.setText(radiation.SolarRad + " W/m2");
                    lowSolarRadiation.setText(radiation.LowSolarRad + " W/m2");
                    highSolarRadiation.setText(radiation.HiSolarRad + " W/m2");
                    uvIndex.setText(radiation.UvNdex + " W/m2");
                    uvDose.setText(radiation.UvDose + " W/m2");
                    highUv.setText(radiation.HiUv + " W/m2");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    // End Header ----------------------------------------------------------------------------

    // Start Bar Chart ----------------------------------------------------------------------------

    private void drawBarGraph() {
        barChart.setOnChartValueSelectedListener(this);
        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);
        barChart.getDescription().setEnabled(false);
        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        barChart.setMaxVisibleValueCount(60);
        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(false);
        // lineChart.setDrawYLabels(false);
        IAxisValueFormatter xAxisFormatter = new RadiationXAxisValueFormatter(barChart);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        // xAxis.setTypeface(mTfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);

        IAxisValueFormatter custom = new RadiationYAxisValueFormatter();

        YAxis leftAxis = barChart.getAxisLeft();
        // leftAxis.setTypeface(mTfLight);
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        // rightAxis.setTypeface(mTfLight);
        rightAxis.setLabelCount(8, false);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)

        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);

        XYMarkerView mv = new XYMarkerView(getActivity(), xAxisFormatter);
        mv.setChartView(barChart); // For bounds control
        barChart.setMarker(mv); // Set the marker to the chart
        initBarChart();
    }

    private void initBarChart() {
        Call<ResponseBody> getBarChartRadiation = null;

        getBarChartRadiation = API.getApiRequestService().RadInterval(String.valueOf(USM.getStation()), USM.getUserID(), USM.getUserToken(), intervalBarChart);

        getBarChartRadiation.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Radiation>() {}.getType();
                    radiation = gson.fromJson(result, listType);
                    setBarData((float) radiation.SolarRad, (float) radiation.LowSolarRad, true);
                } catch (Exception e) {
                    Log.wtf("call",call.request().url().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private void setBarData(float min, float avg, boolean update) {

        ArrayList<BarEntry> yValues = new ArrayList<>();

        yValues.add(new BarEntry(0, min));
        yValues.add(new BarEntry(1, avg));

        BarDataSet set;

        if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {
            set = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set.setValues(yValues);
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();
        } else {
            set = new BarDataSet(yValues, "Radiation");
            set.setDrawIcons(false);
            set.setColors(getResources().getColor(R.color.colorPrimary));
            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set);
            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            // data.setValueTypeface(mTfLight);
            data.setBarWidth(0.6f);

            barChart.setData(data);
        }
        barChart.invalidate();
    }

    // End Bar Chart ----------------------------------------------------------------------------

    @OnClick(R.id.solar_radiation_line_chart)
    protected void solarRadiationLineChartOnClick(){
        typeRadiationLineChart = SOLAR_TYPE;
        if(isCalendarLineChart) {
            titleLineChart.setText(typeRadiationLineChart + " graph for " + startDateTitleLineChart + " - " + endDateTitleLineChart);
            initFromToLineChart();
        }
        else {
            titleLineChart.setText(typeRadiationLineChart + " graph for " + intervalLineChart);
            checkTypeRadiation();
        }
    }

    @OnClick(R.id.uv_line_chart)
    protected void uvLineChartOnClick(){
        typeRadiationLineChart = UV_TYPE;
        if(isCalendarLineChart) {
            titleLineChart.setText(typeRadiationLineChart + " graph for " + startDateTitleLineChart + " - " + endDateTitleLineChart);
            initFromToLineChart();
        }
        else {
            titleLineChart.setText(typeRadiationLineChart + " graph for " + intervalLineChart);
            checkTypeRadiation();
        }
    }

    @OnClick(R.id.uv_dose_line_chart)
    protected void uvDoseLineChartOnClick(){
        typeRadiationLineChart = UV_DOSE_TYPE;
        if(isCalendarLineChart) {
            titleLineChart.setText(typeRadiationLineChart + " graph for " + startDateTitleLineChart + " - " + endDateTitleLineChart);
            initFromToLineChart();
        }
        else {
            titleLineChart.setText(typeRadiationLineChart + " graph for " + intervalLineChart);
            checkTypeRadiation();
        }
    }

    @OnClick(R.id.calendar_line_chart)
    protected void calendarLineChartOnClick(){
        initDatePickerLineChart();
    }

    @OnClick(R.id.btn_pick_date_line_chart)
    protected void btnPickDateLineChartOnClick(){
        datePickerLineChartContainer.setVisibility(View.GONE);
        ArrayList<Date> selectedDates = (ArrayList<Date>) datePickerLineChart.getSelectedDates();

        if(selectedDates.size()>1){
            isCalendarLineChart = true;
            startDateLineChart = getDate(selectedDates.get(0));
            endDateLineChart = getDate(selectedDates.get(selectedDates.size()-1));

            startDateTitleLineChart = getTitleDate(selectedDates.get(0));
            endDateTitleLineChart = getTitleDate(selectedDates.get(selectedDates.size()-1));
            titleLineChart.setText(typeRadiationLineChart + " graph for " + startDateTitleLineChart + " - " + endDateTitleLineChart);

            initFromToLineChart();
        }
    }

    private void initDatePickerLineChart(){
        datePickerLineChartContainer.setVisibility(View.VISIBLE);

        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);
        Calendar lastDay = Calendar.getInstance();
        lastDay.add(Calendar.DATE, -1);

        datePickerLineChart.init(lastYear.getTime(),new Date())
                .withSelectedDate(lastDay.getTime())
                .inMode(CalendarPickerView.SelectionMode.RANGE);
    }

    private void checkTypeRadiation(){
        if(typeRadiationLineChart.equals(SOLAR_TYPE))
            initIntervalLineChart();
        else if(typeRadiationLineChart.equals(UV_TYPE))
            initIntervalLineChart();
        else if(typeRadiationLineChart.equals(UV_DOSE_TYPE))
            initIntervalLineChart();
    }

    private void drawLineGraph() {
        lineChart.setOnChartValueSelectedListener(this);
        // no description text
        lineChart.getDescription().setEnabled(false);
        // enable touch gestures
        lineChart.setTouchEnabled(true);
        lineChart.setDragDecelerationFrictionCoef(0.9f);
        // enable scaling and dragging
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        lineChart.setDrawGridBackground(false);
        lineChart.setHighlightPerDragEnabled(true);
        // if disabled, scaling can be done on x- and y-axis separately
        lineChart.setPinchZoom(true);
        // set an alternative background color
        lineChart.setBackgroundColor(Color.WHITE);
    }

    private void setLegend() {
        lineChart.animateX(2500);

        // get the legend (only possible after setting data)
        Legend l = lineChart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);
        //   l.setTypeface(mTfLight);
        l.setTextSize(11f);
        l.setTextColor(Color.GRAY);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
//        l.setYOffset(11f);

        XAxis xAxis = lineChart.getXAxis();
        //  xAxis.setTypeface(mTfLight);
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.GRAY);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);

        YAxis leftAxis = lineChart.getAxisLeft();
        //   leftAxis.setTypeface(mTfLight);
        leftAxis.setTextColor(Color.BLACK);
        leftAxis.setAxisMinimum(0);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);

        YAxis rightAxis = lineChart.getAxisRight();
        //   leftAxis.setTypeface(mTfLight);
        rightAxis.setTextColor(Color.BLACK);
        rightAxis.setAxisMinimum(0);
        rightAxis.setDrawGridLines(true);
        rightAxis.setGranularityEnabled(true);
    }

    private void setData(List<Radiation> radiationGraphList) {
        ArrayList<Entry> yValues = new ArrayList<>();
        for (int i = 0; i < radiationGraphList.size(); i++) {
            if (typeRadiationLineChart.equals(SOLAR_TYPE)) {
                yValues.add(new Entry(i, (float) radiationGraphList.get(i).SolarRadAvg));
            } else if (typeRadiationLineChart.equals(UV_TYPE)) {
                yValues.add(new Entry(i, (float) radiationGraphList.get(i).UVndexAvg));
            } else if (typeRadiationLineChart.equals(UV_DOSE_TYPE)) {
                yValues.add(new Entry(i, (float) radiationGraphList.get(i).UVdoseAvg));
            }
        }
        LineDataSet set3;
        if (lineChart.getData() != null &&
                lineChart.getData().getDataSetCount() > 0) {
            set3 = (LineDataSet) lineChart.getData().getDataSetByIndex(0);
            set3.setValues(yValues);
            lineChart.getData().notifyDataChanged();
            lineChart.notifyDataSetChanged();
        } else {
            set3 = new LineDataSet(yValues, "Radiation");
            set3.setAxisDependency(YAxis.AxisDependency.LEFT);
            set3.setColor(getResources().getColor(R.color.colorPrimary));
            set3.setCircleColor(Color.GRAY);
            set3.setLineWidth(2f);
            set3.setCircleRadius(3f);
            set3.setFillAlpha(65);
            set3.setFillColor(ColorTemplate.colorWithAlpha(Color.YELLOW, 200));
            set3.setDrawCircleHole(false);
            set3.setHighLightColor(Color.rgb(244, 117, 117));

            // create a data object with the datasets
            LineData data = new LineData(set3);
            data.setValueTextColor(Color.GRAY);
            data.setValueTextSize(9f);

            lineChart.setData(data);
        }
        lineChart.invalidate();
    }

    private void initIntervalLineChart() {
        Call<ResponseBody> getMinLineTemper = API.getApiRequestService().GraphicRadiationDay(String.valueOf(USM.getStation()),
                USM.getUserID(), USM.getUserToken(), intervalLineChart);

        getMinLineTemper.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String result = API.getResponse(response.body().byteStream());
                Log.wtf("call",call.request().url().toString());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Radiation>>() {}.getType();
                    radiationGraphList = gson.fromJson(result, listType);
                    if (radiationGraphList.size() > 0) {
                        setData(radiationGraphList);
                        setLegend();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    private void initFromToLineChart() {
        Call<ResponseBody> getColumnTemper = API.getApiRequestService().GraphicRadiationFromToDay(String.valueOf(USM.getStation()),
                USM.getUserID(), USM.getUserToken(), startDateLineChart, endDateLineChart);

        getColumnTemper.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Radiation>>() {}.getType();
                    radiationGraphList = gson.fromJson(result, listType);
                    if (radiationGraphList.size() > 0) {
                        setData(radiationGraphList);
                        setLegend();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

        //End Line Chart -------------------------------------------------------------------------
    }

    // Start Radiation Periods -------------------------------------------------------------------
    @OnClick(R.id.calendar_drought_period)
    protected void calendarDroughtPeriodOnClick(){
        initDatePickerDroughtPeriod();
    }

    @OnClick(R.id.btn_pick_date_drought_period)
    protected void btnPickDateDroughtPeriodOnClick(){
        datePickerDroughtPeriodContainer.setVisibility(View.GONE);
        ArrayList<Date> selectedDates = (ArrayList<Date>) datePickerDroughtPeriod.getSelectedDates();

        if(selectedDates.size()>1){
            startDateDroughtPeriod = getDate(selectedDates.get(0));
            endDateDroughtPeriod = getDate(selectedDates.get(selectedDates.size()-1));

            startDateTitleDroughtPeriod = getTitleDate(selectedDates.get(0));
            endDateTitleDroughtPeriod = getTitleDate(selectedDates.get(selectedDates.size()-1));
            titleDroughtPeriod.setText("Drought period for " + startDateTitleDroughtPeriod + " - " + endDateTitleDroughtPeriod);

            initFromToDroughtPeriod();
        }
    }

    private void initDatePickerDroughtPeriod(){
        datePickerDroughtPeriodContainer.setVisibility(View.VISIBLE);

        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);
        Calendar lastDay = Calendar.getInstance();
        lastDay.add(Calendar.DATE, -1);

        datePickerDroughtPeriod.init(lastYear.getTime(),new Date())
                .withSelectedDate(lastDay.getTime())
                .inMode(CalendarPickerView.SelectionMode.RANGE);
    }

    @OnClick(R.id.calendar_sunny_days)
    protected void calendarSunnyDaysOnClick(){
        initDatePickerSunnyDays();
    }

    @OnClick(R.id.btn_pick_date_sunny_days)
    protected void btnPickDateSunnyDaysOnClick(){
        datePickerSunnyDaysContainer.setVisibility(View.GONE);
        ArrayList<Date> selectedDates = (ArrayList<Date>) datePickerSunnyDays.getSelectedDates();

        if(selectedDates.size()>1){
            startDateSunnyDays = getDate(selectedDates.get(0));
            endDateSunnyDays = getDate(selectedDates.get(selectedDates.size()-1));

            startDateTitleSunnyDays = getTitleDate(selectedDates.get(0));
            endDateTitleSunnyDays = getTitleDate(selectedDates.get(selectedDates.size()-1));
            titleSunnyDays.setText("Number of sunny days for " + startDateTitleSunnyDays + " - " + endDateTitleSunnyDays);

            initFromToSunnyDays();
        }
    }

    private void initDatePickerSunnyDays(){
        datePickerSunnyDaysContainer.setVisibility(View.VISIBLE);

        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);
        Calendar lastDay = Calendar.getInstance();
        lastDay.add(Calendar.DATE, -1);

        datePickerSunnyDays.init(lastYear.getTime(),new Date())
                .withSelectedDate(lastDay.getTime())
                .inMode(CalendarPickerView.SelectionMode.RANGE);
    }

    @OnClick(R.id.calendar_cloudy_days)
    protected void calendarCloudyDaysOnClick(){
        initDatePickerCloudyDays();
    }

    @OnClick(R.id.btn_pick_date_cloudy_days)
    protected void btnPickDateCloudyDaysOnClick(){
        datePickerCloudyDaysContainer.setVisibility(View.GONE);
        ArrayList<Date> selectedDates = (ArrayList<Date>) datePickerCloudyDays.getSelectedDates();

        if(selectedDates.size()>1){
            startDateCloudyDays= getDate(selectedDates.get(0));
            endDateCloudyDays = getDate(selectedDates.get(selectedDates.size()-1));

            startDateTitleCloudyDays = getTitleDate(selectedDates.get(0));
            endDateTitleCloudyDays = getTitleDate(selectedDates.get(selectedDates.size()-1));
            titleCloudyDays.setText("Number of cloudy days for " + startDateTitleCloudyDays + " - " + endDateTitleCloudyDays);

            initFromToCloudyDays();
        }
    }

    private void initDatePickerCloudyDays(){
        datePickerCloudyDaysContainer.setVisibility(View.VISIBLE);

        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);
        Calendar lastDay = Calendar.getInstance();
        lastDay.add(Calendar.DATE, -1);

        datePickerCloudyDays.init(lastYear.getTime(),new Date())
                .withSelectedDate(lastDay.getTime())
                .inMode(CalendarPickerView.SelectionMode.RANGE);
    }

    private void initIntervalDroughtPeriod(){
        Call<ResponseBody> getBarChartRadiation = null;

        getBarChartRadiation = API.getApiRequestService().DurSunnyDays(String.valueOf(USM.getStation()), USM.getUserID(),
                USM.getUserToken(), intervalDroughtPeriod);

        getBarChartRadiation.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Radiation>() {}.getType();
                    radiation = gson.fromJson(result, listType);
                    daysDroughtPeriod.setText(radiation.DurSunnyDays + " days");
                } catch (Exception e) {
                    Log.wtf("call",call.request().url().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private void initIntervalSunnyDays(){
        Call<ResponseBody> getBarChartRadiation = null;

        getBarChartRadiation = API.getApiRequestService().SunnyDays(String.valueOf(USM.getStation()), USM.getUserID(),
                USM.getUserToken(), intervalSunnyDays);

        getBarChartRadiation.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Radiation>() {}.getType();
                    radiation = gson.fromJson(result, listType);
                    daysSunnyDays.setText(radiation.SunnyDays + " days");
                } catch (Exception e) {
                    Log.wtf("call",call.request().url().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private void initIntervalCloudyDays(){
        Call<ResponseBody> getBarChartRadiation = null;

        getBarChartRadiation = API.getApiRequestService().CloudyDays(String.valueOf(USM.getStation()), USM.getUserID(),
                USM.getUserToken(), intervalCloudyDays);

        getBarChartRadiation.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Radiation>() {}.getType();
                    radiation = gson.fromJson(result, listType);
                    daysCloudyDays.setText(radiation.Cloudydays + " days");
                } catch (Exception e) {
                    Log.wtf("call",call.request().url().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private void initFromToDroughtPeriod(){
        Call<ResponseBody> getBarChartRadiation = null;

        getBarChartRadiation = API.getApiRequestService().DurSunnyDays(String.valueOf(USM.getStation()), USM.getUserID(),
                USM.getUserToken(), startDateDroughtPeriod,endDateDroughtPeriod);

        getBarChartRadiation.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Radiation>() {}.getType();
                    radiation = gson.fromJson(result, listType);
                    daysDroughtPeriod.setText(radiation.DurSunnyDays + " days");
                } catch (Exception e) {
                    Log.wtf("call",call.request().url().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private void initFromToSunnyDays(){
        Call<ResponseBody> getBarChartRadiation = null;

        getBarChartRadiation = API.getApiRequestService().SunnyDays(String.valueOf(USM.getStation()), USM.getUserID(),
                USM.getUserToken(), startDateSunnyDays,endDateSunnyDays);

        getBarChartRadiation.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Radiation>() {}.getType();
                    radiation = gson.fromJson(result, listType);
                    daysSunnyDays.setText(radiation.SunnyDays + " days");
                } catch (Exception e) {
                    Log.wtf("call",call.request().url().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private void initFromToCloudyDays(){
        Call<ResponseBody> getBarChartRadiation = null;

        getBarChartRadiation = API.getApiRequestService().CloudyDays(String.valueOf(USM.getStation()), USM.getUserID(),
                USM.getUserToken(), startDateCloudyDays,endDateCloudyDays);

        getBarChartRadiation.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Radiation>() {}.getType();
                    radiation = gson.fromJson(result, listType);
                    daysCloudyDays.setText(radiation.Cloudydays + " days");
                } catch (Exception e) {
                    Log.wtf("call",call.request().url().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }
}
