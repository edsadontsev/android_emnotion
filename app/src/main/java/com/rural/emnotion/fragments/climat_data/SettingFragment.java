package com.rural.emnotion.fragments.climat_data;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rural.emnotion.MainActivity;
import com.rural.emnotion.R;
import com.rural.emnotion.settings.USM;

public class SettingFragment extends Fragment {

    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).showSpinner(false);
    }
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.settings, container, false);
        USM.init(getActivity());
        TextView email = (TextView) view.findViewById(R.id.uEmail);
        TextView phone= (TextView) view.findViewById(R.id.uPhone);
        email.setText(USM.getUserEmail());
        phone.setText(USM.getUserPhone());
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
