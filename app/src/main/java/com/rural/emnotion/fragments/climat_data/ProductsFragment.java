package com.rural.emnotion.fragments.climat_data;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rural.emnotion.MainActivity;
import com.rural.emnotion.R;
import com.rural.emnotion.activities.CreateProductsActivity;
import com.rural.emnotion.adapters.ProductsAdapter;
import com.rural.emnotion.models.Product;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductsFragment extends Fragment {


    public ProductsFragment() {
        // Required empty public constructor
    }

    private RecyclerView recyclerView;
    private ProductsAdapter adapter;
    private CardView addProduct;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_products, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        addProduct = (CardView) view.findViewById(R.id.add_product);
        addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),CreateProductsActivity.class));
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).showSpinner(false);
        getAllProducts();
    }

    private void getAllProducts() {
        USM.init(getActivity());
        Call<List<Product>> getSpecialists = API.getApiRequestService().getMyProducts(USM.getUserID(),
                USM.getUserToken());
        getSpecialists.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                Log.wtf("call",call.request().url().toString());
                adapter = new ProductsAdapter(getActivity(), response.body());
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
            }
        });
    }
}
