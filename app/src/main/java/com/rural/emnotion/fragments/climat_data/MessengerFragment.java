package com.rural.emnotion.fragments.climat_data;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.rural.emnotion.MainActivity;
import com.rural.emnotion.R;
import com.rural.emnotion.adapters.ChatsAdapter;
import com.rural.emnotion.adapters.SearchSpecialistAdapter;
import com.rural.emnotion.models.Message;
import com.rural.emnotion.models.Specialist;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessengerFragment extends Fragment {


    public MessengerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).showSpinner(false);
        getChats();
    }


    private RecyclerView recyclerView;

    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_messenger, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return view;
    }

    private void getChats(){
        USM.init(getActivity());
        Call<List<Message>> getSpecialists  = API.getApiRequestService().getChats(USM.getUserID(),
                USM.getUserToken());
        getSpecialists.enqueue(new Callback<List<Message>>() {
            @Override
            public void onResponse(Call<List<Message>> call, Response<List<Message>> response) {
                ChatsAdapter adapter = new ChatsAdapter(getActivity(), response.body());
                recyclerView.setAdapter(adapter);
            }
            @Override
            public void onFailure(Call<List<Message>> call, Throwable t) {
            }
        });
    }
}

