package com.rural.emnotion.fragments.climat_data;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.rural.emnotion.MainActivity;
import com.rural.emnotion.R;
import com.rural.emnotion.adapters.SearchSpecialistAdapter;
import com.rural.emnotion.models.Specialist;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchSpecialistFragment extends Fragment {


    public SearchSpecialistFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).showSpinner(false);
    }


    private RecyclerView recyclerView;
    private SearchSpecialistAdapter adapter;
    private List<Specialist> specialistList;
    private EditText keyword;
    private EditText location;
    private ImageView searchButton;

    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_search_specialist, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        keyword = (EditText) view.findViewById(R.id.keyword);
        location = (EditText) view.findViewById(R.id.location);
        searchButton = (ImageView) view.findViewById(R.id.btn_search);

        specialistList = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        getAllSpecialists();

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if(!keyword.getText().toString().equals("") && !location.getText().toString().equals("")){
                    searchSpecialist();
                }
                else{
                    getAllSpecialists();
                }
            }
        });

        return view;
    }

    private void getAllSpecialists(){
        USM.init(getActivity());
        Call<List<Specialist>> getSpecialists  = API.getApiRequestService().getAllSpecialists(USM.getUserID(),
                USM.getUserToken());
        getSpecialists.enqueue(new Callback<List<Specialist>>() {
            @Override
            public void onResponse(Call<List<Specialist>> call, Response<List<Specialist>> response) {
                Log.wtf("call",call.request().url().toString());
                specialistList = response.body();
                adapter = new SearchSpecialistAdapter(getActivity(), specialistList);
                recyclerView.setAdapter(adapter);
            }
            @Override
            public void onFailure(Call<List<Specialist>> call, Throwable t) {
            }
        });
    }

    private void searchSpecialist(){
        USM.init(getActivity());
        Call<List<Specialist>> getSpecialists  = API.getApiRequestService().searchSpecialist(keyword.getText().toString(),
                location.getText().toString(),USM.getUserID(), USM.getUserToken());
        getSpecialists.enqueue(new Callback<List<Specialist>>() {
            @Override
            public void onResponse(Call<List<Specialist>> call, Response<List<Specialist>> response) {
                Log.wtf("call",call.request().url().toString());
                specialistList = response.body();
                adapter = new SearchSpecialistAdapter(getActivity(), specialistList);
                recyclerView.setAdapter(adapter);
            }
            @Override
            public void onFailure(Call<List<Specialist>> call, Throwable t) {
            }
        });
    }
}
