package com.rural.emnotion.fragments.climat_data;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rural.emnotion.MainActivity;
import com.rural.emnotion.R;
import com.rural.emnotion.adapters.ImpactAdapter;
import com.rural.emnotion.models.Impact;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForecastFragment extends Fragment implements ImpactAdapter.OnShowClickListener {

    private RecyclerView recyclerView;
    private TextView description;
    private TextView solution;
    private ImageView arrow;

    public ForecastFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).showSpinner(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forecast, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        description = view.findViewById(R.id.description);
        solution = view.findViewById(R.id.solution);
        arrow = view.findViewById(R.id.arrow);

        getImpacts();

        return  view;
    }

    private void getImpacts(){
        USM.init(getActivity());
        Call<List<Impact>> getSpecialists = API.getApiRequestService().getImpacts(USM.getUserID(),USM.getStation(),USM.getUserToken());
        getSpecialists.enqueue(new Callback<List<Impact>>() {
            @Override
            public void onResponse(Call<List<Impact>> call, Response<List<Impact>> response) {
                Log.wtf("call",call.request().url().toString());
                ImpactAdapter impactAdapter = new ImpactAdapter(getContext(),response.body());
                impactAdapter.setOnShowClickListener(ForecastFragment.this);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(impactAdapter);

                if(response.body().size()!=0){
                    getImpactById(response.body().get(0).IdAlarmRisk);
                }
            }

            @Override
            public void onFailure(Call<List<Impact>> call, Throwable t) {
            }
        });
    }

    @Override
    public void onShowClick(int id) {
        getImpactById(id);
    }

    private void getImpactById(int id){
        USM.init(getActivity());
        Call<List<Impact>> getSpecialists = API.getApiRequestService().getImpactById(USM.getUserID(),id,USM.getUserToken());
        getSpecialists.enqueue(new Callback<List<Impact>>() {
            @Override
            public void onResponse(Call<List<Impact>> call, Response<List<Impact>> response) {
                arrow.setRotation(response.body().get(0).Level);
                solution.setText(response.body().get(0).Solution);
                description.setText(response.body().get(0).AlarmRiskName +"\n" +
                        response.body().get(0).CalcTimeToAlarm +"\n" +
                        response.body().get(0).Description);
            }

            @Override
            public void onFailure(Call<List<Impact>> call, Throwable t) {
            }
        });
    }
}
