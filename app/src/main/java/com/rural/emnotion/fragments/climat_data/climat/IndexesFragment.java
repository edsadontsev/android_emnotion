package com.rural.emnotion.fragments.climat_data.climat;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rural.emnotion.MainActivity;
import com.rural.emnotion.R;
import com.rural.emnotion.charts.IndexesAxisValueFormatter;
import com.rural.emnotion.charts.MyAxisValueFormatter;
import com.rural.emnotion.charts.XYMarkerView;
import com.rural.emnotion.fragments.climat_data.BaseFragment;
import com.rural.emnotion.models.Index;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;
import com.squareup.timessquare.CalendarPickerView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.rural.emnotion.util.DateUtils.getDate;
import static com.rural.emnotion.util.DateUtils.getTitleDate;

public class IndexesFragment extends BaseFragment {

    private static final String AVG_TYPE_1 = " chill wind average ";
    private static final String AVG_TYPE_2 = " heat index average ";
    private static final String AVG_TYPE_3 = " THW index average ";
    private static final String AVG_TYPE_4 = " THSW index average ";
    private static final String AVG_TYPE_5 = " ET index average ";
    private static final String AVG_TYPE_6 = " dew point index average ";

    private static final String TIMELINE_TYPE_1 = " last rain timeline ";
    private static final String TIMELINE_TYPE_2 = "Lst sunny day timeline ";
    private static final String TIMELINE_TYPE_3 = "Low solar radiation timeline ";
    private static final String TIMELINE_TYPE_4 = "High solar radiation timeline ";
    private static final String TIMELINE_TYPE_5 = "UV low timeline ";
    private static final String TIMELINE_TYPE_6 = "UV hi timeline ";

    @BindView(R.id.wind_chill) TextView windChill;
    @BindView(R.id.heat_index) TextView heatIndex;
    @BindView(R.id.thw_index) TextView thwIndex;
    @BindView(R.id.thsw_index) TextView thswIndex;
    @BindView(R.id.current_dev_point) TextView currentDevPoint;
    @BindView(R.id.evapotranspiration_et) TextView evapotranspirationEt;

    @BindView(R.id.bar_chart) BarChart barChart;
    @BindView(R.id.title_bar_chart) TextView titleBarChart;
    @BindView(R.id.spinner_bar_chart) MaterialSpinner spinnerBarChart;
    @BindView(R.id.day_bar_chart) ImageView dayBarChart;
    @BindView(R.id.night_bar_chart) ImageView nightBarChart;
    @BindView(R.id.calendar_bar_chart) ImageView calendarBarChart;
    @BindView(R.id.date_picker_bar_chart) CalendarPickerView datePickerBarChart;
    @BindView(R.id.date_picker_bar_chart_container) RelativeLayout datePickerBarChartContainer;

    @BindView(R.id.line_chart_first) LineChart lineChartFirst;
    @BindView(R.id.title_line_chart_first) TextView titleLineChartFirst;
    @BindView(R.id.spinner_line_chart_first) MaterialSpinner spinnerLineChartFirst;
    @BindView(R.id.day_line_chart_first) ImageView dayLineChartFirst;
    @BindView(R.id.night_line_chart_first) ImageView nightLineChartFirst;
    @BindView(R.id.calendar_line_chart_first) ImageView calendarLineChartFirst;
    @BindView(R.id.date_picker_line_chart_first) CalendarPickerView datePickerLineChartFirst;
    @BindView(R.id.date_picker_line_chart_first_container) RelativeLayout datePickerLineChartFirstContainer;
    @BindView(R.id.avg_wind_chill) CardView avgWindChill;
    @BindView(R.id.avg_heat_index) CardView avgHeatIndex;
    @BindView(R.id.avg_thw) CardView avgThw;
    @BindView(R.id.avg_thsw) CardView avgTsw;
    @BindView(R.id.avg_et) CardView avgEt;
    @BindView(R.id.avg_dew_point) CardView avgDewPoint;

    @BindView(R.id.line_chart_second) LineChart lineChartSecond;
    @BindView(R.id.title_line_chart_second) TextView titleLineChartSecond;
    @BindView(R.id.spinner_line_chart_second) MaterialSpinner spinnerLineChartSecond;
    @BindView(R.id.day_line_chart_second) ImageView dayLineChartSecond;
    @BindView(R.id.night_line_chart_second) ImageView nightLineChartSecond;
    @BindView(R.id.calendar_line_chart_second) ImageView calendarLineChartSecond;
    @BindView(R.id.date_picker_line_chart_second) CalendarPickerView datePickerLineChartSecond;
    @BindView(R.id.date_picker_line_chart_second_container) RelativeLayout datePickerLineChartSecondContainer;
    @BindView(R.id.timeline_last_rain) CardView timelineLastRain;
    @BindView(R.id.timeline_last_sunny_day) CardView timelineLastSunnyDay;
    @BindView(R.id.timeline_low_solar_radiation) CardView timelineLowSolarRadiation;
    @BindView(R.id.timeline_high_solar_radiation) CardView timelineHighSolarRadiation;
    @BindView(R.id.timeline_uv_low) CardView timelineUvLow;
    @BindView(R.id.timeline_uv_high) CardView timelineUvHigh;
    
    @BindColor(R.color.colorPrimary) int blue;
    @BindColor(R.color.colorOrange) int orange;

    private String partOfDayBarChart;
    private String intervalBarChart;
    private boolean isDayBarChart;
    private boolean isCalendarBarChart;
    private String startDateBarChart;
    private String endDateBarChart;
    private String startDateTitleBarChart;
    private String endDateTitleBarChart;

    private String partOfDayLineChartFirst;
    private String intervalLineChartFirst;
    private boolean isDayLineChartFirst;
    private boolean isCalendarLineChartFirst;
    private String typeIndexLineChartFirst;
    private String startDateLineChartFirst;
    private String endDateLineChartFirst;
    private String startDateTitleLineChartFirst;
    private String endDateTitleLineChartFirst;

    private String partOfDayLineChartSecond;
    private String intervalLineChartSecond;
    private boolean isDayLineChartSecond;
    private boolean isCalendarLineChartSecond;
    private String typeIndexLineChartSecond;
    private String startDateLineChartSecond;
    private String endDateLineChartSecond;
    private String startDateTitleLineChartSecond;
    private String endDateTitleLineChartSecond;

    private List<Index> indexGraphList;
    private Index index;

    public IndexesFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).showSpinner(true);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ind, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

        USM.init(getActivity());
        initObjects();
        initViews();
        initListeners();

        initHeaderIndexes();

        avgWindChill.performClick();
        drawLineGraphFirst();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                timelineLastRain.performClick();
                drawLineGraphSecond();
            }
        },1000);

        drawBarGraph();
    }

    private void initObjects(){
        partOfDayBarChart = "Day";
        intervalBarChart = "5 min";
        isDayBarChart = true;

        partOfDayLineChartFirst = "Day";
        intervalLineChartFirst = "last day";
        isDayLineChartFirst = true;

        partOfDayLineChartSecond = "Day";
        intervalLineChartSecond = "last day";
        isDayLineChartSecond = true;
    }

    private void initViews() {
        String[] intervalArray = getResources().getStringArray(R.array.indexes_intervals);
        spinnerBarChart.setItems(intervalArray);
        titleBarChart.setText(partOfDayBarChart + " indexes for " + intervalBarChart);

        String[] interval = getResources().getStringArray(R.array.general_intervals);
        spinnerLineChartFirst.setItems(interval);
        spinnerLineChartSecond.setItems(interval);
    }

    private void initListeners() {
        spinnerBarChart.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                isCalendarBarChart = false;
                Toast.makeText(getContext(), item.toString(), Toast.LENGTH_SHORT).show();
                intervalBarChart = item.toString();
                initBarChart();
                titleBarChart.setText(partOfDayBarChart + " indexes for " + intervalBarChart);
            }
        });

        spinnerLineChartFirst.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                isCalendarLineChartFirst = false;
                Toast.makeText(getContext(), item.toString(), Toast.LENGTH_SHORT).show();
                intervalLineChartFirst = item.toString();
                titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + "(°C) for " + intervalLineChartFirst);
                initIntervalIndex();
            }
        });

        spinnerLineChartSecond.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                isCalendarLineChartSecond = false;
                Toast.makeText(getContext(), item.toString(), Toast.LENGTH_SHORT).show();
                intervalLineChartSecond = item.toString();
                titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " for " + intervalLineChartSecond);
                initIntervalTimeline();
            }
        });
    }

    // Start Header ----------------------------------------------------------------------------

    private void initHeaderIndexes() {
        Call<ResponseBody> getTemper = API.getApiRequestService().Index(String.valueOf(USM.getStation()), USM.getUserID(), USM.getUserToken());
        getTemper.enqueue(new Callback<ResponseBody>() {
            @SuppressLint({"DefaultLocale", "SetTextI18n"})
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String result = API.getResponse(response.body().byteStream());
                Log.wtf("call",call.request().url().toString());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Index>() {}.getType();
                    index = gson.fromJson(result, listType);
                    String tempDim = USM.getTemperatureDim();

                    windChill.setText(String.format("%.2f", index.WindChill) + tempDim);
                    heatIndex.setText(String.format("%.2f", index.HeatIndex) + tempDim);
                    thwIndex.setText(String.format("%.2f", index.ThwIndex) + tempDim);
                    thswIndex.setText(String.format("%.2f", index.ThswIndex) + tempDim);
                    currentDevPoint.setText(String.format("%.2f", index.DewPt) + tempDim);
                    evapotranspirationEt.setText(String.format("%.2f", index.ET) + " mm");
                } catch (Exception e) {
                    Log.wtf("call",e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    // End Header ----------------------------------------------------------------------------

    // Start Bar Chart ----------------------------------------------------------------------------

    @OnClick({R.id.day_bar_chart,R.id.night_bar_chart})
    protected void partOfDayBarChartOnClick(){
        if (isDayBarChart) {
            dayBarChart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_day_noactiv));
            nightBarChart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_night_activ));
            isDayBarChart = false;
            partOfDayBarChart = "Night";
            if(isCalendarBarChart) {
                titleBarChart.setText(partOfDayBarChart + " indexes for " + startDateTitleBarChart + " - " + endDateTitleBarChart);
                initFromToBarChart();
            }
            else {
                titleBarChart.setText(partOfDayBarChart + " indexes for " + intervalBarChart);
                initBarChart();
            }
        } else {
            dayBarChart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_day_activ));
            nightBarChart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_night_noactiv));
            isDayBarChart = true;
            partOfDayBarChart = "Day";
            if(isCalendarBarChart) {
                titleBarChart.setText(partOfDayBarChart + " indexes for " + startDateTitleBarChart + " - " + endDateTitleBarChart);
                initFromToBarChart();
            }
            else {
                titleBarChart.setText(partOfDayBarChart + " indexes for " + intervalBarChart);
                initBarChart();
            }
        }
    }

    @OnClick(R.id.calendar_bar_chart)
    protected void calendarBarChartOnClick(){
        initDatePickerBarChart();
    }

    @OnClick(R.id.btn_pick_date_bar_chart)
    protected void btnPickDateBarChartOnClick(){
        datePickerBarChartContainer.setVisibility(View.GONE);
        ArrayList<Date> selectedDates = (ArrayList<Date>) datePickerBarChart.getSelectedDates();

        if(selectedDates.size()>1){
            isCalendarBarChart = true;
            startDateBarChart = getDate(selectedDates.get(0));
            endDateBarChart = getDate(selectedDates.get(selectedDates.size()-1));

            startDateTitleBarChart = getTitleDate(selectedDates.get(0));
            endDateTitleBarChart = getTitleDate(selectedDates.get(selectedDates.size()-1));
            titleBarChart.setText(partOfDayBarChart + " indexes for " + startDateTitleBarChart + " - " + endDateTitleBarChart);

            initFromToBarChart();
        }
    }

    private void initDatePickerBarChart(){
        datePickerBarChartContainer.setVisibility(View.VISIBLE);

        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);
        Calendar lastDay = Calendar.getInstance();
        lastDay.add(Calendar.DATE, -1);

        datePickerBarChart.init(lastYear.getTime(),new Date())
                .withSelectedDate(lastDay.getTime())
                .inMode(CalendarPickerView.SelectionMode.RANGE);
    }

    private void drawBarGraph() {
        barChart.setOnChartValueSelectedListener(this);
        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);
        barChart.getDescription().setEnabled(false);
        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        barChart.setMaxVisibleValueCount(60);
        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(false);
        // LineChartFirst.setDrawYLabels(false);
        IAxisValueFormatter xAxisFormatter = new IndexesAxisValueFormatter(barChart);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        // xAxis.setTypeface(mTfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);

        IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = barChart.getAxisLeft();
        // leftAxis.setTypeface(mTfLight);
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        // rightAxis.setTypeface(mTfLight);
        rightAxis.setLabelCount(8, false);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)

        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);

        XYMarkerView mv = new XYMarkerView(getActivity(), xAxisFormatter);
        mv.setChartView(barChart); // For bounds control
        barChart.setMarker(mv); // Set the marker to the chart
        initBarChart();
    }

    private void initBarChart() {
        Call<ResponseBody> getBarChartTemper = null;
        if (isDayBarChart)
            getBarChartTemper = API.getApiRequestService().IndDay(String.valueOf(USM.getStation()), USM.getUserID(), USM.getUserToken(), intervalBarChart);
        else
            getBarChartTemper = API.getApiRequestService().IndNight(String.valueOf(USM.getStation()), USM.getUserID(), USM.getUserToken(), intervalBarChart);

        getBarChartTemper.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Index>() {}.getType();
                    index = gson.fromJson(result, listType);
                    setBarData((float) index.WindChill, (float) index.HeatIndex, (float) index.ThwIndex,
                            (float) index.ThswIndex, (float) index.DewPt, (float) index.ET);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private void initFromToBarChart() {
        Call<ResponseBody> getColumnTemper = null;
        if (isDayBarChart) getColumnTemper = API.getApiRequestService().IndDay(String.valueOf(USM.getStation()),
                USM.getUserID(), USM.getUserToken(), startDateBarChart, endDateBarChart);
        else getColumnTemper = API.getApiRequestService().IndNight(String.valueOf(USM.getStation()),
                USM.getUserID(), USM.getUserToken(), startDateBarChart, endDateBarChart);

        getColumnTemper.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {

                    Gson gson = new Gson();
                    Type listType = new TypeToken<Index>() {}.getType();
                    index = gson.fromJson(result, listType);
                    setBarData((float) index.WindChill, (float) index.HeatIndex, (float) index.ThwIndex,
                            (float) index.ThswIndex, (float) index.DewPt, (float) index.ET);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void setBarData(float a, float b, float c, float d, float e, float f) {

        ArrayList<BarEntry> yValues = new ArrayList<BarEntry>();

        yValues.add(new BarEntry(0, a));
        yValues.add(new BarEntry(1, b));
        yValues.add(new BarEntry(2, c));
        yValues.add(new BarEntry(3, d));
        yValues.add(new BarEntry(4, e));

        BarDataSet set;

        if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {
            set = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set.setValues(yValues);
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();
        } else {
            set = new BarDataSet(yValues, "Index");
            set.setDrawIcons(false);
            set.setColors(getResources().getColor(R.color.colorPrimary));
            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set);
            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            // data.setValueTypeface(mTfLight);
            data.setBarWidth(0.6f);

            barChart.setData(data);
        }
        barChart.invalidate();
    }

    // End Bar Chart ----------------------------------------------------------------------------

    // Start Line Chart First ----------------------------------------------------------------------------

    @OnClick({R.id.day_line_chart_first,R.id.night_line_chart_first})
    protected void partOfDayLineChartOnClick() {
        if (isDayLineChartFirst) {
            dayLineChartFirst.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_day_noactiv));
            nightLineChartFirst.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_night_activ));
            isDayLineChartFirst = false;
            partOfDayLineChartFirst = "Night";
            if(isCalendarLineChartFirst) {
                titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst+ " (°C) for " +
                        startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);
                initFromToLineChart();
            }
            else {
                titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + " (°C) for " + intervalLineChartFirst);
                initIntervalIndex();
            }


        } else {
            dayLineChartFirst.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_day_activ));
            nightLineChartFirst.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_night_noactiv));
            isDayLineChartFirst = true;
            partOfDayLineChartFirst = "Day";
            if(isCalendarLineChartFirst) {
                titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + " (°C) for "
                        + startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);
                initFromToLineChart();
            }
            else {
                titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + " (°C) for " + intervalLineChartFirst);
                initIntervalIndex();
            }
        }
    }

    @OnClick(R.id.avg_wind_chill)
    protected void avgWindChillLineChartOnClick(){
        typeIndexLineChartFirst = AVG_TYPE_1;
        changeBgLineChartFirst();
        if(isCalendarLineChartFirst) {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + " (°C) for " +
                    startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);
            initFromToLineChart();
        }
        else {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + " (°C) for " + intervalLineChartFirst);
            initIntervalIndex();
        }
    }

    @OnClick(R.id.avg_heat_index)
    protected void avgHeatIndexLineChartOnClick(){
        typeIndexLineChartFirst = AVG_TYPE_2;
        changeBgLineChartFirst();
        if(isCalendarLineChartFirst) {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + " (°C) for "
                    + startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);
            initFromToLineChart();
        }
        else {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + " (°C) for " + intervalLineChartFirst);
            initIntervalIndex();
        }
    }

    @OnClick(R.id.avg_thw)
    protected void avgThwLineChartOnClick(){
        typeIndexLineChartFirst = AVG_TYPE_3;
        changeBgLineChartFirst();
        if(isCalendarLineChartFirst) {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + " (°C) for "
                    + startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);
            initFromToLineChart();
        }
        else {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + " (°C) for " + intervalLineChartFirst);
            initIntervalIndex();
        }
    }

    @OnClick(R.id.avg_thsw)
    protected void avgThswLineChartOnClick(){
        typeIndexLineChartFirst = AVG_TYPE_4;
        changeBgLineChartFirst();
        if(isCalendarLineChartFirst) {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + " (°C) for "
                    + startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);
            initFromToLineChart();
        }
        else {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + " (°C) for " + intervalLineChartFirst);
            initIntervalIndex();
        }
    }

    @OnClick(R.id.avg_et)
    protected void avgEtLineChartOnClick(){
        typeIndexLineChartFirst = AVG_TYPE_5;
        changeBgLineChartFirst();
        if(isCalendarLineChartFirst) {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + " (°C) for "
                    + startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);
            initFromToLineChart();
        }
        else {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + " (°C) for " + intervalLineChartFirst);
            initIntervalIndex();
        }
    }

    @OnClick(R.id.avg_dew_point)
    protected void avgDewPointLineChartOnClick(){
        typeIndexLineChartFirst = AVG_TYPE_6;
        changeBgLineChartFirst();
        if(isCalendarLineChartFirst) {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + " (°C) for "
                    + startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);
            initFromToLineChart();
        }
        else {
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + " (°C) for " + intervalLineChartFirst);
            initIntervalIndex();
        }
    }

    @OnClick(R.id.calendar_line_chart_first)
    protected void calendarLineChartOnClick(){
        initDatePickerLineChart();
    }

    @OnClick(R.id.btn_pick_date_line_chart_first)
    protected void btnPickDateLineChartOnClick(){
        datePickerLineChartFirstContainer.setVisibility(View.GONE);
        ArrayList<Date> selectedDates = (ArrayList<Date>) datePickerLineChartFirst.getSelectedDates();

        if(selectedDates.size()>1){
            isCalendarLineChartFirst = true;
            startDateLineChartFirst = getDate(selectedDates.get(0));
            endDateLineChartFirst = getDate(selectedDates.get(selectedDates.size()-1));

            startDateTitleLineChartFirst = getTitleDate(selectedDates.get(0));
            endDateTitleLineChartFirst = getTitleDate(selectedDates.get(selectedDates.size()-1));
            titleLineChartFirst.setText(partOfDayLineChartFirst + typeIndexLineChartFirst + " (°C) for "
                    + startDateTitleLineChartFirst + " - " + endDateTitleLineChartFirst);

            initFromToLineChart();
        }
    }

    private void changeBgLineChartFirst(){
        avgWindChill.setBackgroundColor(blue);
        avgHeatIndex.setBackgroundColor(blue);
        avgThw.setBackgroundColor(blue);
        avgTsw.setBackgroundColor(blue);
        avgEt.setBackgroundColor(blue);
        avgDewPoint.setBackgroundColor(blue);
        
        if(typeIndexLineChartFirst.equals(AVG_TYPE_1)) avgWindChill.setBackgroundColor(orange);
        else if(typeIndexLineChartFirst.equals(AVG_TYPE_2)) avgHeatIndex.setBackgroundColor(orange);
        else if(typeIndexLineChartFirst.equals(AVG_TYPE_3)) avgThw.setBackgroundColor(orange);
        else if(typeIndexLineChartFirst.equals(AVG_TYPE_4)) avgTsw.setBackgroundColor(orange);
        else if(typeIndexLineChartFirst.equals(AVG_TYPE_5)) avgEt.setBackgroundColor(orange);
        else if(typeIndexLineChartFirst.equals(AVG_TYPE_6)) avgDewPoint.setBackgroundColor(orange);
    }
    private void initDatePickerLineChart(){
        datePickerLineChartFirstContainer.setVisibility(View.VISIBLE);

        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);
        Calendar lastDay = Calendar.getInstance();
        lastDay.add(Calendar.DATE, -1);

        datePickerLineChartFirst.init(lastYear.getTime(),new Date())
                .withSelectedDate(lastDay.getTime())
                .inMode(CalendarPickerView.SelectionMode.RANGE);
    }

    private void drawLineGraphFirst() {
        lineChartFirst.setOnChartValueSelectedListener(this);
        // no description text
        lineChartFirst.getDescription().setEnabled(false);
        // enable touch gestures
        lineChartFirst.setTouchEnabled(true);
        lineChartFirst.setDragDecelerationFrictionCoef(0.9f);
        // enable scaling and dragging
        lineChartFirst.setDragEnabled(true);
        lineChartFirst.setScaleEnabled(true);
        lineChartFirst.setDrawGridBackground(false);
        lineChartFirst.setHighlightPerDragEnabled(true);
        // if disabled, scaling can be done on x- and y-axis separately
        lineChartFirst.setPinchZoom(true);
        // set an alternative background color
        lineChartFirst.setBackgroundColor(Color.WHITE);
    }

    private void setLegend() {
        lineChartFirst.animateX(2500);

        // get the legend (only possible after setting data)
        Legend l = lineChartFirst.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);
        //   l.setTypeface(mTfLight);
        l.setTextSize(11f);
        l.setTextColor(Color.GRAY);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
//        l.setYOffset(11f);

        XAxis xAxis = lineChartFirst.getXAxis();
        //  xAxis.setTypeface(mTfLight);
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.GRAY);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);

        YAxis leftAxis = lineChartFirst.getAxisLeft();
        //   leftAxis.setTypeface(mTfLight);
        leftAxis.setTextColor(Color.BLACK);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);

        YAxis rightAxis = lineChartFirst.getAxisRight();
        //   leftAxis.setTypeface(mTfLight);
        rightAxis.setTextColor(Color.BLACK);
        rightAxis.setDrawGridLines(true);
        rightAxis.setGranularityEnabled(true);
    }

    private void setData(List<Index> temperatureGraphList) {
        ArrayList<Entry> yValues = new ArrayList<>();
        for (int i = 0; i < temperatureGraphList.size(); i++) {
            if (typeIndexLineChartFirst.equals(AVG_TYPE_1)) {
                yValues.add(new Entry(i, (float) temperatureGraphList.get(i).WindChillAvg));
            } else if (typeIndexLineChartFirst.equals(AVG_TYPE_2)) {
                yValues.add(new Entry(i, (float) temperatureGraphList.get(i).HeatIndexAvg));
            } else if (typeIndexLineChartFirst.equals(AVG_TYPE_3)) {
                yValues.add(new Entry(i, (float) temperatureGraphList.get(i).ThwIndexAvg));
            }
            else if (typeIndexLineChartFirst.equals(AVG_TYPE_4)) {
                yValues.add(new Entry(i, (float) temperatureGraphList.get(i).ThswIndexAvg));
            }
            else if (typeIndexLineChartFirst.equals(AVG_TYPE_5)) {
                yValues.add(new Entry(i, (float) temperatureGraphList.get(i).ETAvg));
            }
            else if (typeIndexLineChartFirst.equals(AVG_TYPE_6)) {
                yValues.add(new Entry(i, (float) temperatureGraphList.get(i).DewPtAvg));
            }
        }
        LineDataSet set3;
        if (lineChartFirst.getData() != null &&
                lineChartFirst.getData().getDataSetCount() > 0) {
            set3 = (LineDataSet) lineChartFirst.getData().getDataSetByIndex(0);
            set3.setValues(yValues);
            lineChartFirst.getData().notifyDataChanged();
            lineChartFirst.notifyDataSetChanged();
        } else {
            set3 = new LineDataSet(yValues, "Indexes");
            set3.setAxisDependency(YAxis.AxisDependency.LEFT);
            set3.setColor(getResources().getColor(R.color.colorPrimary));
            set3.setCircleColor(Color.GRAY);
            set3.setLineWidth(2f);
            set3.setCircleRadius(3f);
            set3.setFillAlpha(65);
            set3.setFillColor(ColorTemplate.colorWithAlpha(Color.YELLOW, 200));
            set3.setDrawCircleHole(false);
            set3.setHighLightColor(Color.rgb(244, 117, 117));

            // create a data object with the datasets
            LineData data = new LineData(set3);
            data.setValueTextColor(Color.GRAY);
            data.setValueTextSize(9f);

            lineChartFirst.setData(data);
        }
        lineChartFirst.invalidate();
    }

    private void initIntervalIndex() {
        Call<ResponseBody> getAvgLineTemper = null;

        if(typeIndexLineChartFirst.equals(AVG_TYPE_5)) {
            if (isDayLineChartFirst)
                getAvgLineTemper = API.getApiRequestService().IndexGraphicETDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartFirst);
            else
                getAvgLineTemper = API.getApiRequestService().IndexGraphicETNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartFirst);
        }

        else{
            if (isDayLineChartFirst)
                getAvgLineTemper = API.getApiRequestService().IndexGraphicIndexDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartFirst);
            else
                getAvgLineTemper = API.getApiRequestService().IndexGraphicIndexNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartFirst);
        }

        getAvgLineTemper.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String result = API.getResponse(response.body().byteStream());
                Log.wtf("call",call.request().url().toString());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Index>>() {}.getType();
                    indexGraphList = gson.fromJson(result, listType);
                    if (indexGraphList.size() > 0) {
                        setData(indexGraphList);
                        setLegend();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void initFromToLineChart() {
        Call<ResponseBody> getColumnTemper = null;

        if(typeIndexLineChartFirst.equals(AVG_TYPE_5)) {
            if (isDayLineChartFirst)
                getColumnTemper = API.getApiRequestService().IndexGraphicETDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), startDateLineChartFirst, endDateLineChartFirst);
            else
                getColumnTemper = API.getApiRequestService().IndexGraphicETNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), startDateLineChartFirst, endDateLineChartFirst);
        }
        else{
            if (isDayLineChartFirst)
                getColumnTemper = API.getApiRequestService().IndexGraphicIndexDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), startDateLineChartFirst, endDateLineChartFirst);
            else
                getColumnTemper = API.getApiRequestService().IndexGraphicIndexNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), startDateLineChartFirst, endDateLineChartFirst);
        }

        getColumnTemper.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Index>>() {}.getType();
                    indexGraphList = gson.fromJson(result, listType);
                    if (indexGraphList.size() > 0) {
                        setData(indexGraphList);
                        setLegend();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

        //End Line Chart First -------------------------------------------------------------------------
    }

    // Start Line Chart Second ----------------------------------------------------------------------------

    @OnClick({R.id.day_line_chart_second,R.id.night_line_chart_second})
    protected void partOfDayLineChartSecondOnClick() {
        if (isDayLineChartSecond) {
            dayLineChartSecond.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_day_noactiv));
            nightLineChartSecond.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_night_activ));
            isDayLineChartSecond = false;
            partOfDayLineChartSecond = "Night";
            if(isCalendarLineChartSecond) {
                titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond+ " for " +
                        startDateTitleLineChartSecond + " - " + endDateTitleLineChartSecond);
                initFromToLineChartSecond();
            }
            else {
                titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " for " + intervalLineChartSecond);
                initIntervalTimeline();
            }


        } else {
            dayLineChartSecond.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_day_activ));
            nightLineChartSecond.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_night_noactiv));
            isDayLineChartSecond = true;
            partOfDayLineChartSecond = "Day";
            if(isCalendarLineChartSecond) {
                titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " for "
                        + startDateTitleLineChartSecond + " - " + endDateTitleLineChartSecond);
                initFromToLineChartSecond();
            }
            else {
                titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " (mm) for " + intervalLineChartSecond);
                initIntervalTimeline();
            }
        }
    }

    @OnClick(R.id.timeline_last_rain)
    protected void timelineLastRainLineChartOnClick(){
        Log.wtf("aaa","click");
        dayLineChartSecond.setVisibility(View.VISIBLE);
        nightLineChartSecond.setVisibility(View.VISIBLE);
        isDayLineChartSecond = true;
        partOfDayLineChartSecond = "Day";
        typeIndexLineChartSecond = TIMELINE_TYPE_1;
        changeBgLineChartSecond();
        if(isCalendarLineChartSecond) {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " for " +
                    startDateTitleLineChartSecond + " - " + endDateTitleLineChartSecond);
            initFromToLineChartSecond();
        }
        else {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " for " + intervalLineChartSecond);
            initIntervalTimeline();
        }
    }

    @OnClick(R.id.timeline_last_sunny_day)
    protected void timelineLastSunnyDatLineChartOnClick(){
        dayLineChartSecond.setVisibility(View.GONE);
        nightLineChartSecond.setVisibility(View.GONE);
        isDayLineChartSecond = true;
        partOfDayLineChartSecond = "";
        typeIndexLineChartSecond = TIMELINE_TYPE_2;
        changeBgLineChartSecond();
        if(isCalendarLineChartSecond) {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " for "
                    + startDateTitleLineChartSecond + " - " + endDateTitleLineChartSecond);
            initFromToLineChartSecond();
        }
        else {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " for " + intervalLineChartSecond);
            initIntervalTimeline();
        }
    }

    @OnClick(R.id.timeline_low_solar_radiation)
    protected void timelineSolarRadiationLineChartOnClick(){
        dayLineChartSecond.setVisibility(View.GONE);
        nightLineChartSecond.setVisibility(View.GONE);
        isDayLineChartSecond = true;
        partOfDayLineChartSecond = "";
        typeIndexLineChartSecond = TIMELINE_TYPE_3;
        changeBgLineChartSecond();
        if(isCalendarLineChartSecond) {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " for "
                    + startDateTitleLineChartSecond + " - " + endDateTitleLineChartSecond);
            initFromToLineChartSecond();
        }
        else {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " for " + intervalLineChartSecond);
            initIntervalTimeline();
        }
    }

    @OnClick(R.id.timeline_high_solar_radiation)
    protected void timelineHighSolarRadiationLineChartOnClick(){
        dayLineChartSecond.setVisibility(View.GONE);
        nightLineChartSecond.setVisibility(View.GONE);
        isDayLineChartSecond = true;
        partOfDayLineChartSecond = "";
        typeIndexLineChartSecond = TIMELINE_TYPE_4;
        changeBgLineChartSecond();
        if(isCalendarLineChartSecond) {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " for "
                    + startDateTitleLineChartSecond + " - " + endDateTitleLineChartSecond);
            initFromToLineChartSecond();
        }
        else {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " for " + intervalLineChartSecond);
            initIntervalTimeline();
        }
    }

    @OnClick(R.id.timeline_uv_low)
    protected void timeLineUvLowLineChartOnClick(){
        dayLineChartSecond.setVisibility(View.GONE);
        nightLineChartSecond.setVisibility(View.GONE);
        isDayLineChartSecond = true;
        partOfDayLineChartSecond = "";
        typeIndexLineChartSecond = TIMELINE_TYPE_5;
        changeBgLineChartSecond();
        if(isCalendarLineChartSecond) {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " for "
                    + startDateTitleLineChartSecond + " - " + endDateTitleLineChartSecond);
            initFromToLineChartSecond();
        }
        else {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " for " + intervalLineChartSecond);
            initIntervalTimeline();
        }
    }

    @OnClick(R.id.timeline_uv_high)
    protected void timelineUvHighLineChartOnClick(){
        dayLineChartSecond.setVisibility(View.GONE);
        nightLineChartSecond.setVisibility(View.GONE);
        isDayLineChartSecond = true;
        partOfDayLineChartSecond = "";
        typeIndexLineChartSecond = TIMELINE_TYPE_6;
        changeBgLineChartSecond();
        if(isCalendarLineChartSecond) {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " for "
                    + startDateTitleLineChartSecond + " - " + endDateTitleLineChartSecond);
            initFromToLineChartSecond();
        }
        else {
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " for " + intervalLineChartSecond);
            initIntervalTimeline();
        }
    }

    @OnClick(R.id.calendar_line_chart_second)
    protected void calendarLineChartSecondOnClick(){
        initDatePickerLineChartSecond();
    }

    @OnClick(R.id.btn_pick_date_line_chart_second)
    protected void btnPickDateLineChartSecondOnClick(){
        datePickerLineChartSecondContainer.setVisibility(View.GONE);
        ArrayList<Date> selectedDates = (ArrayList<Date>) datePickerLineChartSecond.getSelectedDates();

        if(selectedDates.size()>1){
            isCalendarLineChartSecond = true;
            startDateLineChartSecond = getDate(selectedDates.get(0));
            endDateLineChartSecond = getDate(selectedDates.get(selectedDates.size()-1));

            startDateTitleLineChartSecond = getTitleDate(selectedDates.get(0));
            endDateTitleLineChartSecond = getTitleDate(selectedDates.get(selectedDates.size()-1));
            titleLineChartSecond.setText(partOfDayLineChartSecond + typeIndexLineChartSecond + " for "
                    + startDateTitleLineChartSecond + " - " + endDateTitleLineChartSecond);

            initFromToLineChartSecond();
        }
    }

    private void changeBgLineChartSecond(){
        timelineLastRain.setBackgroundColor(blue);
        timelineLastSunnyDay.setBackgroundColor(blue);
        timelineLowSolarRadiation.setBackgroundColor(blue);
        timelineHighSolarRadiation.setBackgroundColor(blue);
        timelineUvLow.setBackgroundColor(blue);
        timelineUvHigh.setBackgroundColor(blue);

        if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_1)) timelineLastRain.setBackgroundColor(orange);
        else if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_2)) timelineLastSunnyDay.setBackgroundColor(orange);
        else if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_3)) timelineLowSolarRadiation.setBackgroundColor(orange);
        else if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_4)) timelineHighSolarRadiation.setBackgroundColor(orange);
        else if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_5)) timelineUvLow.setBackgroundColor(orange);
        else if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_6)) timelineUvHigh.setBackgroundColor(orange);
    }
    private void initDatePickerLineChartSecond(){
        datePickerLineChartSecondContainer.setVisibility(View.VISIBLE);

        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);
        Calendar lastDay = Calendar.getInstance();
        lastDay.add(Calendar.DATE, -1);

        datePickerLineChartSecond.init(lastYear.getTime(),new Date())
                .withSelectedDate(lastDay.getTime())
                .inMode(CalendarPickerView.SelectionMode.RANGE);
    }

    private void drawLineGraphSecond() {
        lineChartSecond.setOnChartValueSelectedListener(this);
        // no description text
        lineChartSecond.getDescription().setEnabled(false);
        // enable touch gestures
        lineChartSecond.setTouchEnabled(true);
        lineChartSecond.setDragDecelerationFrictionCoef(0.9f);
        // enable scaling and dragging
        lineChartSecond.setDragEnabled(true);
        lineChartSecond.setScaleEnabled(true);
        lineChartSecond.setDrawGridBackground(false);
        lineChartSecond.setHighlightPerDragEnabled(true);
        // if disabled, scaling can be done on x- and y-axis separately
        lineChartSecond.setPinchZoom(true);
        // set an alternative background color
        lineChartSecond.setBackgroundColor(Color.WHITE);
    }

    private void setLegendSecond() {
        lineChartSecond.animateX(2500);

        // get the legend (only possible after setting data)
        Legend l = lineChartSecond.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);
        //   l.setTypeface(mTfLight);
        l.setTextSize(11f);
        l.setTextColor(Color.GRAY);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
//        l.setYOffset(11f);

        XAxis xAxis = lineChartSecond.getXAxis();
        //  xAxis.setTypeface(mTfLight);
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.GRAY);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);

        YAxis leftAxis = lineChartSecond.getAxisLeft();
        //   leftAxis.setTypeface(mTfLight);
        leftAxis.setTextColor(Color.BLACK);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);

        YAxis rightAxis = lineChartSecond.getAxisRight();
        //   leftAxis.setTypeface(mTfLight);
        rightAxis.setTextColor(Color.BLACK);
        rightAxis.setDrawGridLines(true);
        rightAxis.setGranularityEnabled(true);
    }

    private void setDataSecond(List<Index> temperatureGraphList) {
        ArrayList<Entry> yValues = new ArrayList<>();
        for (int i = 0; i < temperatureGraphList.size(); i++) {
            if (typeIndexLineChartSecond.equals(TIMELINE_TYPE_1)) {
                yValues.add(new Entry(i, (float) temperatureGraphList.get(i).LastRain));
            } else if (typeIndexLineChartSecond.equals(TIMELINE_TYPE_2)) {
                yValues.add(new Entry(i, (float) temperatureGraphList.get(i).SunDay));
            } else if (typeIndexLineChartSecond.equals(TIMELINE_TYPE_3)) {
                yValues.add(new Entry(i, (float) temperatureGraphList.get(i).LowSolar));
            }
            else if (typeIndexLineChartSecond.equals(TIMELINE_TYPE_4)) {
                yValues.add(new Entry(i, (float) temperatureGraphList.get(i).HiSolar));
            }
            else if (typeIndexLineChartSecond.equals(TIMELINE_TYPE_5)) {
                yValues.add(new Entry(i, (float) temperatureGraphList.get(i).LowUV));
            }
            else if (typeIndexLineChartSecond.equals(TIMELINE_TYPE_6)) {
                yValues.add(new Entry(i, (float) temperatureGraphList.get(i).HiUV));
            }
        }
        LineDataSet set3;
        if (lineChartSecond.getData() != null &&
                lineChartSecond.getData().getDataSetCount() > 0) {
            set3 = (LineDataSet) lineChartSecond.getData().getDataSetByIndex(0);
            set3.setValues(yValues);
            lineChartSecond.getData().notifyDataChanged();
            lineChartSecond.notifyDataSetChanged();
        } else {
            set3 = new LineDataSet(yValues, "Indexes");
            set3.setAxisDependency(YAxis.AxisDependency.LEFT);
            set3.setColor(getResources().getColor(R.color.colorPrimary));
            set3.setCircleColor(Color.GRAY);
            set3.setLineWidth(2f);
            set3.setCircleRadius(3f);
            set3.setFillAlpha(65);
            set3.setFillColor(ColorTemplate.colorWithAlpha(Color.YELLOW, 200));
            set3.setDrawCircleHole(false);
            set3.setHighLightColor(Color.rgb(244, 117, 117));

            // create a data object with the datasets
            LineData data = new LineData(set3);
            data.setValueTextColor(Color.GRAY);
            data.setValueTextSize(9f);

            lineChartSecond.setData(data);
        }
        lineChartSecond.invalidate();
    }

    private void initIntervalTimeline() {
        Call<ResponseBody> getAvgLineTemper = null;

        if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_1)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().IndexTimeLineLastRainDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartSecond);
            else
                getAvgLineTemper = API.getApiRequestService().IndexTimeLineLastRainNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartSecond);
        }

        else if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_2)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().IndexTimeLineLastSunDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartSecond);
        }

        else if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_3)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().IndexTimeLineLowSolar(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartSecond);
        }

        else if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_4)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().IndexTimeLineHiSolar(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartSecond);
        }

        else if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_5)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().IndexTimeLineLowUV(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartSecond);
        }

        else if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_6)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().IndexTimeLineHiUV(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), intervalLineChartSecond);
        }

        getAvgLineTemper.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String result = API.getResponse(response.body().byteStream());
                Log.wtf("call",call.request().url().toString());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Index>>() {}.getType();
                    indexGraphList = gson.fromJson(result, listType);
                    if (indexGraphList.size() > 0) {
                        setDataSecond(indexGraphList);
                        setLegendSecond();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void initFromToLineChartSecond() {
        Call<ResponseBody> getAvgLineTemper = null;

        if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_1)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().IndexTimeLineLastRainDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), startDateLineChartSecond, endDateLineChartSecond);
            else
                getAvgLineTemper = API.getApiRequestService().IndexTimeLineLastRainNight(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), startDateLineChartSecond, endDateLineChartSecond);
        }

        else if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_2)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().IndexTimeLineLastSunDay(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), startDateLineChartSecond, endDateLineChartSecond);
        }

        else if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_3)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().IndexTimeLineLowSolar(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), startDateLineChartSecond, endDateLineChartSecond);
        }

        else if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_4)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().IndexTimeLineHiSolar(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), startDateLineChartSecond, endDateLineChartSecond);
        }

        else if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_5)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().IndexTimeLineLowUV(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), startDateLineChartSecond, endDateLineChartSecond);
        }

        else if(typeIndexLineChartSecond.equals(TIMELINE_TYPE_6)) {
            if (isDayLineChartSecond)
                getAvgLineTemper = API.getApiRequestService().IndexTimeLineHiUV(String.valueOf(USM.getStation()),
                        USM.getUserID(), USM.getUserToken(), startDateLineChartSecond, endDateLineChartSecond);
        }

        getAvgLineTemper.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Index>>() {}.getType();
                    indexGraphList = gson.fromJson(result, listType);
                    if (indexGraphList.size() > 0) {
                        setDataSecond(indexGraphList);
                        setLegend();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

        //End Line Chart Second -------------------------------------------------------------------------
    }
}
