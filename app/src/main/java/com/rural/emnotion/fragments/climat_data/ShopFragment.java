package com.rural.emnotion.fragments.climat_data;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.rural.emnotion.MainActivity;
import com.rural.emnotion.R;
import com.rural.emnotion.adapters.ShopAdapter;
import com.rural.emnotion.adapters.SpecialistAdapter;
import com.rural.emnotion.models.Product;
import com.rural.emnotion.models.Specialist;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;
import com.rural.emnotion.views.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopFragment extends Fragment {


    public ShopFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).showSpinner(false);
    }

    private RecyclerView recyclerView;
    private ShopAdapter adapter;
    private List<Product> specialistList;
    private EditText name;
    private ImageView searchButton;
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;

    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_shop, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        name = (EditText) view.findViewById(R.id.name);
        searchButton = (ImageView) view.findViewById(R.id.btn_search);

        specialistList = new ArrayList<>();
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(mLayoutManager,5) {
            @Override
            public void onLoadMore(int page) {
                getAllProducts(page);
            }
        };
        recyclerView.addOnScrollListener(endlessRecyclerOnScrollListener);

        getAllProducts(1);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if(!name.getText().toString().equals("")){
                    searchProducts();
                }
                else{
                    getAllProducts(1);
                }
            }
        });

        return view;
    }

    private void getAllProducts(final int page){
        USM.init(getActivity());
        Call<List<Product>> getSpecialists  = API.getApiRequestService().getShopProduct(USM.getUserID(),
                USM.getUserToken(),page);
        getSpecialists.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                Log.wtf("call",call.request().url().toString());
                if (page == 1) {
                    endlessRecyclerOnScrollListener.resetState();
                    specialistList = response.body();
                    adapter = new ShopAdapter(getActivity(), specialistList);
                    recyclerView.setAdapter(adapter);
                }
                else{
                    adapter.getAlbumList().addAll(response.body());
                    adapter.notifyItemRangeChanged(adapter.getAlbumList().size()
                            - response.body().size(), response.body().size());
                }
            }
            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
            }
        });
    }

    private void searchProducts(){
        USM.init(getActivity());
        Call<List<Product>> getSpecialists  = API.getApiRequestService().searchShopProduct(USM.getUserID(), USM.getUserToken(),name.getText().toString());
        getSpecialists.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                Log.wtf("call",call.request().url().toString());
                specialistList = response.body();
                adapter = new ShopAdapter(getActivity(), specialistList);
                recyclerView.setAdapter(adapter);
            }
            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
            }
        });
    }
}
