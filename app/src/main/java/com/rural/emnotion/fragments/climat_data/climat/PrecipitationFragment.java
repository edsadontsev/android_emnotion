package com.rural.emnotion.fragments.climat_data.climat;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rural.emnotion.MainActivity;
import com.rural.emnotion.R;
import com.rural.emnotion.charts.PrecipitationXAxisValueFormatter;
import com.rural.emnotion.charts.PrecipitationYAxisValueFormatter;
import com.rural.emnotion.charts.XYMarkerView;
import com.rural.emnotion.fragments.climat_data.BaseFragment;
import com.rural.emnotion.models.Raining;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;
import com.squareup.timessquare.CalendarPickerView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.rural.emnotion.util.DateUtils.getDate;
import static com.rural.emnotion.util.DateUtils.getTitleDate;

public class PrecipitationFragment extends BaseFragment {

    @BindView(R.id.rain) TextView rain;
    @BindView(R.id.rain_rate) TextView rainRate;
    @BindView(R.id.rainfall_last_hour) TextView rainfallLastHour;
    @BindView(R.id.rainfall_totals) TextView rainfallTotals;
    @BindView(R.id.max_rain_rate) TextView maxRainRate;

    @BindView(R.id.bar_chart) BarChart barChart;
    @BindView(R.id.title_bar_chart) TextView titleBarChart;
    @BindView(R.id.spinner_bar_chart) MaterialSpinner spinnerBarChart;
    @BindView(R.id.day_bar_chart) ImageView dayBarChart;
    @BindView(R.id.night_bar_chart) ImageView nightBarChart;
    @BindView(R.id.calendar_bar_chart) ImageView calendarBarChart;
    @BindView(R.id.date_picker_bar_chart) CalendarPickerView datePickerBarChart;
    @BindView(R.id.date_picker_bar_chart_container) RelativeLayout datePickerBarChartContainer;

    @BindView(R.id.line_chart) LineChart lineChart;
    @BindView(R.id.title_line_chart) TextView titleLineChart;
    @BindView(R.id.spinner_line_chart) MaterialSpinner spinnerLineChart;
    @BindView(R.id.day_line_chart) ImageView dayLineChart;
    @BindView(R.id.night_line_chart) ImageView nightLineChart;
    @BindView(R.id.calendar_line_chart) ImageView calendarLineChart;
    @BindView(R.id.date_picker_line_chart) CalendarPickerView datePickerLineChart;
    @BindView(R.id.date_picker_line_chart_container) RelativeLayout datePickerLineChartContainer;
    @BindView(R.id.graph_rainfall) CardView graphRainfall;
    @BindView(R.id.time_last_rain) TextView timeLastRain;
    @BindView(R.id.number_of_rainy_days) TextView numberOfRainyDays;

    private String partOfDayBarChart;
    private String intervalBarChart;
    private boolean isDayBarChart;
    private boolean isCalendarBarChart;
    private String startDateBarChart;
    private String endDateBarChart;
    private String startDateTitleBarChart;
    private String endDateTitleBarChart;

    private String partOfDayLineChart;
    private String intervalLineChart;
    private boolean isDayLineChart;
    private boolean isCalendarLineChart;
    private String startDateLineChart;
    private String endDateLineChart;
    private String startDateTitleLineChart;
    private String endDateTitleLineChart;

    private List<Raining> rainingGraphList;
    private Raining raining;

    public PrecipitationFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showSpinner(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_precipitation, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

        USM.init(getActivity());
        initObjects();
        initViews();
        initListeners();

        initHeaderRaining();

        graphRainfall.performClick();

        drawLineGraph();
        drawBarGraph();
    }

    private void initObjects(){
        partOfDayBarChart = "Day";
        intervalBarChart = "last day";
        isDayBarChart = true;

        partOfDayLineChart = "Day";
        intervalLineChart = "last day";
        isDayLineChart = true;
    }

    private void initViews() {
        String[] intervalArray = getResources().getStringArray(R.array.general_intervals);
        spinnerBarChart.setItems(intervalArray);
        titleBarChart.setText(partOfDayBarChart + " precipitation (mm) for " + intervalBarChart);

        spinnerLineChart.setItems(intervalArray);
    }

    private void initListeners() {
        spinnerBarChart.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                isCalendarBarChart = false;
                Toast.makeText(getContext(), item.toString(), Toast.LENGTH_SHORT).show();
                intervalBarChart = item.toString();
                initBarChart();
                titleBarChart.setText(partOfDayBarChart + " precipitation (mm) for " + intervalBarChart);
            }
        });

        spinnerLineChart.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                isCalendarLineChart = false;
                Toast.makeText(getContext(), item.toString(), Toast.LENGTH_SHORT).show();
                intervalLineChart = item.toString();
                titleLineChart.setText(partOfDayLineChart + " graph rainfall (mm) for " + intervalLineChart);
                initIntervalLineChart();
            }
        });
    }

    // Start Header ----------------------------------------------------------------------------

    private void initHeaderRaining() {
        Call<ResponseBody> getTemper = API.getApiRequestService().Raining(String.valueOf(USM.getStation()), USM.getUserID(), USM.getUserToken());
        getTemper.enqueue(new Callback<ResponseBody>() {
            @SuppressLint({"DefaultLocale", "SetTextI18n"})
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Raining>>(){}.getType();
                    List<Raining> RainingList = gson.fromJson(result, listType);

                    Raining raining = RainingList.get(0);

                    rain.setText(String.format("%.1f", raining.Rain) + " mm");
                    rainRate.setText(String.format("%.1f", raining.RainRate) + " mm");
                    rainfallLastHour.setText(String.format("%.1f", raining.SumRain) + " mm");
                    rainfallTotals.setText(String.format("%.1f", raining.RainFall) + " mm");
                    maxRainRate.setText(String.format("%.1f", raining.MaxRainRate) + " mm");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    // End Header ----------------------------------------------------------------------------

    // Start Bar Chart ----------------------------------------------------------------------------

    @OnClick({R.id.day_bar_chart,R.id.night_bar_chart})
    protected void partOfDayBarChartOnClick(){
        if (isDayBarChart) {
            dayBarChart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_day_noactiv));
            nightBarChart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_night_activ));
            isDayBarChart = false;
            partOfDayBarChart = "Night";
            if(isCalendarBarChart) {
                titleBarChart.setText(partOfDayBarChart + " precipitation (mm) for " + startDateTitleBarChart + " - " + endDateTitleBarChart);
                initFromToBarChart();
            }
            else {
                titleBarChart.setText(partOfDayBarChart + " precipitation (mm) for " + intervalBarChart);
                initBarChart();
            }
        } else {
            dayBarChart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_day_activ));
            nightBarChart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_night_noactiv));
            isDayBarChart = true;
            partOfDayBarChart = "Day";
            if(isCalendarBarChart) {
                titleBarChart.setText(partOfDayBarChart + " precipitation (mm) for " + startDateTitleBarChart + " - " + endDateTitleBarChart);
                initFromToBarChart();
            }
            else {
                titleBarChart.setText(partOfDayBarChart + " precipitation (mm) for " + intervalBarChart);
                initBarChart();
            }
        }
    }

    @OnClick(R.id.calendar_bar_chart)
    protected void calendarBarChartOnClick(){
        initDatePickerBarChart();
    }

    @OnClick(R.id.btn_pick_date_bar_chart)
    protected void btnPickDateBarChartOnClick(){
        datePickerBarChartContainer.setVisibility(View.GONE);
        ArrayList<Date> selectedDates = (ArrayList<Date>) datePickerBarChart.getSelectedDates();

        if(selectedDates.size()>1){
            isCalendarBarChart = true;
            startDateBarChart = getDate(selectedDates.get(0));
            endDateBarChart = getDate(selectedDates.get(selectedDates.size()-1));

            startDateTitleBarChart = getTitleDate(selectedDates.get(0));
            endDateTitleBarChart = getTitleDate(selectedDates.get(selectedDates.size()-1));
            titleBarChart.setText(partOfDayBarChart + " precipitation (mm) for " + startDateTitleBarChart + " - " + endDateTitleBarChart);

            initFromToBarChart();
        }
    }

    private void initDatePickerBarChart(){
        datePickerBarChartContainer.setVisibility(View.VISIBLE);

        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);
        Calendar lastDay = Calendar.getInstance();
        lastDay.add(Calendar.DATE, -1);

        datePickerBarChart.init(lastYear.getTime(),new Date())
                .withSelectedDate(lastDay.getTime())
                .inMode(CalendarPickerView.SelectionMode.RANGE);
    }

    private void drawBarGraph() {
        barChart.setOnChartValueSelectedListener(this);
        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);
        barChart.getDescription().setEnabled(false);
        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        barChart.setMaxVisibleValueCount(60);
        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(false);
        // lineChart.setDrawYLabels(false);
        IAxisValueFormatter xAxisFormatter = new PrecipitationXAxisValueFormatter(barChart);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        // xAxis.setTypeface(mTfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);

        IAxisValueFormatter custom = new PrecipitationYAxisValueFormatter();

        YAxis leftAxis = barChart.getAxisLeft();
        // leftAxis.setTypeface(mTfLight);
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        // rightAxis.setTypeface(mTfLight);
        rightAxis.setLabelCount(8, false);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);

        XYMarkerView mv = new XYMarkerView(getActivity(), xAxisFormatter);
        mv.setChartView(barChart); // For bounds control
        barChart.setMarker(mv); // Set the marker to the chart
        initBarChart();
    }

    private void initBarChart() {
        Call<ResponseBody> getBarChartTemper = null;
        if (isDayBarChart)
            getBarChartTemper = API.getApiRequestService().RainDay(String.valueOf(USM.getStation()), USM.getUserID(), USM.getUserToken(), intervalBarChart);
        else
            getBarChartTemper = API.getApiRequestService().RainNight(String.valueOf(USM.getStation()), USM.getUserID(), USM.getUserToken(), intervalBarChart);

        getBarChartTemper.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Raining>() {}.getType();
                    raining = gson.fromJson(result, listType);
                    Log.wtf("aaa",(float) raining.SumRain+"");
                    setBarData((float) raining.SumRain, (float) raining.RainFall, (float) raining.MaxRainRate, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private void initFromToBarChart() {
        Call<ResponseBody> getColumnTemper = null;
        if (isDayBarChart) getColumnTemper = API.getApiRequestService().RainDateDay(String.valueOf(USM.getStation()),
                USM.getUserID(), USM.getUserToken(), startDateBarChart, endDateBarChart);
        else getColumnTemper = API.getApiRequestService().RainDateNight(String.valueOf(USM.getStation()),
                USM.getUserID(), USM.getUserToken(), startDateBarChart, endDateBarChart);

        getColumnTemper.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Raining>() {}.getType();
                    raining = gson.fromJson(result, listType);
                    setBarData((float) raining.SumRain, (float) raining.RainFall, (float) raining.MaxRainRate, true);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void setBarData(float min, float avg, float max, boolean update) {

        ArrayList<BarEntry> yValues = new ArrayList<>();

        yValues.add(new BarEntry(0, min));
        yValues.add(new BarEntry(1, avg));
        yValues.add(new BarEntry(2, max));

        BarDataSet set;

        if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {
            set = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set.setValues(yValues);
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();
        } else {
            set = new BarDataSet(yValues, "Precipitation");
            set.setDrawIcons(false);
            set.setColors(getResources().getColor(R.color.colorPrimary));
            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set);
            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            // data.setValueTypeface(mTfLight);
            data.setBarWidth(0.6f);

            barChart.setData(data);
        }
        barChart.invalidate();
    }

    // End Bar Chart ----------------------------------------------------------------------------

    // Start Line Chart ----------------------------------------------------------------------------

    @OnClick({R.id.day_line_chart,R.id.night_line_chart})
    protected void partOfDayLineChartOnClick() {
        if (isDayLineChart) {
            dayLineChart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_day_noactiv));
            nightLineChart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_night_activ));
            isDayLineChart = false;
            partOfDayLineChart = "Night";
            if(isCalendarLineChart) {
                titleLineChart.setText(partOfDayLineChart + " graph rainfall (mm) for " + startDateTitleLineChart + " - " + endDateTitleLineChart);
                initFromToLineChart();
            }
            else {
                titleLineChart.setText(partOfDayLineChart + " graph rainfall (mm) for " + intervalLineChart);
                initIntervalLineChart();
            }


        } else {
            dayLineChart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_day_activ));
            nightLineChart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_night_noactiv));
            isDayLineChart = true;
            partOfDayLineChart = "Day";
            if(isCalendarLineChart) {
                titleLineChart.setText(partOfDayLineChart + " graph rainfall (mm) for " + startDateTitleLineChart + " - " + endDateTitleLineChart);
                initFromToLineChart();
            }
            else {
                titleLineChart.setText(partOfDayLineChart + " graph rainfall (mm) for " + intervalLineChart);
                initIntervalLineChart();
            }
        }
    }

    @OnClick(R.id.graph_rainfall)
    protected void graphRainfallLineChartOnClick(){
        if(isCalendarLineChart) {
            titleLineChart.setText(partOfDayLineChart + " graph rainfall (mm) for " + startDateTitleLineChart + " - " + endDateTitleLineChart);
            initFromToLineChart();
        }
        else {
            titleLineChart.setText(partOfDayLineChart + " graph rainfall (mm) for " + intervalLineChart);
            initIntervalLineChart();
        }
    }

    @OnClick(R.id.calendar_line_chart)
    protected void calendarLineChartOnClick(){
        initDatePickerLineChart();
    }

    @OnClick(R.id.btn_pick_date_line_chart)
    protected void btnPickDateLineChartOnClick(){
        datePickerLineChartContainer.setVisibility(View.GONE);
        ArrayList<Date> selectedDates = (ArrayList<Date>) datePickerLineChart.getSelectedDates();

        if(selectedDates.size()>1){
            isCalendarLineChart = true;
            startDateLineChart = getDate(selectedDates.get(0));
            endDateLineChart = getDate(selectedDates.get(selectedDates.size()-1));

            startDateTitleLineChart = getTitleDate(selectedDates.get(0));
            endDateTitleLineChart = getTitleDate(selectedDates.get(selectedDates.size()-1));
            titleLineChart.setText(partOfDayLineChart + " graph rainfall (mm) for " + startDateTitleLineChart + " - " + endDateTitleLineChart);

            initFromToLineChart();
        }
    }

    private void initDatePickerLineChart(){
        datePickerLineChartContainer.setVisibility(View.VISIBLE);

        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);
        Calendar lastDay = Calendar.getInstance();
        lastDay.add(Calendar.DATE, -1);

        datePickerLineChart.init(lastYear.getTime(),new Date())
                .withSelectedDate(lastDay.getTime())
                .inMode(CalendarPickerView.SelectionMode.RANGE);
    }

    private void drawLineGraph() {
        lineChart.setOnChartValueSelectedListener(this);
        // no description text
        lineChart.getDescription().setEnabled(false);
        // enable touch gestures
        lineChart.setTouchEnabled(true);
        lineChart.setDragDecelerationFrictionCoef(0.9f);
        // enable scaling and dragging
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        lineChart.setDrawGridBackground(false);
        lineChart.setHighlightPerDragEnabled(true);
        // if disabled, scaling can be done on x- and y-axis separately
        lineChart.setPinchZoom(true);
        // set an alternative background color
        lineChart.setBackgroundColor(Color.WHITE);
    }

    private void setLegend() {
        lineChart.animateX(2500);

        // get the legend (only possible after setting data)
        Legend l = lineChart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);
        //   l.setTypeface(mTfLight);
        l.setTextSize(11f);
        l.setTextColor(Color.GRAY);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
//        l.setYOffset(11f);

        XAxis xAxis = lineChart.getXAxis();
        //  xAxis.setTypeface(mTfLight);
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.GRAY);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);

        YAxis leftAxis = lineChart.getAxisLeft();
        //   leftAxis.setTypeface(mTfLight);
        leftAxis.setTextColor(Color.BLACK);
        leftAxis.setAxisMinimum(0);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);

        YAxis rightAxis = lineChart.getAxisRight();
        //   leftAxis.setTypeface(mTfLight);
        rightAxis.setTextColor(Color.BLACK);
        rightAxis.setAxisMinimum(0);
        rightAxis.setDrawGridLines(true);
        rightAxis.setGranularityEnabled(true);
    }

    private void setData(List<Raining> rainingGraphList) {
        ArrayList<Entry> yValues = new ArrayList<>();
        for (int i = 0; i < rainingGraphList.size(); i++) {
            yValues.add(new Entry(i, (float) rainingGraphList.get(i).SumRainGraf));
            
        }
        LineDataSet set3;
        if (lineChart.getData() != null &&
                lineChart.getData().getDataSetCount() > 0) {
            set3 = (LineDataSet) lineChart.getData().getDataSetByIndex(0);
            set3.setValues(yValues);
            lineChart.getData().notifyDataChanged();
            lineChart.notifyDataSetChanged();
        } else {
            set3 = new LineDataSet(yValues, "Precipitation");
            set3.setAxisDependency(YAxis.AxisDependency.LEFT);
            set3.setColor(getResources().getColor(R.color.colorPrimary));
            set3.setCircleColor(Color.GRAY);
            set3.setLineWidth(2f);
            set3.setCircleRadius(3f);
            set3.setFillAlpha(65);
            set3.setFillColor(ColorTemplate.colorWithAlpha(Color.YELLOW, 200));
            set3.setDrawCircleHole(false);
            set3.setHighLightColor(Color.rgb(244, 117, 117));

            // create a data object with the datasets
            LineData data = new LineData(set3);
            data.setValueTextColor(Color.GRAY);
            data.setValueTextSize(9f);

            lineChart.setData(data);
        }
        lineChart.invalidate();
    }

    private void initIntervalLineChart() {
        initCountsIntervalLineChart();
        Call<ResponseBody> getAvgLineTemper = null;
        if (isDayLineChart) getAvgLineTemper = API.getApiRequestService().GraphicRainDay(String.valueOf(USM.getStation()),
                USM.getUserID(), USM.getUserToken(), intervalLineChart);
        else getAvgLineTemper = API.getApiRequestService().GraphicRainNight(String.valueOf(USM.getStation()),
                USM.getUserID(), USM.getUserToken(), intervalLineChart);

        getAvgLineTemper.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String result = API.getResponse(response.body().byteStream());
                Log.wtf("call",call.request().url().toString());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Raining>>() {}.getType();
                    rainingGraphList = gson.fromJson(result, listType);
                    if (rainingGraphList.size() > 0) {
                        setData(rainingGraphList);
                        setLegend();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void initFromToLineChart() {
        initCountFromToLineChart();
        Call<ResponseBody> getColumnTemper = null;
        if (isDayLineChart) getColumnTemper = API.getApiRequestService().RainingGraphicFromToDay(String.valueOf(USM.getStation()),
                USM.getUserID(), USM.getUserToken(), startDateLineChart, endDateLineChart);
        else getColumnTemper = API.getApiRequestService().RainingGraphicFromToNight(String.valueOf(USM.getStation()),
                USM.getUserID(), USM.getUserToken(), startDateLineChart, endDateLineChart);

        getColumnTemper.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Raining>>() {}.getType();
                    rainingGraphList = gson.fromJson(result, listType);
                    if (rainingGraphList.size() > 0) {
                        setData(rainingGraphList);
                        setLegend();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void initCountsIntervalLineChart(){
        final Call<ResponseBody> lastRain = API.getApiRequestService().LastRain(String.valueOf(USM.getStation()),
                USM.getUserID(), USM.getUserToken(), intervalLineChart);
        lastRain.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String result = API.getResponse(response.body().byteStream());
                Log.wtf("call",call.request().url().toString());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Raining>() {}.getType();
                    raining = gson.fromJson(result, listType);
                    timeLastRain.setText(raining.LastRain + " hours");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

        final Call<ResponseBody> rainyDays = API.getApiRequestService().RainyDays(String.valueOf(USM.getStation()),
                USM.getUserID(), USM.getUserToken(), intervalLineChart);
        rainyDays.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String result = API.getResponse(response.body().byteStream());
                Log.wtf("call",call.request().url().toString());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Raining>() {}.getType();
                    raining = gson.fromJson(result, listType);
                    numberOfRainyDays.setText(raining.RainyDays + " days");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void initCountFromToLineChart(){
        final Call<ResponseBody> lastRain = API.getApiRequestService().LastRainFromTo(String.valueOf(USM.getStation()),
                USM.getUserID(), USM.getUserToken(), startDateLineChart,endDateLineChart);
        lastRain.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String result = API.getResponse(response.body().byteStream());
                Log.wtf("call",call.request().url().toString());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Raining>() {}.getType();
                    raining = gson.fromJson(result, listType);
                    timeLastRain.setText(raining.LastRain + " hours");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

        final Call<ResponseBody> rainyDays = API.getApiRequestService().RainyDaysFromTo(String.valueOf(USM.getStation()),
                USM.getUserID(), USM.getUserToken(), startDateLineChart,endDateLineChart);
        rainyDays.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String result = API.getResponse(response.body().byteStream());
                Log.wtf("call",call.request().url().toString());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<Raining>() {}.getType();
                    raining = gson.fromJson(result, listType);
                    numberOfRainyDays.setText(raining.RainyDays + " days");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
    //End Line Chart -------------------------------------------------------------------------
}

