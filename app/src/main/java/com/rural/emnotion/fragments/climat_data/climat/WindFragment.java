package com.rural.emnotion.fragments.climat_data.climat;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rural.emnotion.MainActivity;
import com.rural.emnotion.R;
import com.rural.emnotion.charts.XYMarkerView;
import com.rural.emnotion.models.Temperature;
import com.rural.emnotion.models.Wind;
import com.rural.emnotion.models.Wind;
import com.rural.emnotion.network.API;
import com.rural.emnotion.settings.USM;
import com.rural.emnotion.util.WindRoseUtils;
import com.squareup.timessquare.CalendarPickerView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.rural.emnotion.util.DateUtils.getDate;
import static com.rural.emnotion.util.DateUtils.getTitleDate;

public class WindFragment extends Fragment {

    private static final String WIND_ROSE_TYPE = "Wind rose graph ";
    private static final String DAY_WIND_ROSE_TYPE = "Day period wind rose graph ";
    private static final String NIGHT_WIND_ROSE_TYPE = "Night period wind rose graph ";

    @BindView(R.id.wind_speed) TextView windSpeed;
    @BindView(R.id.wind_run) TextView windRun;
    @BindView(R.id.high_wind_speed) TextView highWindSpeed;
    @BindView(R.id.wind_direction) TextView windDirection;
    @BindView(R.id.high_wind_direction) TextView highWindDirection;

    @BindView(R.id.radar_chart) RadarChart radarChart;
    @BindView(R.id.title_radar_chart) TextView titleRadarChart;
    @BindView(R.id.spinner_radar_chart) MaterialSpinner spinnerRadarChart;
    @BindView(R.id.calendar_radar_chart) ImageView calendarRadarChart;
    @BindView(R.id.date_picker_radar_chart) CalendarPickerView datePickerRadarChart;
    @BindView(R.id.date_picker_radar_chart_container) RelativeLayout datePickerRadarChartContainer;
    @BindView(R.id.wind_rose_radar_chart) CardView windRoseRadarChart;
    @BindView(R.id.day_period_wind_radar_chart) CardView dayWindRoseRadarChart;
    @BindView(R.id.night_period_wind_radar_chart) CardView nightWindRoseRadarChart;
    
    private String intervalRadarChart;
    private boolean isCalendarRadarChart;
    private String typeWindRoseRadarChart;
    private String startDateRadarChart;
    private String endDateRadarChart;
    private String startDateTitleRadarChart;
    private String endDateTitleRadarChart;

    private List<Wind> windGraphList;
    private Wind wind;
    
    public WindFragment() {
    }


    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).showSpinner(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_wind, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        USM.init(getActivity());

        initObjects();
        initViews();
        initListeners();

        initHeaderWind();

        drawRadarGraph();
        windRoseRadarChart.performClick();
    }   

    private void initObjects(){
        intervalRadarChart = "last day";
    }

    private void initViews() {
        String[] intervalArray = getResources().getStringArray(R.array.general_intervals);
        spinnerRadarChart.setItems(intervalArray);
    }

    private void initListeners() {
        spinnerRadarChart.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                isCalendarRadarChart = false;
                Toast.makeText(getContext(), item.toString(), Toast.LENGTH_SHORT).show();
                intervalRadarChart = item.toString();
                titleRadarChart.setText(typeWindRoseRadarChart + " for " + " for" + intervalRadarChart);
                initIntervalRadarChart();
            }
        });
    }

    // Start Header ----------------------------------------------------------------------------

    private void initHeaderWind() {
        Call<ResponseBody> getTemper = API.getApiRequestService().Wind(String.valueOf(USM.getStation()), USM.getUserID(), USM.getUserToken());
        getTemper.enqueue(new Callback<ResponseBody>() {
            @SuppressLint({"DefaultLocale", "SetTextI18n"})
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Wind>>(){}.getType();
                    List<Wind> windList = gson.fromJson(result, listType);
                    Wind wind = windList.get(0);

                    windSpeed.setText(String.format("%.1f", wind.WindSpeed) + " m/sec");
                    windRun.setText(String.format("%.1f", wind.WindRun) + " m/sec");
                    highWindSpeed.setText(String.format("%.1f", wind.HiSpeed) + " m/sec");
                    windDirection.setText(wind.WindName + " " + wind.WindSamp + " °");
                    highWindDirection.setText(wind.WindHiName + " " + wind.WindHiSamp + " °");
                    
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    // End Header ----------------------------------------------------------------------------

    // Start Radar Chart ----------------------------------------------------------------------------

    @OnClick(R.id.wind_rose_radar_chart)
    protected void winRoseRadarChartOnClick(){
        typeWindRoseRadarChart = WIND_ROSE_TYPE;
        if(isCalendarRadarChart) {
            titleRadarChart.setText(typeWindRoseRadarChart + " for " + startDateTitleRadarChart + " - " + endDateTitleRadarChart);
            initFromToRadarChart();
        }
        else {
            titleRadarChart.setText(typeWindRoseRadarChart + " for " + intervalRadarChart);
            initIntervalRadarChart();
        }
    }

    @OnClick(R.id.day_period_wind_radar_chart)
    protected void dayWinRoseRadarChartOnClick(){
        typeWindRoseRadarChart = DAY_WIND_ROSE_TYPE;
        if(isCalendarRadarChart) {
            titleRadarChart.setText(typeWindRoseRadarChart + " for " + startDateTitleRadarChart + " - " + endDateTitleRadarChart);
            initFromToRadarChart();
        }
        else {
            titleRadarChart.setText(typeWindRoseRadarChart + " for " + intervalRadarChart);
            initIntervalRadarChart();
        }
    }

    @OnClick(R.id.night_period_wind_radar_chart)
    protected void nightWinRoseRadarChartOnClick(){
        typeWindRoseRadarChart = NIGHT_WIND_ROSE_TYPE;
        if(isCalendarRadarChart) {
            titleRadarChart.setText(typeWindRoseRadarChart + " for " + startDateTitleRadarChart + " - " + endDateTitleRadarChart);
            initFromToRadarChart();
        }
        else {
            titleRadarChart.setText(typeWindRoseRadarChart + " for " + intervalRadarChart);
            initIntervalRadarChart();
        }
    }

    @OnClick(R.id.calendar_radar_chart)
    protected void calendarRadarChartOnClick(){
        initDatePickerRadarChart();
    }

    @OnClick(R.id.btn_pick_date_radar_chart)
    protected void btnPickDateRadarChartOnClick(){
        datePickerRadarChartContainer.setVisibility(View.GONE);
        ArrayList<Date> selectedDates = (ArrayList<Date>) datePickerRadarChart.getSelectedDates();

        if(selectedDates.size()>1){
            isCalendarRadarChart = true;
            startDateRadarChart = getDate(selectedDates.get(0));
            endDateRadarChart = getDate(selectedDates.get(selectedDates.size()-1));

            startDateTitleRadarChart = getTitleDate(selectedDates.get(0));
            endDateTitleRadarChart = getTitleDate(selectedDates.get(selectedDates.size()-1));
            titleRadarChart.setText(typeWindRoseRadarChart + " for " + startDateTitleRadarChart + " - " + endDateTitleRadarChart);

            initFromToRadarChart();
        }
    }

    private void initDatePickerRadarChart(){
        datePickerRadarChartContainer.setVisibility(View.VISIBLE);

        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);
        Calendar lastDay = Calendar.getInstance();
        lastDay.add(Calendar.DATE, -1);

        datePickerRadarChart.init(lastYear.getTime(),new Date())
                .withSelectedDate(lastDay.getTime())
                .inMode(CalendarPickerView.SelectionMode.RANGE);
    }

    private void drawRadarGraph() {
        radarChart.setBackgroundColor(Color.WHITE);

        radarChart.getDescription().setEnabled(false);

        radarChart.setWebLineWidth(1f);
        radarChart.setWebColor(Color.LTGRAY);
        radarChart.setWebLineWidthInner(1f);
        radarChart.setWebColorInner(Color.LTGRAY);
        radarChart.setWebAlpha(100);
    }

    private void setLegend(List<Wind> windGraphList) {
        final String array[] = new String[16];
        for(int i=0;i<windGraphList.size();i++){
            array[i] = windGraphList.get(i).WindDir + " " + String.format("%.2f", windGraphList.get(i).Percent)+"%";
        }

        radarChart.animateXY(
                1400, 1400, Easing.EasingOption.EaseInOutQuad, Easing.EasingOption.EaseInOutQuad);

        XAxis xAxis = radarChart.getXAxis();
        xAxis.setTextSize(9f);
        xAxis.setYOffset(0f);
        xAxis.setXOffset(0f);
        xAxis.setValueFormatter(new IAxisValueFormatter() {

            private String[] mActivities = array;

            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return mActivities[(int) value % mActivities.length];
            }
        });
        xAxis.setTextColor(Color.BLACK);

        YAxis yAxis = radarChart.getYAxis();
        yAxis.setLabelCount(5, false);
        yAxis.setTextSize(9f);
        yAxis.setAxisMinimum(0f);
        yAxis.setDrawLabels(true);

        Legend l = radarChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(5f);
        l.setTextColor(Color.BLACK);
    }

    private void setData(List<Wind> windGraphList) {
        ArrayList<RadarEntry> entries1 = new ArrayList<>();
        ArrayList<RadarEntry> entries2 = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int i = 0; i < windGraphList.size(); i++) {
            entries1.add(new RadarEntry(windGraphList.get(i).MaxWindSpeed));

            entries2.add(new RadarEntry(windGraphList.get(i).AvgWindSpeed));
        }

        RadarDataSet set1 = new RadarDataSet(entries1, "Max wind speed. m/sec");
        set1.setColor(Color.rgb(103, 110, 129));
        set1.setFillColor(Color.rgb(103, 110, 129));
        set1.setDrawFilled(true);
        set1.setFillAlpha(180);
        set1.setLineWidth(1f);
        set1.setDrawHighlightCircleEnabled(true);
        set1.setDrawHighlightIndicators(false);

        RadarDataSet set2 = new RadarDataSet(entries2, "Avg wind speed. m/sec");
        set2.setColor(Color.rgb(121, 162, 175));
        set2.setFillColor(Color.rgb(121, 162, 175));
        set2.setDrawFilled(true);
        set2.setFillAlpha(180);
        set2.setLineWidth(1f);
        set2.setDrawHighlightCircleEnabled(true);
        set2.setDrawHighlightIndicators(false);

        ArrayList<IRadarDataSet> sets = new ArrayList<>();
        sets.add(set1);
        sets.add(set2);

        RadarData data = new RadarData(sets);
        data.setValueTextSize(8f);
        data.setDrawValues(false);
        data.setValueTextColor(Color.BLACK);

        radarChart.setData(data);
    }

    private void initIntervalRadarChart() {
        Call<ResponseBody> getAvgLineTemper = null;
        if (typeWindRoseRadarChart.equals(WIND_ROSE_TYPE)) {
            getAvgLineTemper = API.getApiRequestService().GraphicWindRose(String.valueOf(USM.getStation()),
                    USM.getUserID(), USM.getUserToken(), intervalRadarChart);
        } else if (typeWindRoseRadarChart.equals(DAY_WIND_ROSE_TYPE)) {
            getAvgLineTemper = API.getApiRequestService().GraphicWindRoseDay(String.valueOf(USM.getStation()),
                    USM.getUserID(), USM.getUserToken(), intervalRadarChart);
        } else if (typeWindRoseRadarChart.equals(NIGHT_WIND_ROSE_TYPE)) {
            getAvgLineTemper = API.getApiRequestService().GraphicWindRoseNight(String.valueOf(USM.getStation()),
                    USM.getUserID(), USM.getUserToken(), intervalRadarChart);
        }

        getAvgLineTemper.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String result = API.getResponse(response.body().byteStream());
                Log.wtf("call",call.request().url().toString());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Wind>>() {}.getType();
                    List<Wind> temp = gson.fromJson(result, listType);
                    windGraphList = WindRoseUtils.transformArray(temp);
                    if (windGraphList.size() > 0) {
                        setLegend(windGraphList);
                        setData(windGraphList);
                    }

                } catch (Exception e) {
                    Log.wtf("call",e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void initFromToRadarChart() {
        Call<ResponseBody> getColumnTemper = null;
        if (typeWindRoseRadarChart.equals(WIND_ROSE_TYPE)) {
            getColumnTemper = API.getApiRequestService().GraphicWindRose(String.valueOf(USM.getStation()),
                    USM.getUserID(), USM.getUserToken(), startDateRadarChart,endDateRadarChart);
        } else if (typeWindRoseRadarChart.equals(DAY_WIND_ROSE_TYPE)) {
            getColumnTemper = API.getApiRequestService().GraphicWindRoseDay(String.valueOf(USM.getStation()),
                    USM.getUserID(), USM.getUserToken(), startDateRadarChart,endDateRadarChart);
        } else if (typeWindRoseRadarChart.equals(NIGHT_WIND_ROSE_TYPE)) {
            getColumnTemper = API.getApiRequestService().GraphicWindRoseNight(String.valueOf(USM.getStation()),
                    USM.getUserID(), USM.getUserToken(), startDateRadarChart,endDateRadarChart);
        }

        getColumnTemper.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.wtf("call",call.request().url().toString());
                String result = API.getResponse(response.body().byteStream());
                try {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Wind>>() {}.getType();
                    List<Wind> temp = gson.fromJson(result, listType);
                    windGraphList = WindRoseUtils.transformArray(temp);
                    if (windGraphList.size() > 0) {
                        setLegend(windGraphList);
                        setData(windGraphList);
                    }

                } catch (Exception e) {
                    Log.wtf("call",e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

        //End Radar Chart -------------------------------------------------------------------------
    }
}

