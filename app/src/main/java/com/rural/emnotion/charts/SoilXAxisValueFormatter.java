package com.rural.emnotion.charts;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import static com.rural.emnotion.fragments.climat_data.climat.SoilFragment.FIRST_TYPE_BAR_CHART;
import static com.rural.emnotion.fragments.climat_data.climat.SoilFragment.SECOND_TYPE_BAR_CHART;
import static com.rural.emnotion.fragments.climat_data.climat.SoilFragment.THIRD_TYPE_BAR_CHART;

public class SoilXAxisValueFormatter implements IAxisValueFormatter {

    protected String[] mMonths;

    private BarLineChartBase<?> chart;

    public SoilXAxisValueFormatter(BarLineChartBase<?> chart,String type) {
        this.chart = chart;
        if(type.equals(FIRST_TYPE_BAR_CHART))
            mMonths = new String[]{"Moisture1","Moisture2","Moisture3","Moisture4"};
        else if(type.equals(SECOND_TYPE_BAR_CHART))
            mMonths = new String[]{"Temperature1","Temperature2","Temperature3","Temperature4"};
        else if(type.equals(THIRD_TYPE_BAR_CHART))
            mMonths = new String[]{"Leaf wetness1","Leaf wetness2"};
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {

        int days = (int) value;

        return mMonths[days];
    }

}

