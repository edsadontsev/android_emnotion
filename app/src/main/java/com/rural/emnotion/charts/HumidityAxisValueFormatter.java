package com.rural.emnotion.charts;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

public class HumidityAxisValueFormatter implements IAxisValueFormatter
{

    private DecimalFormat mFormat;

    public HumidityAxisValueFormatter() {
        mFormat = new DecimalFormat("###.##");
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return mFormat.format(value) + "%";
    }
}
