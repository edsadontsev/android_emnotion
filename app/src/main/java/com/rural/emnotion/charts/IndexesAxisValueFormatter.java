package com.rural.emnotion.charts;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

public class IndexesAxisValueFormatter implements IAxisValueFormatter {

    protected String[] mMonths = new String[]{
            "Wind chill", "Heat", "THW", "THSW","Dew point"};

    private BarLineChartBase<?> chart;

    public IndexesAxisValueFormatter(BarLineChartBase<?> chart) {
        this.chart = chart;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {

        int days = (int) value;

        return mMonths[days];
    }

}
