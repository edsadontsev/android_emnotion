package com.rural.emnotion.charts;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

/**
 * Created by Ed on 22.01.2018.
 */

public class PrecipitationXAxisValueFormatter implements IAxisValueFormatter {

    protected String[] mMonths = new String[]{
            "Sum rainfall", "Max rainfall", "Max rain rate"};

    private BarLineChartBase<?> chart;

    public PrecipitationXAxisValueFormatter(BarLineChartBase<?> chart) {
        this.chart = chart;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {

        int days = (int) value;

        return mMonths[days];
    }

}
