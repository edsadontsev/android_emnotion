package com.rural.emnotion.charts;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

/**
 * Created by Ed on 19.01.2018.
 */

public class BarometricPressureAxisValueFormatter implements IAxisValueFormatter
{

    private DecimalFormat mFormat;

    public BarometricPressureAxisValueFormatter() {
        mFormat = new DecimalFormat("###,###,###,##0.0");
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return mFormat.format(value) + " mmHg";
    }
}
