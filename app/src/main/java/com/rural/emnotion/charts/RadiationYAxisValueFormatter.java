package com.rural.emnotion.charts;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

/**
 * Created by Ed on 22.01.2018.
 */

public class RadiationYAxisValueFormatter implements IAxisValueFormatter {

    private DecimalFormat mFormat;

    public RadiationYAxisValueFormatter() {
        mFormat = new DecimalFormat("###.##");
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return mFormat.format(value) + " W/m2";
    }

}