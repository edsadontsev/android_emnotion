package com.rural.emnotion.settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.rural.emnotion.activities.LoginActivity;

/**
 * Created by Administrator on 31.03.2017.
 */

public class USM {
    private static String kmmPrefMainKey = "EmnotionRuralPreferencesMainKey";
    private static SharedPreferences preferences;

    public static void init(Context context) {
        preferences = null;
        System.gc(); // чтоб в памяти не плодилось
        preferences = context.getSharedPreferences(kmmPrefMainKey, Context.MODE_PRIVATE);
    }



    private static String KEY_USER_ID= "id";
    private static String KEY_USER_EMAIL= "email";
    private static String KEY_USER_PHONE= "phone";
    private static String KEY_USER_KEY= "key";
    private static String KEY_USER_SOC= "social";
    private static String KEY_USER_TOKEN= "token";
    private static String KEY_USER_STATUS= "status";
    private static String KEY_REGISTER = "isReg";

    private static String KEY_USER_STATION = "station";

    private static String KEY_USER_PRECIPITATION_DIM= "precipitationDim";
    private static String KEY_USER_TEMPERATURE_DIM= "temperatureDim";
    private static String KEY_USER_BAROMETRIC_DIM = "barmetricDim";


//    "Email": "android222@gmail",
//            "Pass": null,
//            "Phone": "123456789",
//            "IdUser": 7,
//            "ProviderKey": null,
//            "SocialNetwork": null,
//            "Token": "95eb2ca521d7dad3228f85773dd9283ae471ee50bb0be59dda4ca2bcf31f1de1",
//            "Status": 0

//    public static ArrayList<Order> getMyOrders() {
//        String recordsJson = preferences.getString(KEY_ORDERS, "");
//        if (recordsJson.equals("")) {
//            return new ArrayList<Order>();
//        }
//        return (new Gson()).fromJson(recordsJson, new TypeToken<ArrayList<Order>>() {
//        }.getType());
//    }
//
//
//    public static void setMyOrders(ArrayList<Order> list) {
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putString(KEY_ORDERS, (new Gson()).toJson(list));
//        editor.apply();
//    }


//
//
//    public static String  getCartDiscount() {
//        String code = preferences.getString(KEY_DISCOUNT, "");
//        return code;
//    }
//
//    public static void setCartDiscount(String code) {
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putString(KEY_DISCOUNT, code);
//        editor.apply();
//    }


    public static String  getUserID() {
        String code = preferences.getString(KEY_USER_ID, "");
        return code;
    }


    public static String  getUserEmail() {
        String code = preferences.getString(KEY_USER_EMAIL, "");
        return code;
    }

    public static int  getStation() {
        int code = preferences.getInt(KEY_USER_STATION, 9);
        return code;
    }

    public static String  getUserPhone() {
        String code = preferences.getString(KEY_USER_PHONE, "");
        return code;
    }

    public static String  getUserToken() {
        String code = preferences.getString(KEY_USER_TOKEN, "");
        return code;
    }


    public static void setUserSession(String id, String email,
                                      String phone, String token,
                                      String key, String soc, String status) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_USER_ID, id);
        editor.putString(KEY_USER_EMAIL, email);
        editor.putString(KEY_USER_PHONE, phone);
        editor.putString(KEY_USER_TOKEN, token);
        editor.putString(KEY_USER_KEY, key);
        editor.putString(KEY_USER_SOC, soc);
        editor.putString(KEY_USER_STATUS, status);
        editor.putBoolean(KEY_REGISTER, true);
        editor.apply();
    }

    public static boolean  isRegister() {
        boolean isReg = preferences.getBoolean(KEY_REGISTER, false);
        return isReg;
    }

    public static void setUserSetting(String precipitation, String temperature,
                                      String barometric ) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_USER_PRECIPITATION_DIM, precipitation);
        editor.putString(KEY_USER_TEMPERATURE_DIM, temperature);
        editor.putString(KEY_USER_BAROMETRIC_DIM, barometric);
        editor.apply();
    }

    public static String  getTemperatureDim() {
        String code = preferences.getString(KEY_USER_TEMPERATURE_DIM, "°C");
        return code;
    }


    public static String  getBarometricDim() {
        String code = preferences.getString(KEY_USER_BAROMETRIC_DIM, "");
        return code;
    }


    public static String  getPrecipitationDim() {
        String code = preferences.getString(KEY_USER_PRECIPITATION_DIM, "");
        return code;
    }

    public static void setStation(int station) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_USER_STATION, station);
        editor.apply();
    }



    public static void logout(Context _context){
        // Clearing all user data from Shared Preferences
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }

}
