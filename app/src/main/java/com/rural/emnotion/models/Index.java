package com.rural.emnotion.models;

/**
 * Created by user on 07-Sep-17.
 */

public class Index {

    public double  WindChill;

    public double HeatIndex;

    public double ThwIndex;

    public double ThswIndex;

    public double DewPt;

    public double ET;

    public String Date;

    public String Time;

    public double WindChillAvg;

    public double HeatIndexAvg;

    public double ThwIndexAvg;

    public double ThswIndexAvg;

    public double DewPtAvg;

    public double ETAvg;

    // time line

    public double LastRain;

    public double SunDay;

    public double LowSolar;

    public double HiSolar;

    public double LowUV;

    public double HiUV;

    public String Status;
}
