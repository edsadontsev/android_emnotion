package com.rural.emnotion.models;

public class Impact {

    public int IdAlarmRisk;
    public String AlarmRiskName;
    public int Level;
    public String CalcTimeToAlarm;
    public String Description;
    public String Solution;
    public int IdStation;
    public int IdProduct;
}
