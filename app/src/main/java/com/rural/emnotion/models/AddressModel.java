package com.rural.emnotion.models;

import java.util.List;

public class AddressModel {

    public List<Result> results;

    public class Result{
        public String formatted_address;
        public List<AddressComponent> address_components;
    }

    public class AddressComponent{
        public String long_name;
    }
}
