package com.rural.emnotion.models;

/**
 * Created by user on 07-Sep-17.
 */

public class Radiation {


    public double SolarRad;

    public double LowSolarRad;

    public double HiSolarRad;

    public double UvNdex;

    public double UvDose;

    public double HiUv;

    public int SunnyDays;

    public int DurSunnyDays;

    public int Cloudydays;

    // graphic

    public String Date;

    public double SolarRadAvg;

    public double UVndexAvg;

    public double UVdoseAvg;

    public String Status;
}
