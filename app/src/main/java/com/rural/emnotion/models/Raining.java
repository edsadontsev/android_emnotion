package com.rural.emnotion.models;

/**
 * Created by user on 07-Sep-17.
 */

public class Raining {


    public double Rain;

    public double RainRate;

    public double SumRain;

    public double RainFall;

    public double MaxRainRate;

    public String Time;

    public int LastRain;

    public int RainyDays;

    // graphic

    public String Date;

    public String TimeGraf;

    public double SumRainGraf;

    public String Status;
}
