package com.rural.emnotion.models;

import java.io.Serializable;

/**
 * Created by Administrator on 12.01.2018.
 */

public class Product implements Serializable {
    public String Delivery;
    public String Description;
    public int IdProduct;
    public int IdUser;
    public String Instruction;
    public String Name;
    public String PhotoName;
    public String Status;
    public String Token;
    public int PageNumber;
    public int Price;
    public String PaymentURL;
    public String IdLanguage;
    public String OwnerName;
    public String Email;
    public String Phone;
}
