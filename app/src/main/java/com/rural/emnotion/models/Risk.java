package com.rural.emnotion.models;

import java.io.Serializable;
import java.util.List;

public class Risk implements Serializable {

    public int IdUserAlarmRisk;
    public String AlarmRiskName;
    public int IdUserStation;
    public String Description;
    public String Solution;
    public int DayPart;
    public String StartDate;
    public String EndDate;
    public int Years;
    public int Days;
    public int Hours;
    public int Intervals;
    public int IdProduct;
    public String RegDate;
    public String RegTime;
    public int IdStation;
    public List<AlarmParams> alarmParams;

    public class AlarmParams implements Serializable{
        public int IdUserAlarmRisk;
        public int Id;
        public String ParameterName;
        public String Min;
        public String Max;
        public String Avg;
        public String Deviation;
        public String Operator;
        public String Interval;
    }
}
