package com.rural.emnotion.models;

import java.io.Serializable;

public class Message implements Serializable{

    public int IdMessage;
    public String IdUser;
    public String IdUserTo;
    public String MailText;
    public String NameFrom;
    public String NameTo;
    public String PhotoNameFrom;
    public String PhotoNameTo;
    public String MailDate;
    public String MailTime;
    public int CountMail;
}
