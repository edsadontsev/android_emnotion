package com.rural.emnotion.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 22.09.2017.
 */

public class Alarm implements Serializable{

    public int IdAlarmUserLog;
    public int IdUserAlarm;
    public String AlarmName;
    public int IdUserStation;
    public String StartAlarmTime;
    public boolean IsActive;
    public int IdUser;
    public int IdStation;

    public String RegDate;
    public String RegTime;
    public String Description;
    public String Status;

    public List<AlarmParams> alarmParams;

    public static class AlarmParams implements Serializable{
        public String IdUserAlarm;
        public int Id;
        public String Name;
        public String ParameterName;
        public String Min;
        public String Max;
        public String IdUser;
    }
    /*
    * [
    {
        "IdAlarmUserLog": 0,
        "IdUserAlarm": 30,
        "AlarmName": "Test1",
        "IdUserStation": 269,
        "StartAlarmTime": null,
        "IsActive": true,
        "IdUser": null,
        "IdStation": 0,
        "RegDate": "09-22-2017",
        "RegTime": "11:41:04",
        "Description": "Test1Test1Test1Test1Test1",
        "Status": "token success",
        "Token": null,
        "alarmParams": [
            {
                "IdUserAlarm": 30,
                "Id": 1,
                "Name": null,
                "ParameterName": "temp_out",
                "Min": "17",
                "Max": "20"
            }
        ]
    }
]*/
}
