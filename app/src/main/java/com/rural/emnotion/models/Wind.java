package com.rural.emnotion.models;

/**
 * Created by user on 07-Sep-17.
 */

public class Wind {

    public double WindSpeed;

    public double WindRun;

    public double HiSpeed;

    public int IdWindDir;

    public int WindSamp;

    public String WindName;

    public int IdWindHiDir;

    public int WindHiSamp;

    public String WindHiName;

    public String Status;

    // graphic

    public String WindDir;

    public float AvgWindSpeed;

    public float MaxWindSpeed;

    public double Percent;
}
