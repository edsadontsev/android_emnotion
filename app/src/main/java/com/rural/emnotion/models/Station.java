package com.rural.emnotion.models;

import java.io.Serializable;

/**
 * Created by user on 07-Sep-17.
 */

public class Station implements Serializable{

    public int IdStation;

    public int IdUser;

    public String Name;

    public int IdStationType;

    public String IpAddress;

    public double Longitude;

    public double Latitude;

    public int PrivateAccess;

    public int IdStationUsage;

    public String Description;

    public int IsActiv;

    public double Elevation;

    public int IdAddress;

    public String Country;

    public String City;

    public String Address;

    public String Status;

    public String Token;

    public Boolean IsEdit;

    public String NameStationUsage;
}
