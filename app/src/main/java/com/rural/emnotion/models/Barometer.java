package com.rural.emnotion.models;

/**
 * Created by user on 07-Sep-17.
 */
public class Barometer {

    public double Bar;

    public double BarHi;

    public String TimeHi;

    public double BarLow;

    public String TimeLow;

    public double BarMin;

    public double BarMax;

    public double BarAvg;

    public String Date;

    public String Time;

    public double BaroutAvg;

    public double BaroutMin;

    public double BaroutMax;

    public String Status;
}
