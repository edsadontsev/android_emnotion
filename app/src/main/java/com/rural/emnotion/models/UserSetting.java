package com.rural.emnotion.models;

/**
 * Created by user on 07-Sep-17.
 */

public class UserSetting {


    public String IdLanguage;

    public int UserType;

    public String Email;

    public String Phone;

    public int Temperature;

    public int Precipitation;

    public int Barometric;

    public int IdUser;

    public String Status;

    public String Token;

    public String SocialNetwork;
}
