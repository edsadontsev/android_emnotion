package com.rural.emnotion.models;

public class Temperature {
    public double TempOut;
    public double TempHi;
    public String TimeHi;
    public double TempLow;
    public String TimeLow;
    public double TempMin;
    public double TempMax;
    public double TempAvg;
    public String Date;
    public String Time;
    public double TempoutAvg;
    public double TempoutMin;
    public double TempoutMax;
    public String Status;

//    {"TempOut":0.0,"TempHi":0.0,"TimeHi":"0001-01-01T00:00:00","TempLow":0.0,"TimeLow":"0001-01-01T00:00:00","TempMin":1.22,"TempMax":25.55,"TempAvg":14.92,"Date":"0001-01-01T00:00:00","Time":null,"TempoutAvg":0,"TempoutMin":0,"TempoutMax":0,"Status":"token success"}
}
