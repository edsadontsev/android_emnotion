package com.rural.emnotion.models;

/**
 * Created by Administrator on 24.12.2017.
 */

public class Specialist {
    private int IdUserCompany;
    private String Name;
    private String Email1;

    private String Phone;
    private String MobilePhone;
    private String Skype;
    private String Messenger;
    private String Specialization;
    private String PhotoName;
    private int IdAddress;
    private int IdCountry;
    private int IdRegion;
    private int IdCity;
    private String FullAddress;
    private int Latitude;
    private int Longitude;
    private int IdUserContact;
    private String IdUser;
    private String Status;

    public int getIdUserCompany() {
        return IdUserCompany;
    }

    public void setIdUserCompany(int idUserCompany) {
        IdUserCompany = idUserCompany;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail1() {
        return Email1;
    }

    public void setEmail1(String email1) {
        Email1 = email1;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    public String getSkype() {
        return Skype;
    }

    public void setSkype(String skype) {
        Skype = skype;
    }

    public String getMessenger() {
        return Messenger;
    }

    public void setMessenger(String messenger) {
        Messenger = messenger;
    }

    public String getSpecialization() {
        return Specialization;
    }

    public void setSpecialization(String specialization) {
        Specialization = specialization;
    }

    public String getPhotoName() {
        return PhotoName;
    }

    public void setPhotoName(String photoName) {
        PhotoName = photoName;
    }

    public int getIdAddress() {
        return IdAddress;
    }

    public void setIdAddress(int idAddress) {
        IdAddress = idAddress;
    }

    public int getIdCountry() {
        return IdCountry;
    }

    public void setIdCountry(int idCountry) {
        IdCountry = idCountry;
    }

    public int getIdRegion() {
        return IdRegion;
    }

    public void setIdRegion(int idRegion) {
        IdRegion = idRegion;
    }

    public int getIdCity() {
        return IdCity;
    }

    public void setIdCity(int idCity) {
        IdCity = idCity;
    }

    public String getFullAddress() {
        return FullAddress;
    }

    public void setFullAddress(String fullAddress) {
        FullAddress = fullAddress;
    }

    public int getLatitude() {
        return Latitude;
    }

    public void setLatitude(int latitude) {
        Latitude = latitude;
    }

    public int getLongitude() {
        return Longitude;
    }

    public void setLongitude(int longitude) {
        Longitude = longitude;
    }

    public int getIdUserContact() {
        return IdUserContact;
    }

    public void setIdUserContact(int idUserContact) {
        IdUserContact = idUserContact;
    }

    public String getIdUser() {
        return IdUser;
    }

    public void setIdUser(String idUser) {
        IdUser = idUser;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}



/*
* IdUserCompany : 2
Name : "Viacheslav"
Email1 : "scalandis@ukr.net"
Phone : "123456789410"
MobilePhone : "123456789410"
Skype : ""
Messenger : ""
Specialization : "Pascal, Delphi, C++, CSS, Java, Pyton"
PhotoName : "1.jpg"
IdAddress : 0
IdCountry : 0
IdRegion : 0
IdCity : 0
FullAddress : "вулиця Міська, 1, Київ, Украина"
Latitude : 0
Longitude : 0
IdUserContact : 0
IdUser : "2"
Status : "token success"
*
* */